<?php

return [

    'paginator_template' => 'partials::bootstrap-4',

    "message" => "Welcome to your new package",

    # When the form containing file upload inputs is submitted, this is the path where
    # the uploaded files will be temporarily stored befored they are consumed by
    # form submit handler.
    "form_temporary_uploads_path" => public_path() . '/files/form-temporary-uploads',

    "form_temporary_uploads_db_connection" => 'mysql',

    "form_temporary_uploads_db_table" => 'laravel_form_temporary_uploads',

];
