<?php

namespace Vicvk\Lib;

class MyList
{
    public $totalItems;
    public $items;

    ///////////////////////////////////////////////////////////////////////////////////////////////////

    protected $paramsPrefix = '';

    protected $defaultSortBy = 'id';
    protected $defaultSortOrder = 'asc';
    protected $defaultRecsPerPage = 100;

    protected $sortableTags =
        [
            'id' =>
                [
                    'asc' => ['id|asc'],
                    'desc'=> ['id|desc']
                ],

                # 'name' =>
                #     [
                #         'asc' => ['last_name|asc', 'first_name|asc'],
                #         'desc'=> ['last_name|desc', 'first_name|desc']
                #     ],
        ];

    ///////////////////////////////////////////////////////////////////////////////////////////////////

///    public function __construct()
///    {
///    }


    /**
     * @param $query
     * @param array $requestInput
     * @throws Exception
     */
    public function fromQuery($query, Array $requestInput=null)
    {
        if ($requestInput == null) {
            $requestInput = \Request::all();
        }

        $currentSortBy = $this->getCurrentSortBy($requestInput);
        $currentSortOrder = $this->getCurrentSortOrder($requestInput);

        if (isset($this->sortableTags[$currentSortBy][$currentSortOrder])) {
            # all OK, we can sort
            $sort_fields = $this->sortableTags[$currentSortBy][$currentSortOrder];
        } 
        else {
            throw new Exception("Can't sort - the sort_by|sort_order combination is not found in sortable tags");
        }

        //////////////

        $currentPage = $this->getCurrentPage($requestInput);
        $currentRecsPerPage = $this->getCurrentRecsPerPage($requestInput);

        //////////////

        //        if ($query instanceof Collection)
        //        {
        //
        //        }

        $this->totalItems = $query->count();

        foreach($sort_fields as $idx => $s) {
            $s = explode('|', $s);
            $query = $query->orderBy($s[0], $s[1]);
        }

        $query = $query->skip($currentRecsPerPage * ($currentPage - 1))->take($currentRecsPerPage);

//\DB::connection()->enableQueryLog();
        $this->items = $query->get();
//dd(\DB::getQueryLog());

    }

    public function getLastPageNumber(Array $requestInput=null)
    {
        # it is supposed that fromQuery() method has been called by this moment, and we have
        # a $this->totalItems value set.

        if ($requestInput == null) {
            $requestInput = \Request::all();
        }

        $currentRecsPerPage = $this->getCurrentRecsPerPage($requestInput);

        return (int) ($this->totalItems / $currentRecsPerPage);
    }

    public function getFirstSortOrderForTag($tag)
    {
        $result = null;

        if (isset($this->sortableTags[$tag])) {
            $availableSortOrdersForTag = array_keys($this->sortableTags[$tag]);

            if (isset($availableSortOrdersForTag[0])) {
                $result = $availableSortOrdersForTag[0];
            }
        }

        return $result;
    }

    public function getNextSortOrderForTag($tag, $currentSortOrder)
    {
        $result = null;

        if (isset($this->sortableTags[$tag])) {
            $availableSortOrdersForTag = array_keys($this->sortableTags[$tag]);

            $nextSortId = array_search($currentSortOrder, $availableSortOrdersForTag);

            if ($nextSortId === false) {
                $nextSortId = 0;
            }

            $nextSortId++;

            if (isset($availableSortOrdersForTag[$nextSortId])) {
                $result = $availableSortOrdersForTag[$nextSortId];
            }
            elseif (isset($availableSortOrdersForTag[0])) {
                $result = $availableSortOrdersForTag[0];
            }
        }

        return $result;
    }

    public function getSortByParam()
    {
        return "{$this->paramsPrefix}_sb";
///        return "{$this->paramsPrefix}_sort_by";
    }

    public function getSortOrderParam()
    {
        return "{$this->paramsPrefix}_so";
///        return "{$this->paramsPrefix}_sort_order";
    }

    public function getPageParam()
    {
        return "{$this->paramsPrefix}_pg";
///        return "{$this->paramsPrefix}_page";
    }

    public function getRecsPerPageParam()
    {
        return "{$this->paramsPrefix}_rp";
///        return "{$this->paramsPrefix}_recs_per_page";
    }

    public function getCurrentSortBy(Array $requestInput=null)
    {
        if ($requestInput == null) {
            $requestInput = \Request::all();
        }

        $sortByParam = $this->getSortByParam();
        return isset($requestInput[$sortByParam]) ? $requestInput[$sortByParam] : $this->defaultSortBy;
    }

    public function getCurrentSortOrder(Array $requestInput=null)
    {
        if ($requestInput == null) {
            $requestInput = \Request::all();
        }

        $sortOrderParam = $this->getSortOrderParam();
        return isset($requestInput[$sortOrderParam]) ? $requestInput[$sortOrderParam] : $this->defaultSortOrder;
    }

    public function getCurrentPage(Array $requestInput=null)
    {
        if ($requestInput == null) {
            $requestInput = \Request::all();
        }

        $pageParam = $this->getPageParam();
        return isset($requestInput[$pageParam]) ? $requestInput[$pageParam] : 1;
    }

    public function getCurrentRecsPerPage(Array $requestInput=null)
    {
        if ($requestInput == null) {
            $requestInput = \Request::all();
        }

        $recsPerPageParam = $this->getRecsPerPageParam();
        return isset($requestInput[$recsPerPageParam]) ? $requestInput[$recsPerPageParam] : $this->defaultRecsPerPage;
    }


}

