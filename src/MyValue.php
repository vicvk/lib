<?php

namespace Vicvk\Lib;

abstract class MyValue
{
    protected $attributes;

    public function __construct($param1)
    {
        $className = get_class($this);

        $trace = debug_backtrace();

        throw new \BadMethodCallException("Constructor of value object (class {$className}) must be overwritten with real constructor implementation in child class. Error in {$trace[0]['file']} on line {$trace[0]['line']}");

        #### Example:
        $this->attributes['param1'] = (string) $param1;

        if ($this->attributes['param1'] == '') {
            throw new \InvalidArgumentException('param1 is required');
        }
    }

    public function sameValueAs(MyValue $value)
    {
        return (get_class($this) == get_class($value)) && ($this->attributes = $value->attributes);

///        $className = get_class($this);
///
///        $trace = debug_backtrace();
///
///        throw new \BadMethodCallException("Constructor of value object (class {$className}) must be overwritten with real constructor implementation in child class. Error in {$trace[0]['file']} on line {$trace[0]['line']}");
///
///        #### Example:
///
///        return
///            ($this->param1 == $value->param1)
///            ;

    }


    public function __set($name, $value)
    {
        $className = get_class($this);

        $trace = debug_backtrace();

        throw new \LogicException("Trying to modify a '{$name}' property of the value object (class {$className}) in {$trace[0]['file']} on line {$trace[0]['line']}");
    }

    public function __get($name)
    {
        if (array_key_exists($name, $this->attributes)) {
            return $this->attributes[$name];
        }

        $className = get_class($this);

        $trace = debug_backtrace();

        throw new \LogicException("Trying to get a '{$name}' property of the value object (class {$className}). But this value object doesn't have this property. Error in {$trace[0]['file']} on line {$trace[0]['line']}");
    }

    public function __isset($name)
    {
        return isset($this->attributes[$name]);
    }

    public function __unset($name)
    {
        $className = get_class($this);

        $trace = debug_backtrace();

        throw new \LogicException("Trying to unset a '{$name}' property of the value object (class {$className}) in {$trace[0]['file']} on line {$trace[0]['line']}");
    }

}