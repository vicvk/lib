<?php

namespace Vicvk\Lib\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Auth\Authenticatable;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Vicvk\Lib\QueryByRequestBuilder;

class MyUser extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    # roles should not be mass assignable!
    protected $fillable = ['first_name', 'last_name', 'email', 'password'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    #--------------------------------------------------------------
    ####################  BEGIN RELATIONSHIPS  ####################

//    public function savedSearches()
//    {
//        return $this->hasMany('Models\SavedSearch')->orderBy('id', 'desc');
//    }
//
//    public function savedListings()
//    {
//        return $this->hasMany('Models\SavedListing')->orderBy('id', 'desc');
//    }

    ####################  END RELATIONSHIPS  ####################
    #^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


    public function getFullNameAttribute()
    {
        $result = [];
        $result[] = $this->first_name;
        $result[] = $this->last_name;

        return implode(' ', $result);
    }

    public function hasRole($role)
    {
        if (is_array($role)) {
            $requireRole = $role;
        }
        else {
            $requireRole = preg_split('/[\|\,]/', $role);
        }

        if (!isset($this->roles) || ($this->roles===null) || ($this->roles===''))
        {
            $roles = [];
        }
        else
        {
            $roles = explode(',', $this->roles);
        }

        foreach ($requireRole as $r) {
            if (in_array($r, $roles)) {
                return true;
            }
        }

        return false;
    }

    public function addRole($role)
    {
        if (!$this->hasRole($role)) {

            if (!isset($this->roles) || ($this->roles===null) || ($this->roles===''))
            {
                $roles = [];
            }
            else
            {
                $roles = explode(',', $this->roles);
            }

            $roles[] = $role;
            $this->roles = implode(',', $roles);
        }
    }

    # NYI - removeRole($role)


    public function scopeByRole($query, $role)
    {
        return $query->whereRaw('FIND_IN_SET(?, roles) > 0', [$role]);
    }

    public function searchByRequest(Array $requestInput=null)
    {
        /// Laravel < 5.4
        /// $builder = app('Vicvk\Lib\QueryByRequestBuilder', [$this, $requestInput]);

        // Laravel >= 5.4
        $builder = app(QueryByRequestBuilder::class, ['model' => $this, 'requestInput' => $requestInput]);

        return $builder
            ->getQuery()
            ;
    }

}


