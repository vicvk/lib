<?php

namespace Vicvk\Lib\Facades;

use Illuminate\Support\Facades\Facade;


class HH extends Facade
{
	/**
	 * Get the registered component.
	 *
	 * @return string
	 */
	protected static function getFacadeAccessor()
	{
		return 'MyHtmlHelper';
	}
}
