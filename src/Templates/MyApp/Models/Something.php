<?php

namespace Models;

#use Illuminate\Database\Eloquent\SoftDeletes;
use Vicvk\Lib\MyModel;
use Vicvk\Lib\Formatter;
use Vicvk\Lib\QueryByRequestBuilder;

class Something extends MyModel
{
#    use SoftDeletes;

    protected $table = 'something';
    protected $primaryKey = 'id';
    public $timestamps = true;

    protected $fillable = ['title', 'status', 'publish_at', 'content_html'];

# This is how to define default values for attributes when model is created.
# Or! when we need to calculate the default values, this array can be set inside
# the model constructor.
#
#    protected $defaultAttributes = [
#        'title' => '1111',
#        'field_name_is_visible' => 'Yes',
#    ];

    # ===============================================================================
    ################  BEGIN CONSTRUCTOR  ############################################

    public function __construct(array $attributes = [])
    {
#        # if we need to calculate the default values, this is how it can be done in constructor:
#        #
#        $this->defaultAttributes = [
#            'some_unixdate' => time(),
#            'field_name_is_visible' => 'Yes',
#        ];



#        ##########################################################################
#        #  Modify attributes right before saving model to database
#
#        $this->saving(function ($model) {
#
#            $model->attributes['title']
#            $model->title
#            $model->getOriginal('title')   // previous value
#        });

        # Do not forget to call the parent constructor
        parent::__construct($attributes);
    }

    ################  END CONSTRUCTOR  ##############################################
    # ===============================================================================

    # =================================================================================
    ################  BEGIN RELATIONSHIPS  ############################################

    /*  https://laravel.com/docs/5.1/eloquent-relationships  */

#    public function category()
#    {
#        return $this->belongsTo('Models\Category', 'something_id', 'category_id');
#    }

#    public function categories()
#    {
#        # belongsToMany() is used only for Many-to-Many relationships.
#        return $this->belongsToMany('Models\Category', 'category_x_something', 'something_id', 'category_id');
#    }

#    public function comments()
#    {
#        return $this->hasMany('Models\Comment');
#    }

    ################  END RELATIONSHIPS  ##############################################
    # =================================================================================

    public function fieldsFormatting() 
    {
        return [
            'created_at' => 'date',
            'created_at_dow_date' => 'dow_date',
            'updated_at' => 'date',

            'publish_at' => ['date', ['Models\Post', 'formattingOnlyDisplayThisIfPostStatusIsPublish']],
            'publish_at_dow_date' => ['dow_date', ['Models\Post', 'formattingOnlyDisplayThisIfPostStatusIsPublish']],
        ];
    }

    public function fieldsAliases()
    {
        return [
            'created_at_dow_date' => 'created_at',
            'publish_at_dow_date' => 'publish_at',
        ];
    }

    public static function formattingOnlyDisplayThisIfPostStatusIsPublish($fieldName, $fieldValue, $dataObj, Formatter $formatterObj, Array $extraParams=[])
    {
        $result = $fieldValue;

        if ($dataObj->status != 'Publish') {
            $result = '-';
        }

        return $result;
    }





    public function rulesSave()
    {
        return [
            'title' => 'required|max:150',
            'status' => 'in:Draft,Publish',
            'publish_at' => 'required_if:status,Publish|integer',
        ];
    }


    public function getSlugAttribute()
    {
        if ($this->attributes['slug'] == '') {
            $result = str_slug($this->attributes['title']);
        }
        else {
            $result = $this->attributes['slug'];
        }
        return $result;
    }

    public function getContentPlainAttribute()
    {
        if ($this->attributes['auto_content_plain'] == '') {
            $result = UtilsHtml::plainTextFromHtml($this->attributes['content_html']);
        }
        else {
            $result = $this->attributes['auto_content_plain'];
        }

        return $result;
    }

    public function getExcerptPlainAttribute()
    {
        if ($this->attributes['auto_excerpt_plain'] == '') {

            if ($this->attributes['excerpt_html'] == '') {
                $result = UtilsString::truncate($this->getContentPlainAttribute(), 300);
            }
            else {
                $result = UtilsHtml::plainTextFromHtml($this->attributes['excerpt_html']);
            }
        }
        else {
            $result = $this->attributes['auto_excerpt_plain'];
        }

        return $result;
    }

    public function getMainPhotoUrlAttribute()
    {
        if ($this->attributes['main_photo'] == '') {

            if ($this->attributes['auto_first_photo_from_content'] == '') {
                $result = '';
            }
            else {
                $result = $this->attributes['auto_first_photo_from_content'];
            }
        }
        else {
            $result = $this->attributes['main_photo'];
        }

        return $result;
    }



    public function searchByRequest(Array $requestInput=null)
    {
        /// Laravel < 5.4
        /// $builder = app('Vicvk\Lib\QueryByRequestBuilder', [$this, $requestInput]);

        // Laravel >= 5.4
        $builder = app(QueryByRequestBuilder::class, ['model' => $this, 'requestInput' => $requestInput]);

        return $builder
            ->addByScope('buildingId')
            ->getQuery()
            ;
    }

    public function scopeByBuildingId($query, $buildingId)
    {
        return $query->where('building_id', '=', $buildingId);
    }

    abstract public function adminSearchByRequest(Array $requestInput=null);

    public function scopeByStatus($query, $status)
    {
        return $query->where('status', '=', $status);
    }

    public function scopePublish($query)
    {
        return $query->where('status', '=', 'Publish')
                     ->where('publish_at', '<=', time());
    }

    public function scopePublished($query)
    {
        return $this->scopePublish($query);
    }


    public static function findPublishedOrFail($id)
    {
        $result = static::where('id', $id)
                      ->where('status', '=', 'Publish')
                      ->where('publish_at', '<=', time())
                      ->first();

        if (!is_null($result)) {
            return $result;
        }

        throw (new ModelNotFoundException)->setModel(get_called_class());
    }

    public function getStatusOptions() {
        return [
            'Draft' => 'Draft',
            'Publish' => 'Publish',
        ];
    }



}
