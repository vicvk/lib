<?php

Route::group(['prefix' => 'admin', 'as' => 'admin-auth::'], function()
{
    Route::get('auth/login', ['as' => 'getLogin', 'uses' => 'AuthController@getLogin']);
    Route::post('auth/login', ['as' => 'postLogin', 'uses' => 'AuthController@postLogin']);
    Route::get('auth/logout', ['as' => 'getLogout', 'uses' => 'AuthController@getLogout']);



});


