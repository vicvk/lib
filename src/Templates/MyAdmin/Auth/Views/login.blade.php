@extends('admin-layouts::master')

@section('meta_title', 'Please sign in')

@section('head')
    <link rel="stylesheet" href="/css/mylib/login.css">
@stop





@section('content')

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif


<form method="POST" action="/admin/auth/login" class="form-signin">
{!! csrf_field() !!}

        <h2 class="form-signin-heading">Please sign in</h2>
        <label for="inputEmail" class="sr-only">Email address</label>
        <input type="text" name="email" value="{{ old('email') }}" id="inputEmail" class="form-control" placeholder="Email address" required autofocus>


        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>

        <div class="checkbox">
          <label>
            <input type="checkbox" name="remember"> Remember me
          </label>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
</form>


@stop
