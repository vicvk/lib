<?php

namespace MyAdmin\Auth;

// NYI - this should inherit from MyComponentServiceProvider base class


use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as SP;

class ServiceProvider extends SP {

    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'MyAdmin\Auth';


    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        # usage: return view('building::test');
        $this->loadViewsFrom(realpath(__DIR__.'/Views'), 'admin-auth');

        //
    }

    public function register()
    {
        parent::register();

        //
    }


    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('MyAdmin/Auth/routes.php'));
    }

}
