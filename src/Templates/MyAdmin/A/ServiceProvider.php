<?php

namespace MyAdmin\A;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as SP;

class ServiceProvider extends SP {

    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'MyAdmin\A';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        $this->loadViewsFrom(realpath(__DIR__.'/Views/#emails'), 'admin-emails');
        $this->loadViewsFrom(realpath(__DIR__.'/Views/#layouts'), 'admin-layouts');
        $this->loadViewsFrom(realpath(__DIR__.'/Views/#partials'), 'admin-partials');
    }

    public function register()
    {
        parent::register();

        //
    }


    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map()
    {
        Route::group([
            'middleware' => 'web',
            'namespace' => $this->namespace,
        ], function ($router) {
            require base_path('MyAdmin/A/routes.php');
        });
    }

}
