<?php

Route::group(['prefix' => 'admin', 'as' => 'admin::', 'middleware' => ['admin.auth', 'role:admin|realtor']], function()
{
    Route::get('/', ['as'=>'index', 'uses'=>function() {
        return \Redirect::route('admin-contact::index');
    } ]);


});


