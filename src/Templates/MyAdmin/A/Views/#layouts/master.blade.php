<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>@yield('meta_title')</title>

    <!-- script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script -->
    <script src="//code.jquery.com/jquery-2.1.1.min.js"></script>

    <link rel="stylesheet" media="all" type="text/css" href="//code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css" />
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jquery-ui-timepicker-addon/1.6.1/jquery-ui-timepicker-addon.min.css">

    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/mylib/custom.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" />
    <meta name="csrf-token" content="{{ csrf_token() }}">

@yield('head')

</head>

<body>

<div class="container">


@if (!Route::is('admin-auth::getLogin') && !Route::is('admin-auth::getLogout'))
    @include('admin-partials::!menu')
@endif



@yield('content')
</div>



<script src="//code.jquery.com/ui/1.11.0/jquery-ui.min.js"></script>

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-ui-timepicker-addon/1.6.1/jquery-ui-timepicker-addon.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-ui-timepicker-addon/1.6.1/i18n/jquery-ui-timepicker-addon-i18n.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-ui-timepicker-addon/1.6.1/jquery-ui-sliderAccess.min.js"></script>

<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

<script src="/js/ckeditor/ckeditor.js"></script>

<script src="/js/mylib/main.js"></script>
<script src="/js/mylib/delete-by-checkboxes.js"></script>
<script src="/js/mylib/search-by-form.js"></script>
<script src="/js/mylib/sort-by-links.js"></script>
<script src="/js/mylib/pagination-links.js"></script>
<script src="/js/mylib/return-back.js"></script>
<script src="/js/mylib/ajax-call.js"></script>
<script src="/js/mylib/ajax-form.js"></script>
<script src="/js/mylib-custom.js"></script>

<script src="/js/typeahead.bundle.js"></script>



@include('admin-partials::flash-messages')



</body>
</html>
