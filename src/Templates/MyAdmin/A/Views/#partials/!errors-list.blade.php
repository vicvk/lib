<?php 
$formId = isset($formId) ? $formId : '';
?>

<div class="mylib-errors-for-form mylib-errors-for-form-{{$formId}}"{!! $errors->any() ? '' : ' style="display:none;"' !!}>
<span>Your submission contained errors:</span>
<ul>
@foreach($errors->all() as $error)
<li>{{ $error }}</li>
@endforeach
</ul>
</div>
