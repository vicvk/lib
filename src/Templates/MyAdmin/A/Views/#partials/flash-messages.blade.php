
@if (\Session::has('flash_error'))
<script>
mylib.popupError( '{{ \Session::get('flash_error') }}' )
</script>
@elseif (\Session::has('flash_warning'))
<script>
mylib.popupWarning( '{{ \Session::get('flash_warning') }}' )
</script>
@elseif (\Session::has('flash_success'))
<script>
mylib.popupSuccess( '{{ \Session::get('flash_success') }}' )
</script>
@elseif (\Session::has('flash_info'))
<script>
mylib.popupInfo( '{{ \Session::get('flash_info') }}' )
</script>
@endif
