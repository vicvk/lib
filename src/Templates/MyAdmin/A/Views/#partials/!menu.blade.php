
<nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid" style="padding-left: 0px;">

        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
{{--
            <a class="navbar-brand" href="#">DerekThornton.com</a>
--}}
        </div>


        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" style="padding-left: 0px;">

            <ul class="nav navbar-nav" style="width:100%;">
                <li><a href="{{ route('admin-contact::index') }}">Contact Form</a></li>
                <li><a href="{{ route('admin-request-showing::index') }}">Showing Requests</a></li>

@if (\Auth::user()->hasRole('admin'))
                <li><a href="{{ route('admin-building::index') }}">Buildings</a></li>
                <li><a href="{{ route('admin-settings::edit') }}">Settings</a></li>
                <li><a href="{{ route('admin-user::index') }}">Realtors</a></li>
                <li><a href="{{ route('admin-blog::index') }}">Blog</a></li>
@endif

                <li><a href="{{ route('admin-auth::getLogout') }}">Logout</a></li>


                <li style="float:right;">Logged in as <strong>{{ \Auth::user()->name }}</strong></li>


{{--
                <li class="active"><a href="#">Buildings <span class="sr-only">(current)</span></a></li>
                <li><a href="#">Link</a></li>
--}}

{{--
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Dropdown <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li class="divider"></li>
                        <li><a href="#">Separated link</a></li>
                        <li class="divider"></li>
                        <li><a href="#">One more separated link</a></li>
                    </ul>
                </li>
--}}

            </ul>


{{--
            <form class="navbar-form navbar-left" role="search">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Search">
                </div>
                <button type="submit" class="btn btn-default">Submit</button>
            </form>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#">Link</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Dropdown <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li class="divider"></li>
                        <li><a href="#">Separated link</a></li>
                    </ul>
                </li>
            </ul>
--}}
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
