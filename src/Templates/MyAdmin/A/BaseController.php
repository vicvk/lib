<?php

namespace MyAdmin\A;

# NYI - !!!! right now CrudCotrollers do not inherit from BaseController - this is incorrect!!!
# probably Crud functionality should be implemented as a CrudTrait.

use Vicvk\Lib\Traits\MyBaseControllerTrait;

use Illuminate\Routing\Controller;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;

abstract class BaseController extends Controller
{
    use MyBaseControllerTrait;

    use AuthorizesRequests, ValidatesRequests;


    /*
     *
     * {{ HH::setRequiredMetaTitlePrefix('') }}
* {{ HH::setRequiredMetaTitleSuffix(' - Derek Thornton') }}
     */


# Here is how to share certain view variables
#    public function __construct ()
#    {
#        $browserDetect = \Request::session()->get('browserDetect', function() {
#
#            $result = \BrowserDetect::detect();
#
#            \Request::session()->put('browserDetect', $result);
#
#            return $result;
#        });
#
#        view()->share('browserDetect', $browserDetect);
#
#
#        $footerFeaturedListings = \Models\Listing::getFooterFeaturedListings();
#        view()->share('footerFeaturedListings', $footerFeaturedListings);
#
#        $footerRecentBlogPosts = \Models\BlogPost::getRecentPosts();
#      
#        view()->share('footerRecentBlogPosts', $footerRecentBlogPosts);
#    }


}
