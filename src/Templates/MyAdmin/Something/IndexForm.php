<?php

namespace MyAdmin\Something;

# When EditForm is present, in most cases it can be used as the IndexForm too.

use Vicvk\Lib\MyForm;

class IndexForm extends MyForm {

    protected $modelClassName = 'Models\Something';  # in search form its probably not needed
                                                   # but we can set it in case if the Model class 
                                                   # is used for example in getPropertyTypeOptions()
    public function getSomethingTypeOptions() {
        return [
            '' => 'Any Type',
            'value-1' => 'label-1',
            'value-2' => 'label-2',
        ];
    }


    public function onProcessRequestInput($formWasSubmitted)
    {
        if (isset($this->v['beds'])) {
            $this->v['min_beds'] = $this->v['beds'];
            $this->v['max_beds'] = $this->v['beds'];
        }

        if (isset($this->v['baths'])) {
            $this->v['min_baths'] = $this->v['baths'];
            $this->v['max_baths'] = $this->v['baths'];
        }
    }


} 

