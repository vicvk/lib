<?php

Route::group(['prefix' => 'admin', 'as' => 'admin-something::', 'middleware' => ['admin.auth', 'role:admin']], function()
{
    Route::get('something/{blogPostId}/{pageNumber?}', ['as'=>'index', 'uses'=>'Controller@index']);
    Route::post('something/{blogPostId}/{pageNumber?}', ['as'=>'store', 'uses'=>'Controller@store']);

    Route::get('something/create', ['as'=>'create', 'uses'=>'Controller@create']);
    Route::post('something', ['as'=>'store', 'uses'=>'Controller@store']);

    Route::get('something/{numericId}/edit', ['as'=>'edit', 'uses'=>'Controller@edit']);
    Route::put('something/{numericId}', ['as'=>'update', 'uses'=>'Controller@update']);


    Route::delete('something', ['as'=>'destroyMultiple', 'uses'=>'Controller@destroyMultiple']);

});

