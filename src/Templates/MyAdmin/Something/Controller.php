<?php

namespace MyAdmin\Something;

use MyAdmin\A\BaseController;
use Vicvk\Lib\Traits\MyIndexControllerTrait;
use Vicvk\Lib\Traits\MyCreateControllerTrait;
use Vicvk\Lib\Traits\MyEditControllerTrait;
use Vicvk\Lib\Traits\MyDestroyControllerTrait;

class Controller extends BaseController
{
    // ==============================================================================
    use MyIndexControllerTrait;

    protected $indexModelClassName = 'Models\Something';
    protected $indexSearchByRequestModelMethod = 'adminSearchByRequest';
    protected $indexListClassName = 'MyAdmin\Something\IndexList';
    protected $indexFormClassName = 'MyAdmin\Something\IndexForm';
    protected $indexView = 'admin-something::index';
    protected $indexListItemsView = 'admin-something::index-list-items';


# this is rarly needed:
#
#    public function index($blogPostId, $pageNumber=null)
#    {
#        $this->addRequestVar('blogPostId', $blogPostId);
#
#        $this->addViewVar('blogPostId', $blogPostId);
#        $this->addViewVar('pageNumber', $pageNumber);
#
#        $hs = $this->handleCreate();
#        $this->addViewVar('form', $hs['form']);
#
#        return $this->traitIndex($pageNumber);
#    }



    // ==============================================================================
    use MyCreateControllerTrait;

    protected $createFormClassName = 'MyAdmin\Something\EditForm';
    protected $createView = 'admin-something::create';

#    public function create()
#    {
#        return $this->traitCreate();
#    }

#    protected function onAdditionalCreateViewVars()
#    {
#        return ['more_template_variables' => '123'];
#    }


    // ==============================================================================
    use MyEditControllerTrait;

    protected $editFormClassName = 'MyAdmin\Something\EditForm';
    protected $editView = 'admin-something::edit';

#    public function edit($id=null)
#    {
#        return $this->traitEdit($id);
#    }

#    protected function onAdditionalEditViewVars()
#    {
#        return ['more_template_variables' => '123'];
#    }

    // ==============================================================================

    use MyDestroyControllerTrait;

    protected $destroyModelClassName = 'Models\Something';

    // ==============================================================================



}

