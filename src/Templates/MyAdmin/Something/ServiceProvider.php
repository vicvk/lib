<?php

namespace MyAdmin\Something;

// NYI - this should inherit from MyComponentServiceProvider base class


use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as SP;

class ServiceProvider extends SP {

    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'MyAdmin\Something';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();

        # usage: return view('admin-something::template');
        $this->loadViewsFrom(realpath(__DIR__.'/Views'), 'admin-something');

        //
    }

    public function register()
    {
        parent::register();

        //
    }


    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map()
    {
        Route::group([
            'middleware' => 'web',
            'namespace' => $this->namespace,
        ], function ($router) {
            require base_path('MyAdmin/Something/routes.php');
        });
    }

}
