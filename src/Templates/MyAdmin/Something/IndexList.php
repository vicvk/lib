<?php

namespace MyAdmin\Something;

use Vicvk\Lib\MyList;

class IndexList extends MyList {

    protected $paramsPrefix = '';

    protected $defaultSortBy = 'sort_order';
    protected $defaultSortOrder = 'asc';
    protected $defaultRecsPerPage = 1000;

    protected $sortableTags =
        [
             'sort_order' =>
                [
                    'asc' => ['sort_order|asc'],
                    'desc'=> ['sort_order|desc'],
                ],

             'created_at' =>
                [
                    'asc' => ['created_at|asc'],
                    'desc'=> ['created_at|desc'],
                ],

             'title' =>
                [
                    'asc' => ['title|asc'],
                    'desc'=> ['title|desc'],
                ],

             'status' =>
                [
                    'asc' => ['status|asc'],
                    'desc'=> ['status|desc'],
                ],

             'publish_at' =>
                [
                    'asc' => ['publish_at|asc'],
                    'desc'=> ['publish_at|desc'],
                ],
        ];
}

