

@foreach ($list->items as $item)
    <tr>
      <td><input type="checkbox" value="{{ $item->id }}" class="mylib-select-checkboxes"></td>
      <td>{{ $item->f('created_at') }}</td>
      <td>{{ $item->title }}</td>
      <td>{{ $item->status }}</td>
      <td>{{ $item->f('publish_at') }}</td>
      <td>{!! HH::buttonEdit()->route('admin-blog::edit', [$item->id]) !!}</td>
      <td>{!! HH::buttonPreview()->route('blog-post-detail', [$item->id, $item->slug]) !!}</td>
    </tr>
@endforeach

