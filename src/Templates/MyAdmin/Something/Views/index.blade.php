@extends('admin-layouts::master')

@section('meta_title', 'Blog Photos')

@section('content')

<h1>Blog Photos</h1>

{{ HH::listMode('classic') }}

{!! Former::vertical_open_for_files()
        ->route('admin-blog-photo::store', $blogPostId, $pageNumber)
        ->method('GET') 
!!}

{{-- ---------------------------------------------------------------------------- --}}


{!! HH::hiddenReturnBack( route('admin-blog-photo::index', $blogPostId, $pageNumber) ) !!}

{!! Former::populate($form->v) !!}

{{-- {!! Former::text('title')->autofocus() !!}  --}}

{!! Former::files('files')->accept('image/jpeg', 'image/png') !!}


{!! HH::buttonSubmit()->label('Upload') !!}
<br/><br/><br/><br/>





{{-- ---------------------------------------------------------------------------- --}}
{!! Former::close() !!}


@if ($list->totalItems > 0)

{!! HH::useList($list) !!}

<div class="mylib-list-paginator-container">
{!! HH::renderPaginator() !!}
</div>

<table class="table table-striped mylib-list-container">
  <thead>
    <tr>
      <th width="5%"><input type="checkbox" class="mylib-toggle-checkboxes" title="Select/Unselect All"></th>

      <th>{!! HH::linkToSortBy('created_at') !!}</th>
      <th>{!! HH::linkToSortBy('title') !!}</th>
      <th>{!! HH::linkToSortBy('status') !!}</th>
      <th>{!! HH::linkToSortBy('publish_at') !!}</th>

      <th>&nbsp;</th>
    </tr>
  </thead>
  <tbody>

@include('admin-blog::index-list-items')

  </tbody>
</table>


<div class="mylib-list-paginator-container">
{!! HH::renderPaginator() !!}
</div>


@else
No records found.
@endif



@stop
