<?php

namespace MyAdmin\Something;

use Vicvk\Lib\MyForm;

class EditForm extends MyForm {

    protected $modelClassName = 'Models\Something';

#    public function rulesSave()
#    {
#        return [
#            'first_name' => 'required|max:50',
#        ];
#    }

    public function rulesStore()
    {
        return [
            'title' => 'required|max:50',
        ];
    }

#    public function rulesUpdate()
#    {
#        return [
#            'name' => 'required|max:50',
#            'email' => 'required|email|max:50',
#            'password' => 'confirmed|max:50',
#        ];
#    }

#    public function messagesSave()
#    {
#        return [
#            'first_name.required' => 'Please enter your first name',
#        ];
#    }

#    public function messagesStore()
#    {
#        return [
#            'first_name.required' => 'Please enter your first name',
#        ];
#    }

#    public function messagesUpdate()
#    {
#        return [
#            'first_name.required' => 'Please enter your first name',
#        ];
#    }


#    protected function onInitValuesForCreate()
#    {
#        parent::onInitValuesForCreate();
#
#        $this->v['publish_at_str'] = date('M d, Y h:i a', time());
#    }

#    protected function onHandleStore()
#    {
#        $this->model->addRole('realtor');
#
#        if ($this->v['password'] != '') {
#            $this->v['password'] = bcrypt($this->v['password']);
#        }
#
#        parent::onHandleStore();
#    }

#    protected function onInitValuesForEdit($id=null)
#    {
#        parent::onInitValuesForEdit($id);
#
#        $this->v['publish_at_str'] = date('M d, Y h:i a', $this->v['publish_at']);
#    }

#    protected function onHandleUpdate($id=null)
#    {
#        $this->model->addRole('realtor');
#
#        if ($this->v['password'] != '') {
#            $this->v['password'] = bcrypt($this->v['password']);
#        }
#
#        parent::onHandleUpdate($id);
#    }

#    protected function onProcessRequestInput($formWasSubmitted)
#    {
#        if (isset($this->v['publish_at_str'])) {
#            $this->v['publish_at'] = strtotime($this->v['publish_at_str']);
#        }
#    }

#    public function getAreaOptions() {
#
#        return \Cache::remember('listings_areas', 60*24*3, function() {
#            $result = [];
#            $result[''] = 'Any Area';
#
#            $areas = \DB::select('select distinct area from wp_realty_listingsdb ORDER BY area ASC');
#
#            foreach ($areas as $r) {
#                $result[$r->area] = $r->area;
#            }
#
#            return $result;
#        });
#    }

#    public function getPropertyTypeOptions() {
#        return [
#            '' => 'Any Type',
#            'Residential Attached' => 'Condo/Townhouse',
#            'Residential Detached' => 'Detached House',
#            'Land Only' => 'Land Only',
#            'Multifamily' => 'Multi-Family',
#        ];
#    }





} 

