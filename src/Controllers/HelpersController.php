<?php

namespace Vicvk\Lib\Controllers;

use Illuminate\Routing\Controller;

class HelpersController extends Controller {

    public function closeFancyboxAndRefreshParent()
    {
        return <<<X
<script>
parent.window.location.replace(parent.window.location.href);
parent.jQuery.fancybox.close();
</script>
X;
    }



}
