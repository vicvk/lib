<?php

namespace Vicvk\Lib\Controllers;

use Illuminate\Routing\Controller;

class DevController extends Controller {

    public function index()
    {
        return view('vicvk.lib::index');
    }

    public function env()
    {
///        $x = config('vicvk-lib.message', '#');
///        echo $x;


        return \App::environment();
    }

    public function routes()
    {
        \Artisan::call('route:list', array());

        $routes_array = [];

        $text = \Artisan::output();

        $lines = explode(PHP_EOL, $text);

        $line_counter = 0;
        foreach($lines as $line)
        {
            if (($line == '') || (substr($line, 0, 1) == '+'))
            {
                continue;
            }

            $line_counter++;

            if ($line_counter == 1)
            {
                continue;
            }

            $line = substr($line, 1);
            $line = substr($line, 0, strlen($line)-1);

            $line_array = explode(' | ', $line);

            array_walk($line_array, function (&$value, $key) { $value = trim($value);});

            $uri_parts = explode('/', $line_array[2]);

            for ($i=0; $i<10; $i++)
            {
                $line_array[100+$i] = isset($uri_parts[$i]) ? $uri_parts[$i] : '';
            }

            $routes_array[] = $line_array;
        }

////        print_r($routes_array);

        for ($i=0; $i<10; $i++)
        {
            $sort_by_uri[$i] = [];
        }

        $sort_by_action = [];
        $sort_by_name = [];

        foreach($routes_array as $key => $route)
        {
            for ($i=0; $i<10; $i++)
            {
                $sort_by_uri[$i][$key] = $route[100+$i];
            }

            $sort_by_action[$key] = $route[4];
            $sort_by_name[$key] = $route[3];
        }

//print_r($sort_by_action);

        array_multisort($sort_by_uri[0], SORT_ASC,
                        $sort_by_uri[1], SORT_ASC,
                        $sort_by_uri[2], SORT_ASC,
                        $sort_by_uri[3], SORT_ASC,
                        $sort_by_uri[4], SORT_ASC,
                        $sort_by_uri[5], SORT_ASC,
                        $sort_by_uri[6], SORT_ASC,
                        $sort_by_uri[7], SORT_ASC,
                        $sort_by_uri[8], SORT_ASC,
                        $sort_by_uri[9], SORT_ASC,
                        $sort_by_action, SORT_ASC,
                        $sort_by_name, SORT_DESC,
                        $routes_array);


        //print_r($lines);
//    print_r($routes_array);

        echo <<<STR
<style>
    body {
        color: #333333;
    }

    table {
        border-collapse: collapse;
    /*    font-family: Courier New;*/
    }

    td {
        border: 1px black solid;
        white-space: nowrap;
        border-color: #CCCCFF;
        padding-left:10px;
        padding-right:10px;
    }

    tr.delimiter td {
        border-left: 1px transparent solid;
        border-right: 1px transparent solid;
    }

    span.slash {
        font-weight: 500%;
        font-size: 120%;
        color: red;
    }
</style>

STR;

        echo '<table>';

        $prev_path = $routes_array[0][100];

        foreach($routes_array as $key => $route)
        {
            if ($prev_path != $route[100])
            {
                $prev_path = $route[100];
                echo <<<STR
<tr class="delimiter">
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
STR;
            }

            $route[2] = str_replace('/', '<span class="slash"> / </span>', $route[2]);

            echo <<<STR
<tr>
<td>{$route[100]}</td>
<td>{$route[0]}</td>
<td>{$route[1]}</td>
<td>{$route[2]}</td>
<td>{$route[3]}</td>
<td>{$route[4]}</td>
<td>{$route[5]}</td>
</tr>
STR;

        }

        echo '</table>';

    }

}
