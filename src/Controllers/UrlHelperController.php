<?php

namespace Vicvk\Lib\Controllers;

use Illuminate\Routing\Controller;

class UrlHelperController extends Controller
{

	public function getSearchUrl(Request $request)
	{
        # Validate search request here
		#$this->validate($request, ['name' => 'required|min:20'], []);



        $params = $request->all();

        # NYI - it is probably (?) a good idea to encode url so that search engines never could see it
		return response()->json([ 'redirectUrl' => $this->makeSearchUrl($params) ]);
	}

	# this can be a separate service class CleanUrlsGenerator / UrlFactory
    # if no search criterias is specified, return "" (empty string)
    # NYI: in this case user should see a warning telling him to specify search criteria
	protected function makeSearchUrl(Array $params)
	{
        // use ksort() to sort the array of params by param name.
        // 'page' param should always be the last one (?)

        $route_name = $params['_route'];

        unset($params['_route']);
        unset($params['_token']);

////        # remove params which values are equal to empty string
////        $params = array_filter($params, function ($var) {
////            return $var != '';
////        });

        $new_params = [];

        # if there were any GET params in URL when search was clicked, these
        # params will be lost - and this is correct. Only preserve helper params
        # (except for page number) and of course the form search params
        foreach($params as $key=>$val)
        {
            if ($val != '') {
                $param_name_end = substr($key, strlen($key)-3);

                # we don't want pagination params here
                if ($param_name_end != '_pg') {
                    $new_params[$key] = $val;
                }
            }
        }

        if ($new_params == [])
        {
            $result = '';
        }
        else
        {
            ksort($new_params);

            $result = route($route_name) . '?' . http_build_query($new_params);
        }

        return $result;
	}


}

