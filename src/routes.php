<?php

//Route::pattern('id', '[0-9]+');
Route::pattern('numericId', '[0-9]+');
Route::pattern('pageNumber', '/page=[0-9]+');


Route::group(['prefix' => 'vicvk-lib/dev', 
              'as' => 'vicvk-lib-dev::', 
              'namespace' => 'Vicvk\Lib\Controllers'
             ], 
function()
{
    Route::get('/', 'DevController@index');

    Route::get('env', 'DevController@env');

    Route::get('routes', 'DevController@routes');
});

Route::group(['prefix' => 'vicvk-lib/helpers', 
              'as' => 'vicvk-lib-helpers::', 
              'namespace' => 'Vicvk\Lib\Controllers'
             ], 
function()
{
    Route::get('close-fancybox-and-refresh-parent', ['as'=>'close-fancybox-and-refresh-parent', 
                                                     'uses'=>'HelpersController@closeFancyboxAndRefreshParent']);


});

Route::post('search', ['as'=>'search', 'uses'=>'Vicvk\Lib\Controllers\UrlHelperController@getSearchUrl']);

