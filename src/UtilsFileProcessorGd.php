<?php

namespace Vicvk\Lib;

/*
<file_processor function="gd" jpeg_quality="50">
    <action do="resize" width="" height=""/>
    <action do="resize2" width="" height=""/>

    <action do="watermark" watermark_file="{$files}/watermarks/1.jpg" offset_x="" offset_y="" watermark_direction=""/>

    <action do="clip" x0="" y0="" x1="" y1="" />
    <action do="clip2" clip_width="" clip_height="" clip_direction="" offset_x="" offset_y="" />
</file_processor>


��� action="clip": (x0, y0) - ���������� �������� ������ ����, 
                   (x1, y1) - ���������� ������� ������� ����


clip2 ��������� �������� �� ������ ������������� � �������� ������� � ������� (clip_width, clip_height)
� ����� ��������� ���������� ������������� � �������� �������������� � ������ ����������� (clip_direction)
� � ������� ���������� (offset_x, offset_y)


NYI: jpeg_quality!

��������� �������� ��� ��������� direction:

"C" (Center) ����������� �� ����������� � �� ���������, ��������� offset_x, offset_y ������������

"N" (North)  �� ���������� offset_y �� �������� ����, ����������� �� �����������, �������� offset_x ������������
"S" (South)  �� ���������� offset_y �� ������� ����, ����������� �� �����������, �������� offset_x ������������
"W" (West)   �� ���������� offset_x �� ������ ����, ����������� �� ���������, �������� offset_y ������������
"E" (East)   �� ���������� offset_x �� ������� ����, ����������� �� ���������, �������� offset_y ������������

"NW" (North-West) �� ���������� offset_x �� ������ ����, �� ���������� offset_y �� �������� ����
"SW" (South-West) �� ���������� offset_x �� ������ ����, �� ���������� offset_y �� ������� ����
"NE" (North-East) �� ���������� offset_x �� ������� ����, �� ���������� offset_y �� �������� ����
"SE" (South-East) �� ���������� offset_x �� ������� ����, �� ���������� offset_y �� ������� ����

��� ��������� �������� direction �������� ���������� � �� ��� ����� ��������


*/

class UtilsFileProcessorGd
{


public static function fileProcessorGd(&$file, $extra_params, $extra_xml_elements)
{   
    $destination = 0;
    $watermark_destination = 0;
    $watermark_source = 0;
    $width = 0;  $height = 0;  $type = 0;
    $type = 0;
    $watermark_width = 0;  $watermark_height = 0;  $watermark_type = 0;
    $jpeg_quality = isset($extra_params['jpeg_quality']) ? $extra_params['jpeg_quality'] : 100;

    static::tmp_func_file_processor_gd_open_pic_file($file,$source,$width,$height,$type);

   
    foreach ($extra_xml_elements as $current_tag)
    {
        if(!isset($current_tag['do']))
        {
            trigger_error("Parameter DO must be set", E_USER_ERROR);
        }
        
        $current_tag['do'] = strtolower($current_tag['do']);
        
        #########################################################################################

        if($current_tag['do'] == 'resize')
        {
            static::tmp_func_file_processor_gd_resize($source,$destination,$current_tag,$width,$height);
            if($source != $destination)
            {
                imagedestroy($source);    
                $source = $destination;
            }
        }

        #########################################################################################

        if($current_tag['do'] == 'resize2')
        {
            static::tmp_func_file_processor_gd_resize2($source,$destination,$current_tag,$width,$height);
            if($source != $destination)
            {
                imagedestroy($source);    
                $source = $destination;
            }
        }

        #########################################################################################
        
        elseif($current_tag['do'] == 'watermark')
        {
            if(!isset($current_tag['watermark_file']))
            {
                trigger_error("Parameter watermark_file must be set", E_USER_ERROR);
            }            
           
            $watermark_file = $current_tag['watermark_file'];
            $watermark_file = resolve_upload_paths_dir($watermark_file);

            $offset_x = isset($current_tag['offset_x']) ? $current_tag['offset_x'] : 10;
            $offset_y = isset($current_tag['offset_y']) ? $current_tag['offset_y'] : 10;

            if(!isset($current_tag['watermark_direction']))
            {
                $watermark_direction = 'SE';
            }            
            else 
            {
                $watermark_direction = $current_tag['watermark_direction'];
                if(!in_array($watermark_direction,array('C','N','S','W','E','NW','SW','NE','SE')))
                {
                    trigger_error("Incorrect watermark direction", E_USER_ERROR);
                }
            }

            $opacity = isset($current_tag['opacity']) ? $current_tag['opacity'] : 100;
            

            static::tmp_func_file_processor_gd_open_pic_file($watermark_file,$watermark_source,$watermark_width,$watermark_height,$watermark_type);
            static::tmp_func_file_processor_gd_copy_watermark($source,$watermark_source,$width,$height,$watermark_width,$watermark_height,$offset_x,$offset_y,$watermark_direction,$opacity,$watermark_type);
            imagedestroy($watermark_source);
        }

        #########################################################################################

        elseif($current_tag['do'] == 'clip')
        {
            if(!isset($current_tag['x0'])||!isset($current_tag['y0'])||!isset($current_tag['x1'])||!isset($current_tag['y1']))
            {
                trigger_error("Clip parameters (x0,y0,x1,y1) must be set", E_USER_ERROR);
            }   
            if(($current_tag['x1']<$current_tag['x0'])||($current_tag['y1']<$current_tag['y0']))
            {
                trigger_error("x1 must be greater than x0 and y1 must be greater than y0", E_USER_ERROR);
            }              

////echo "width = $width#";
////echo "height = $height#";

            if($current_tag['x1']>$width)
            {
                $current_tag['x1'] = $width;
            }              
            if($current_tag['y1']>$height)
            {
                $current_tag['y1'] = $height;
            }              
            $dest_width = $current_tag['x1'] - $current_tag['x0'];
            $dest_height = $current_tag['y1'] - $current_tag['y0'];
            if(($dest_width == 0)||($dest_height == 0))
            {
                trigger_error("Width or height must not be equal zero", E_USER_ERROR);
            }
            $destination = imagecreatetruecolor($dest_width, $dest_height);
            imagecopy ($destination, $source,0, 0, $current_tag['x0'], $current_tag['y0'], $dest_width, $dest_height);
            imagedestroy($source);    
            $source = $destination;            
        }

        #########################################################################################

        elseif($current_tag['do'] == 'clip2')
        {
            # � ����� �� ������ ���� ����� ������ ���������� ����� ����������� ��������������,
            # ������ ���� ������������� ��� ������ ������ � ������ � ������������ � ������ �����������
            # � � ������ ��������� ���������

            $offset_x = isset($current_tag['offset_x']) ? $current_tag['offset_x'] : 0;
            $offset_y = isset($current_tag['offset_y']) ? $current_tag['offset_y'] : 0;

            if(!isset($current_tag['clip_direction']))
            {
                $clip_direction = 'C';
            }            
            else 
            {
                $clip_direction = $current_tag['clip_direction'];
                if(!in_array($clip_direction,array('C','N','S','W','E','NW','SW','NE','SE')))
                {
                    trigger_error("Incorrect clip direction", E_USER_ERROR);
                }
            }

            if(!isset($current_tag['clip_width'])||!isset($current_tag['clip_height']))
            {
                trigger_error("clip_width and clip_height must be set", E_USER_ERROR);
            }   

            # �� �� ����� �������� ������� ���� ��� ������ �������� �������� � ���� ��� ������
            # �������� ��������
            $clip_width  = ($current_tag['clip_width'] > $width) ? $width : $current_tag['clip_width'];
            $clip_height = ($current_tag['clip_height'] > $height) ? $height : $current_tag['clip_height'];

            # ������ ������ ������������:
            static::tmp_func_use_directions($width,$height,$clip_width,$clip_height,$offset_x,$offset_y,$clip_direction);

            $current_tag['x0'] = $offset_x;
            $current_tag['y0'] = $offset_y;

            $current_tag['x1'] = $offset_x + $clip_width;
            $current_tag['y1'] = $offset_y + $clip_height;

            #
            # � ����� ����������� ��� �� ����� ���� ��� � ��� ������� ������� 'clip'
            #
            if($current_tag['x1']>$width)
            {
                $current_tag['x1'] = $width;
            }              
            if($current_tag['y1']>$height)
            {
                $current_tag['y1'] = $height;
            }              
            $dest_width = $current_tag['x1'] - $current_tag['x0'];
            $dest_height = $current_tag['y1'] - $current_tag['y0'];
            if(($dest_width == 0)||($dest_height == 0))
            {
                trigger_error("Width or height must not be equal zero", E_USER_ERROR);
            }
            $destination = imagecreatetruecolor($dest_width, $dest_height);
            imagecopy ($destination, $source,0, 0, $current_tag['x0'], $current_tag['y0'], $dest_width, $dest_height);
            imagedestroy($source);    
            $source = $destination;            
        }
        
    }
    static::tmp_func_file_processor_gd_write_file($source,$file,$type,$jpeg_quality);
    imagedestroy($source);
    $source = 0;   
}   //public static function end

    //  ----------------------------------------

private static function tmp_func_use_directions($width,$height,$area_width,$area_height,&$offset_x,&$offset_y,$area_direction)
{
   switch ($area_direction)
   {  
        case 'N':
        { 
            $offset_x = ($width - $area_width)/2 ;
            break;
        }
        
        case 'S':
        {
            $offset_x = ($width - $area_width)/2 ;
            $offset_y = $height - $area_height - $offset_y;
            break;
        }
        
        case'W':          
        {
           $offset_y = ($height - $area_height)/2 ;
           break;
        }
        
        case'E':          
        {
           $offset_y = ($height - $area_height)/2 ;
           $offset_x = $width - $area_width - $offset_x;
           break;
        }
        case 'C':          
        {
           $offset_y = ($height - $area_height)/2 ;
           $offset_x = ($width - $area_width)/2;
           break;
        }
        
        case'SW':          
        {
            $offset_y = $height - $area_height - $offset_y ;
            break;
        }
        case'NE':          
        {
            $offset_x = $width - $area_width - $offset_x;
            break;
        }
        case'SE':          
        {
            $offset_x = $width - $area_width - $offset_x;
            $offset_y = $height - $area_height - $offset_y;
            break;
        }
   }

}

private static function tmp_func_file_processor_gd_copy_watermark
(
    &$source,
    &$watermark_source,
    $width,
    $height,
    $watermark_width,
    $watermark_height,
    $offset_x,
    $offset_y,
    $watermark_direction,
    $opacity,
    $watermark_type
)
{  
    static::tmp_func_use_directions($width, $height, $watermark_width, $watermark_height, $offset_x, $offset_y, $watermark_direction);

    //imagecopymerge ($dst_im,$src_im,$dst_x,$dst_y,$src_x,$src_y,$src_w,$src_h,$pct );
    if($watermark_type!=3)
    {
//echo '1111';
        imagecopymerge ($source,$watermark_source,$offset_x,$offset_y,0,0,$watermark_width,$watermark_height,$opacity);
    }
    else 
    {
//echo '2222';

///        $bg_cut = imagecreatetruecolor($watermark_width, $watermark_height);
///        imagecopy($bg_cut,$source,0,0, $offset_x, $offset_x,$watermark_width,$watermark_height);
///        $opacity = 100 - $opacity;
///        imagecopy($source,$watermark_source, $offset_x, $offset_x, 0, 0, $watermark_width,$watermark_height);
///        imagecopymerge($source,$bg_cut, $offset_x, $offset_x, 0, 0, $watermark_width,$watermark_height,$opacity);
///        imagedestroy($bg_cut);


        $bg_cut = imagecreatetruecolor($watermark_width, $watermark_height);
        imagecopy($bg_cut,$source,0,0, $offset_x, $offset_y,$watermark_width,$watermark_height);
        $opacity = 100 - $opacity;
        imagecopy($source,$watermark_source, $offset_x, $offset_y, 0, 0, $watermark_width,$watermark_height);
        imagecopymerge($source,$bg_cut, $offset_x, $offset_y, 0, 0, $watermark_width,$watermark_height,$opacity);
        imagedestroy($bg_cut);

    }

//exit;
    
}   //private static function end

private static function tmp_func_file_processor_gd_resize(&$source,&$destination,$current_tag,&$old_width,&$old_height)
{ 
    if(isset($current_tag['width'])) 
    {
        $new_width = $current_tag['width'];
    }
    else 
    {
        $new_width = 5000;        
    }

    if(isset($current_tag['height']))
    {
        $new_height = $current_tag['height'];
    }
    else 
    {
        $new_height = 5000;
    }

    if (!isset($new_width) && !isset($new_height))
    {
        trigger_error("Height or width must be set", E_USER_ERROR);
    }

    if (($new_height >= $old_height) && ($new_width >= $old_width)) 
    {
        # �.�. ����������� �������� �� �� �����
        $destination = $source;
    }
    else 
    {
        $x_ratio = $new_width / $old_width;
        $y_ratio = $new_height / $old_height;
        $ratio = min($x_ratio, $y_ratio);
        $dest_width = ceil($old_width * $ratio);
        $dest_height = ceil($old_height * $ratio);
        $destination = imagecreatetruecolor($dest_width, $dest_height);
        imagecopyresampled($destination, $source, 0, 0, 0, 0, $dest_width, $dest_height, $old_width, $old_height);
        $old_height = $dest_height;
        $old_width = $dest_width;
    }
}    

/*
������� ����� ������ resize2 �� �������� resize ����������� � ��� ��� �������������� ��������
�� ������� ���������� � ������������� � ��������������� ���������. ������ ����� ���������� 
���������: 
������������� � ��������� width, height ������ ���� ������ � �������������� ��������.

����� ����� ��������� �������� ������ � ������� clip2 ��������� ����� � ������� ���������
�������������� �������� ������ ���������� �������, ��������� ��������� ������� �� ������������
�������� � ����� ������� ������ ������ �������� �������. ��� ������ ������ ��� ����:

    <file_version name="thumb" path="{$files}/gallery_photo/">
        <file_processor function="gd" core_module="files">
        <action do="resize2" width="74" height="69" />
        <action do="clip2" clip_width="74" clip_height="69" clip_direction="C" />                
        </file_processor>    
    </file_version>

*/
private static function tmp_func_file_processor_gd_resize2(&$source,&$destination,$current_tag,&$old_width,&$old_height)
{ 
    if(isset($current_tag['width'])) 
    {
        $new_width = $current_tag['width'];
    }
    else 
    {
        trigger_error("Width must be set", E_USER_ERROR);
    }

    if(isset($current_tag['height']))
    {
        $new_height = $current_tag['height'];
    }
    else 
    {
        trigger_error("Height must be set", E_USER_ERROR);
    }

/*
echo "old_height = $old_height";
echo '#';
echo "old_width = $old_width";
echo '#';
echo "new_height = $new_height";
echo '#';
echo "new_width = $new_width";
echo '#';
exit;
*/


///    if ($old_height >= $old_width)
///    {
///        $new_height = 5000;
///    }
///    else
///    {
///        $new_width = 5000;
///    }



    if (($old_height/$new_height) >= ($old_width/$new_width))
    {
        $new_height = 5000;
    }
    else
    {
        $new_width = 5000;
    }


    if (($new_height >= $old_height) && ($new_width >= $old_width)) 
    {
        # �.�. ����������� �������� �� �� �����
        $destination = $source;
    }
    else 
    {
        $x_ratio = $new_width / $old_width;
        $y_ratio = $new_height / $old_height;
/*
echo "x_ratio = $x_ratio";
echo '#';
echo "y_ratio = $y_ratio";
exit;
*/
        $ratio = min($x_ratio, $y_ratio);
        $dest_width = ceil($old_width * $ratio);
        $dest_height = ceil($old_height * $ratio);
/*
echo "dest_width = $dest_width";
echo '#';
echo "dest_height = $dest_height";
exit;
*/
        $destination = imagecreatetruecolor($dest_width, $dest_height);
        imagecopyresampled($destination, $source, 0, 0, 0, 0, $dest_width, $dest_height, $old_width, $old_height);
        $old_height = $dest_height;
        $old_width = $dest_width;
    }
}    
    
private static function tmp_func_file_processor_gd_open_pic_file($file,&$source,&$width,&$height,&$type)
{
    global $config;

    $attr = getimagesize($file);


///echo '!!!!';
///echo $file;
///echo '!!!!';
//return;

    if (is_array($attr) && ($attr != array()))
    {
//echo '1111';
        # ��� � �������, ����� ������ �� ���� ���� ��������
        # ���� � ������ ������������ ��� ����� ���� �� ���������� ���� imagecreatefromjpeg()
        # ���������� �����������. �� � ������ ����������� ������ � ���.
    }
    else
    {
//echo '2222';
        # ����� � ������� �� ���. ��������� �������� ��� ���������������� imagemagick.
        # ���� ��� ���������� 'recoverable jpeg errors' �.�. �������� �� �������� ������ ��������
        # jpeg'�� �� ����������� ����� ����������� �� ������ ��������� - ������ ��� ����������� ����� 
        # ����� ����� ������ ���������������. � 5� PHP ������� � ����� �� ������ ���� ����� ����
        # ��������� jpeg ������ ������������ ����� ������������� ������, �� � ���� PHP
        # ��� �������� �� ��������. � ������� � imagemagic ��� ����� ��������.
        # NYI: � ������ ������� ��������� �� ����������� magicwand (�������������� ��������
        # ��� ��� ���������� ��� �����������) - ��� ���������������� � ����������, 
        # �� ���� �� ���� ��������������
        #
        # ����� ���� � ����������� ������ convert ����������� � ���� �� ����������� ����������

//echo $config['imagemagick']['convert'];

        if (isset($config['imagemagick']['convert']) && file_exists($config['imagemagick']['convert']))
        {
//echo '3333';
            # ������������ � PNG - ��� ������ ��� ������ ��������

            # ����� ��� ����� �� �������� / ��� \
            $file_parts = preg_split('/[\\\\\/]/', $file);
            $last_file_parts_idx = count($file_parts) - 1;
            $file_parts[$last_file_parts_idx] = '~~temp~~' . $file_parts[$last_file_parts_idx] . '.png';
            $file2 = join('/', $file_parts);

            $cmd = "{$config['imagemagick']['convert']} {$file} {$file2}";
            @exec($cmd);

            $cmd = "{$config['imagemagick']['convert']} {$file2} {$file}";
            @exec($cmd);

            # ��� ������������� ������ ��� ������� � ��� ����� �� ������� ������ ����
            # �� � ������� ������ ������� ~~temp~~ �����. �� ������ ����� �� ������ ������.
            @unlink($file2);

            # � ����� �������� ��������������� �������� getimagesize()
            $attr = getimagesize($file);
        }
        else
        {

//echo 'here';
//echo $file;

            # ��������� MagicWand

            # NYI - ��� � �� ����������! ���� ������ �������� �� ��� ��� �����, �� ������
            # ����� � �� ������.
            $resource = NewMagickWand();
            MagickReadImage($resource, $file); 
            MagickWriteImage($resource, $file);
            $attr = getimagesize($file);

////header( 'Content-Type: image/jpeg' );
////MagickEchoImageBlob( $resource );

        }
    }

//print_r($attr);

    list($width, $height, $type) = $attr;

/*
echo "\r\n\r\n\r\n\r\n\r\n\r\n\r\n";
echo '<br/><br/><br/>';
echo "\r\n\r\n\r\n\r\n\r\n\r\n\r\n";

echo $file;
echo '####';
echo $type;
echo '####';
*/

//    if ($type == '')
//    {
//        $type = IMAGETYPE_JPEG;
//    }

//exit;

///return;
    
    switch ($type)
    {
        case IMAGETYPE_GIF:
            $source = @imagecreatefromgif($file);
            if ( !$source )
            {
                trigger_error('GD library is set up without GIF files support on your server.', E_USER_ERROR);
            }
            break;

        case IMAGETYPE_JPEG:
            ini_set("gd.jpeg_ignore_warning", 1);
            $source = @imagecreatefromjpeg($file);
            if ( !$source )
            {
                trigger_error('GD library is set up without JPEG files support on your server.', E_USER_ERROR);
            }
            break;

        case IMAGETYPE_PNG:
            $source = @imagecreatefrompng($file);
            if ( !$source )
            {
                trigger_error('GD library is set up without PNG files support on your server.', E_USER_ERROR);
            }
            break;

        case IMAGETYPE_BMP:
            $source = @imagecreatefrombmp($file);   # �������-������� ����� ���� � PHP ������� imagecreatefrombmp?
            # � ��� ����� resize ������ ������ �� ��������

            if ( !$source )
            {
                trigger_error('GD library is set up without BMP files support on your server.', E_USER_ERROR);
            }
            break;

        default: 
            trigger_error('Cannot identify file type.', E_USER_ERROR);
    }
}

//----------------------------------  ������ �����
private static function tmp_func_file_processor_gd_write_file($source,$filename,$type,$quality)
{
    $success = 0;
    switch ($type)
    {
        case 1:
            if ( function_exists('imagegif') )
            {
                $success = imagegif($source, $filename);
            }
            else
            {
                trigger_error("GD library is set up without GIF files support on your server.", E_USER_ERROR);
            }
            break;
        case 2:
            if ( function_exists('imagejpeg') )
            {
                $success = imagejpeg($source, $filename,$quality);
            }
            else
            {
                trigger_error("GD library is set up without JPEG files support on your server.", E_USER_ERROR);
            }
            break;
        case 3:
            if ( function_exists('imagepng') )
            {
                $success = imagepng($source, $filename);
            }
            else
            {
                trigger_error("GD library is set up without PNG files support on your server.", E_USER_ERROR);
            }
    }

    //�������� ?
    if (!$success)
    {
        trigger_error("Can't write $filename", E_USER_ERROR);
    }

}


}
