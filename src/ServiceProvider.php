<?php

namespace Vicvk\Lib;

use Illuminate\Support\ServiceProvider as SP;

// NYI - should extend RouteServiceProvider

class ServiceProvider extends SP
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    public function boot()
    {
        # usage: return view('vicvk.lib::test');
        $this->loadViewsFrom(realpath(__DIR__.'/../views'), 'vicvk.lib');

        #
        # php artisan vendor:publish --provider="Vicvk\Lib\LibServiceProvider" --tag=views --force
        #
        $paths = config('view.paths');



# this works but don't need to publish anything yet
#        $this->publishes([
#            realpath(__DIR__.'/../views') => $paths[0] . '/vendor/vicvk.lib',
#        ], 'views');


        include (__DIR__.'/routes.php');

        #
        # php artisan vendor:publish --provider="Vicvk\Lib\LibServiceProvider" --tag=config --force
        #
        $this->publishes([
            realpath(__DIR__.'/../config/vicvk-lib.php') => config_path('vicvk-lib.php'),
        ], 'config');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
//echo 'hey';

        $this->app->singleton('MyHtmlHelper', function(){
            return new \Vicvk\Lib\MyHtmlHelper();
        });

        require_once(__DIR__.'/helpers.php');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }

}
