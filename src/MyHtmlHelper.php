<?php

namespace Vicvk\Lib;

use \Vicvk\Lib\MyHtmlHelper\MyLengthAwarePaginator;
use \Vicvk\Lib\MyHtmlHelper\MyBootstrapThreePresenter;
use \Vicvk\Lib\UtilsString;
use \League\Glide\Urls\UrlBuilderFactory;

class MyHtmlHelper
{
    protected $list;
    protected $paginatorTemplate = 'partials::bootstrap-4'; // this should be read from application config file

    # NYI - add ability to read MyHtmlHelper config options from application config file
    protected $listMode = 'ajax';
    protected $searchFormMode = 'ajax';
    protected $searchFormModeParam = '';
    protected $sortLinksMode = 'fragment';
    protected $paginatorLinksMode = 'fragment';

    protected $requiredMetaTitlePrefix = '';
    protected $optionalMetaTitlePrefix = '';
    protected $requiredMetaTitleSuffix = '';
    protected $optionalMetaTitleSuffix = '';

    public function __construct ()
    {
        $this->paginatorTemplate = config('vicvk-lib.paginator_template', 'partials::bootstrap-4');
    }

    public function formerLabelWidthLarge($width)
    {
        $labelWidths = \Former::getOption('TwitterBootstrap3.labelWidths');
        $labelWidths['large'] = $width;
        \Former::setOption('TwitterBootstrap3.labelWidths', $labelWidths);
    }

    public function formerLabelWidthSmall($width)
    {
        $labelWidths = \Former::getOption('TwitterBootstrap3.labelWidths');
        $labelWidths['small'] = $width;
        \Former::setOption('TwitterBootstrap3.labelWidths', $labelWidths);
    }

    public function formerLabelWidthMini($width)
    {
        $labelWidths = \Former::getOption('TwitterBootstrap3.labelWidths');
        $labelWidths['mini'] = $width;
        \Former::setOption('TwitterBootstrap3.labelWidths', $labelWidths);
    }


    public function useList(MyList $list)
    {
        $this->list = $list;
    }

    public function usePaginatorTemplate($paginatorTemplate)
    {
        $this->paginatorTemplate = $paginatorTemplate;
    }

    /**
     * This is a ready presets of how lists of items should function on this page.
     *
     * Example usage:
     *
     * <pre>
     * HH::listMode('classic')
     * HH::listMode('ajax')
     * </pre>
     * @param $listMode Possible values are: 'classic', 'ajax', 'custom'
     */
    public function listMode($listMode)
    {
        if (!in_array($listMode, ['classic', 'ajax', 'custom'])) {
            throw new \BadMethodCallException("\$listMode={$listMode} is not supported.");
        }

        if ($listMode == 'classic') {
            $this->searchFormMode('base_search_url');
            $this->sortLinksMode('query');
            $this->paginatorLinksMode('path');
        }
        elseif ($listMode == 'ajax') {
            $this->searchFormMode('ajax');
            $this->sortLinksMode('fragment');
            $this->paginatorLinksMode('fragment');
        }

        $this->listMode = $listMode;
    }

    public function getListMode()
    {
        return $this->listMode;
    }

    /**
     * Choose how search form should behave when submitted.
     *
     * Example usage:
     *
     * <pre>
     * HH::searchFormMode('search_route', 'contact-form.index')
     * HH::searchFormMode('base_search_url', route('contact-form.index'))
     * HH::searchFormMode('base_search_url')
     * HH::searchFormMode('ajax')
     * </pre>
     * @param $searchFormMode Possible values are: 'search_route', 'base_search_url', 'ajax'
     * @param null $searchFormModeParam
     */
    public function searchFormMode($searchFormMode, $searchFormModeParam='')
    {
        if (!in_array($searchFormMode, ['search_route', 'base_search_url', 'ajax'])) {
            throw new \BadMethodCallException("\$searchFormMode={$searchFormMode} is not supported.");
        }

        if (($searchFormMode == 'base_search_url') && ($searchFormModeParam == '')) {
            $uri = \Request::getUri();
            $query_start = strpos($uri, '?');
            if ($query_start === false) {
                $current_url = $uri;
            }
            else {
                $current_url = substr($uri, 0, $query_start);
            }

            $searchFormModeParam = $current_url;
        }

        $this->searchFormMode = $searchFormMode;
        $this->searchFormModeParam = $searchFormModeParam;

        $this->listMode = 'custom';
    }

    public function getSearchFormMode()
    {
        $result = [];

        $result['mode'] = $this->searchFormMode;
        $result['param'] = $this->searchFormModeParam;

        return $result;
    }

    /**
     * Choose how sort links should behave when clicked.
     *
     * Example usage:
     *
     * <pre>
     * HH::sortLinksMode('query')
     * HH::sortLinksMode('fragment')
     * </pre>
     * @param $sortLinksMode Possible values are: 'query', 'fragment'
     */
    public function sortLinksMode($sortLinksMode)
    {
        if (!in_array($sortLinksMode, ['query', 'fragment'])) {
            throw new \BadMethodCallException("\$sortLinksMode={$sortLinksMode} is not supported.");
        }

        $this->sortLinksMode = $sortLinksMode;
        $this->listMode = 'custom';
    }

    public function getSortLinksMode()
    {
        return $this->sortLinksMode;
    }

    /**
     * Choose how paginator links will be generated and behave when clicked.
     *
     * 'path' => page number will become part of url path, example: http://example.com/users/page=2
     * 'query' => page number will become part of url query, example: http://example.com/users?_pg=2
     * 'fragment' => page number will become part of url fragment, example: http://example.com/users#_pg=2
     *
     * Example usage:
     *
     * <pre>
     * HH::paginatorLinksMode('path')
     * HH::paginatorLinksMode('query')
     * HH::paginatorLinksMode('fragment')
     * </pre>
     * @param $paginatorLinksMode Possible values are: 'path', 'query', 'fragment'
     */
    public function paginatorLinksMode($paginatorLinksMode)
    {
        if (!in_array($paginatorLinksMode, ['path', 'query', 'fragment'])) {
            throw new \BadMethodCallException("\$paginatorLinksMode={$paginatorLinksMode} is not supported.");
        }

        $this->paginatorLinksMode = $paginatorLinksMode;
        $this->listMode = 'custom';
    }

    public function getPaginatorLinksMode()
    {
        return $this->paginatorLinksMode;
    }

    public function renderPaginator($paginatorTemplate = null, MyList $list = null)
    {
        if ($list == null) {
            $list = $this->list;
        }

        if ($paginatorTemplate == null) {
            $paginatorTemplate = $this->paginatorTemplate;
        }

        $path = \Request::getPathInfo(); # getPathInfo() doesn't not trim slashes from the end of path, like other methods do!

        $query = \Request::query();

        if ($this->getPaginatorLinksMode() == 'path')
        {
            unset($query['_pg']);
        }

        ksort($query); // always sort params by param name ascending



        $paginator = new MyLengthAwarePaginator($list->items, $list->totalItems, $list->getCurrentRecsPerPage(), $list->getCurrentPage(),
            [
                'path' => $path,
                'query' => $query,
                'paginatorLinksMode' => $this->getPaginatorLinksMode(),
                'pageName' => $list->getPageParam(),
            ]);

        $l = app();

        if (version_compare($l->version(), '5.3') >= 0) {  # if version is >= 5.3
            return $paginator->render($paginatorTemplate);
        }
        else {
            return $paginator->render(new MyBootstrapThreePresenter($paginator));
        }
    }

// NYI:
//
//    public function aTagDeleteByCheckboxes()
//    {
//        # This is a quick temporary solution. For full featured aTag group of methods, I should create
//        # ATag class similar to Button class
//        $html = <<<HTML
//<a class="mylib-delete-by-checkboxes-command" data-delete-url="http://derek/admin/blog-photo" href="#">
//<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
//Delete
//</a>
//HTML;
//
//        return $html;
//    }

    public function aTagToggleCheckboxes()
    {
        # This is a quick temporary solution. For full featured aTag group of methods, I should create
        # ATag class similar to Button class
        $html = '<a class="mylib-toggle-checkboxes" href="#">Select/Unselect All</a>';

        return $html;
    }


    public function dropdown()
    {
        $obj = app('\Vicvk\Lib\MyHtmlHelper\Dropdown');

        return $obj;
    }

    public function button()
    {
        $obj = app('\Vicvk\Lib\MyHtmlHelper\Button');

        return $obj;
    }

    public function buttonSortArrowDown()
    {
        $obj = app('\Vicvk\Lib\MyHtmlHelper\Button');
        $obj->setRole('SortArrowDown');

        return $obj;
    }

    public function buttonSortArrowUp()
    {
        $obj = app('\Vicvk\Lib\MyHtmlHelper\Button');
        $obj->setRole('SortArrowUp');

        return $obj;
    }

    public function buttonDeleteByCheckboxes()
    {
        $obj = app('\Vicvk\Lib\MyHtmlHelper\Button');
        $obj->setRole('DeleteByCheckboxes');

        return $obj;
    }

    public function buttonCreate()
    {
        $obj = app('\Vicvk\Lib\MyHtmlHelper\Button');
        $obj->setRole('Create');

        return $obj;
    }

    public function buttonEdit()
    {
        $obj = app('\Vicvk\Lib\MyHtmlHelper\Button');
        $obj->setRole('Edit');

        return $obj;
    }

    public function buttonPreview()
    {
        $obj = app('\Vicvk\Lib\MyHtmlHelper\Button');
        $obj->setRole('Preview');

        return $obj;
    }

    public function buttonSearch()
    {
        $obj = app('\Vicvk\Lib\MyHtmlHelper\Button');
        $obj->setRole('Search');

        return $obj;
    }

    public function buttonSubmit()
    {
        $obj = app('\Vicvk\Lib\MyHtmlHelper\Button');
        $obj->setRole('Submit');

        return $obj;
    }

    public function buttonCancel()
    {
        $obj = app('\Vicvk\Lib\MyHtmlHelper\Button');
        $obj->setRole('Cancel');

        return $obj;
    }


    public function wrapInFormGroup($whatToWrap)
    {
        $labelWidths = \Former::getOption('TwitterBootstrap3.labelWidths');

        $large = $labelWidths['large'];
        $small = $labelWidths['small'];

        $large_12 = 12 - $large;
        $small_12 = 12 - $small;

//print_r($x);


        $html = <<<HTML
<div class="form-group">
<div class="col-lg-offset-{$large} col-sm-offset-{$small} col-lg-{$large_12} col-sm-{$small_12}">
{$whatToWrap}
</div></div>
HTML;

        return $html;
    }


    public static function getReturnBackUrl()
    {
        $url = \Request::input('returnBack');

        if ($url == null) {  # if $url == '' then do not look for old value
            $url = \Request::old('returnBack');
        }

        return $url;
    }

    public function hiddenReturnBack($url = null)
    {
        if ($url == null) {
            $value = static::getReturnBackUrl();
        }
        else {
            $value = $url;
        }

        $value = htmlspecialchars($value);

        $html = <<<HTML
<input type="hidden" name="returnBack" value="{$value}">
HTML;

        return $html;
    }


    public function uploadPreview($uploadInputName, $uploadType, $form_v)
    {
        $request = app('request');

        $tmpUploadValueInputName = $uploadInputName.'_tmp_upload_value';
        $tmpUploadValue1 = $request->session()->getOldInput($tmpUploadValueInputName);
        $tmpUploadValue2 = (isset($form_v[$tmpUploadValueInputName]) && ($form_v[$tmpUploadValueInputName]!='')) ? $form_v[$tmpUploadValueInputName] : null;

        $tmpUploadValue = $tmpUploadValue1 ? $tmpUploadValue1 : $tmpUploadValue2;

        if ($tmpUploadValue == null) {

////            $html = 'null';
            $html = '';
        }
        else {

            $previewImageSrc = '';
            $previewImage = '';
            $previewGlideParams = ['w'=>'200', 'fit'=>'crop', 'sharp'=>'9'];
//            $previewGlideParams = ['w'=>'200', 'h'=>'220', 'fit'=>'crop', 'sharp'=>'9'];

            $deleteLink = '';

            $signkey = env('GLIDE_SIGN_KEY');

            if (substr($tmpUploadValue, 0, 18) == '?tmp_upload_value=') {

                # if we need to display a preview for temporary upload

                $xUploadValue = substr($tmpUploadValue, 18);
                $tmpUpload = static::readTempUploadRecord($xUploadValue);

                if (isset($tmpUpload['path'])) {

                    $form_temporary_uploads_root_rel_url = config("vicvk-lib.form_temporary_uploads_root_rel_url");

                    if ($form_temporary_uploads_root_rel_url)
                    {
                        # just for safety, ensure that there is no trailing slash
                        $form_temporary_uploads_root_rel_url = rtrim($form_temporary_uploads_root_rel_url, '/');

                        // Create an instance of the URL builder
                        $urlBuilder = UrlBuilderFactory::create($form_temporary_uploads_root_rel_url.'/', $signkey);

                        // Generate a URL
                        $previewImageSrc = $urlBuilder->getUrl($tmpUpload['path'], $previewGlideParams);

                        $previewImageSrc = '/glide' . $previewImageSrc;
                    }
                }
            }
            else {

                # if we need to display a preview for permanently stored file


                $original_root_rel_url = config("upload.types.{$uploadType}.file_versions.original.root_rel_url");

                if ($original_root_rel_url) {

                    # just for safety, ensure that there is no trailing slash
                    $original_root_rel_url = rtrim($original_root_rel_url, '/');

                    // Create an instance of the URL builder
                    $urlBuilder = UrlBuilderFactory::create($original_root_rel_url.'/', $signkey);

                    // Generate a URL
                    $previewImageSrc = $urlBuilder->getUrl($tmpUploadValue, $previewGlideParams);

                    $previewImageSrc = '/glide' . $previewImageSrc;
                }
            }

            if (substr($tmpUploadValue, 0, 6) != '?reset') {

                if ($previewImageSrc != '') {
                    $previewImage = "<img src=\"{$previewImageSrc}\">";
                }

                $deleteLink = "<a href=\"#\" onclick=\"mylib.resetUploadInput('{$uploadInputName}'); return false;\" id=\"mylib_{$uploadInputName}_delete_link\">Delete</a>";
            }

//// #{$tmpUploadValue}#

            $html = <<<HTML
<span id="mylib_{$uploadInputName}_tmp_upload_preview">
{$previewImage}
</span>
<input type="hidden" name="{$tmpUploadValueInputName}" value="{$tmpUploadValue}" id="mylib_{$tmpUploadValueInputName}">
{$deleteLink}
HTML;
        }

        return $html;
    }

    protected static function readTempUploadRecord($tempUploadId)
    {
        $table = config('vicvk-lib.form_temporary_uploads_db_table');
        $connection = config('vicvk-lib.form_temporary_uploads_db_connection');

        $results = \DB::connection($connection)->select("SELECT * FROM {$table} WHERE id = ? LIMIT 1", [$tempUploadId]);

        if (isset($results[0])) {
            $tempUpload = [
                'path' => $results[0]->path,
                'originalName' => $results[0]->original_name,
                'mimeType' => $results[0]->mime_type,
                'size' => $results[0]->size,
                'error' => $results[0]->error,
                'test' => (bool) $results[0]->test,
            ];
        }
        else {
            $tempUpload = null;
        }

        return $tempUpload;
    }


// ==========================================================================
// ==========================================================================
// ==========================================================================

    public function setRequiredMetaTitlePrefix($prefix)
    {
        $this->requiredMetaTitlePrefix = $prefix;
    }

    public function setOptionalMetaTitlePrefix($prefix)
    {
        $this->optionalMetaTitlePrefix = $prefix;
    }

    public function setRequiredMetaTitleSuffix($suffix)
    {
        $this->requiredMetaTitleSuffix = $suffix;
    }

    public function setOptionalMetaTitleSuffix($suffix)
    {
        $this->optionalMetaTitleSuffix = $suffix;
    }

    public function metaTitle($rawMetaTitleString)
    {
        $extra_params = [];
        $extra_params['required_prefix'] = $this->requiredMetaTitlePrefix;
        $extra_params['required_suffix'] = $this->requiredMetaTitleSuffix;

# NYI - need to recall how these optional string work in my function
#        $extra_params['optional_strings'] = [];
#        $extra_params['optional_strings'][]['prefix'] = $this->optionalMetaTitlePrefix;
#        $extra_params['optional_strings'][]['suffix'] = $this->optionalMetaTitleSuffix;

        return UtilsString::formatMetaTitle($rawMetaTitleString, 70, 75, $extra_params);
    }

    public function metaDescription($rawMetaDescriptionString)
    {
        $extra_params = [];

        return UtilsString::formatMetaDescription($rawMetaDescriptionString, 150, 160, $extra_params);
    }

    public function metaTitleTag($rawMetaTitleString)
    {
        if ($rawMetaTitleString == '') {
            return '';
        }
        else {
            $formattedMetaTitleString = $this->metaTitle($rawMetaTitleString);

            return '<title>' . htmlspecialchars($formattedMetaTitleString, ENT_QUOTES, 'UTF-8') . '</title>' . "\r\n";
        }
    }

    public function metaDescriptionTag($rawMetaDescriptionString)
    {
        if ($rawMetaDescriptionString == '') {
            return '';
        }
        else {
            $formattedMetaDescriptionString = $this->metaDescription($rawMetaDescriptionString);

            return '<meta name="description" content="' . htmlspecialchars($formattedMetaDescriptionString, ENT_QUOTES, 'UTF-8') . '">' . "\r\n";
        }
    }

    public function metaCanonicalTag($canonicalUrl)
    {
        if ($canonicalUrl == '') {
            return '';
        }
        else {
            return '<link rel="canonical" href="' . $canonicalUrl . '" />' . "\r\n";
        }
    }


    public static function selectOptions($form, $selectInputName)
    {
        $methodName = 'get' . ucfirst($selectInputName) . 'Options';

        $options = $form->$methodName();

        $result = [];

        foreach ($options as $value => $label)
        {
            $result[] = '<option value="' . $value . '"' . ((isset($form->v[$selectInputName]) && ($value === $form->v[$selectInputName])) ? ' selected' : ' ') . '>' . $label . '</option>';
        }

        return implode('', $result);
    }


// ==========================================================================
// ==========================================================================
// ==========================================================================

public function __call($method, $parameters)
    {
        if ($method == 'linkToSortBy') {
            $tag = $parameters[0];
////            $text = isset($parameters[1]) ? $parameters[1] : null;

            $obj = app('\Vicvk\Lib\MyHtmlHelper\LinkToSortBy');
            $obj->setTag($tag);
            $obj->setList($this->list);
////            $obj->setLabel($text);
            $obj->setLinkMode($this->getSortLinksMode());
            $obj->setCurrentUrl(\URL::full());

            return $obj;
        }
        elseif ($method == 'thToSortBy') {
            $tag = $parameters[0];
////            $text = isset($parameters[1]) ? $parameters[1] : null;

            $obj = app('\Vicvk\Lib\MyHtmlHelper\ThToSortBy');
            $obj->setTag($tag);
            $obj->setList($this->list);
////            $obj->setLabel($text);
            $obj->setLinkMode($this->getSortLinksMode());
            $obj->setCurrentUrl(\URL::full());

            return $obj;
        }
        else {
            throw new \BadMethodCallException("Method {$method} is not present in MyHtmlHelper class.");
        }

    }


}

