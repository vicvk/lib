<?php

function escape_html($string)
{
    return htmlspecialchars($string, ENT_QUOTES);
}

function get_enum_values($table, $column, $cache_lifetime_in_minutes=60, $force_cache_reload=false)
{
    $connection_name = \DB::connection()->getName();
    $database_name = \DB::connection()->getDatabaseName();

    $cache_prefix = config('cache.prefix', 'laravel');

    if ($cache_prefix == 'laravel') {
        $env = \App::environment();
        $debug = env('APP_DEBUG', '');
        $key = env('APP_KEY', '');

        $cache_prefix = "{$env}::{$key}::{$debug}::";
    }

    $cache_key = $cache_prefix . "vicvk/lib::get_enum_values::connection_name={$connection_name}::database_name={$database_name}::table={$table}::column={$column}";

////echo $cache_key;
////exit;


    if ($force_cache_reload) {
        \Cache::forget($cache_key);
    }

    return \Cache::remember($cache_key, $cache_lifetime_in_minutes, function() use ($table, $column) {

        $desc = \DB::select(\DB::raw("DESC `$table` `{$column}`"));
        $type = $desc[0]->Type;

        preg_match('/^\s*enum\s*\((.*)\)\s*$/i', $type, $matches);

        $values = [];
        foreach( explode(',', $matches[1]) as $value )
        {
            $v = trim($value, "'");
            $values[] = $v;
        }

        return $values;
    });
}


function get_enum_options($table, $column, $cache_lifetime_in_minutes=60, $force_cache_reload=false)
{
    $values = get_enum_values($table, $column, $cache_lifetime_in_minutes, $force_cache_reload);

    $options = [];

    foreach($values as $value) {
        $value = (string) $value;

        $options[$value] = $value;
    }

    return $options;
}

