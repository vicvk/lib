<?php
/*

This is deprecated in favor of MyModel class

namespace Vicvk\Lib\Traits;

# Actually I'm working on a MyModel class, so maybe we can move everything 
# from this trait to MyModel?


# The purpose of this trait is:
# 1) automatically validate model before saving to database



trait MyCrudModelTrait
{

    # NYI - when saving a model, the model should be automatically validated!!



///    # these rules should enforce data integrity when saving model to database
///    # and also they serve as a starting set of rules for validating forms,
///    # when form attempts to store or update model
///    #
///    # NYI!!!! Actually we can use the same rules() method to fetch rules
///    # both for validating a new model before inserting it to database,
///    # and also for validating an existing model, before updating it in database.
///    # We can distinguish between these two states insert/update by examining
///    # $this->id property - it will be non-empty only when in "update"
///    # state. I can even create a helper method $this->modelSaveMode()
///    # which would return "insert" or "update". Think about it.
///
///    public function rulesSave()
///    {
///        return [];
///    }
///
///    public function messagesSave()
///    {
///        return [];
///    }
///
///    public function rulesSearch() # important, it should be public
///    {
///        return [];
///    }
///
///    public function messagesSearch() # important, it should be public
///    {
///        return [];
///    }
///


///    public function searchByRequest(Array $requestInput=null)
///    {
///        return $this;
///    }


}

*/
