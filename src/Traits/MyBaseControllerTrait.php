<?php

namespace Vicvk\Lib\Traits;

# NYI - should create an authorize() method to check if user is
# illigable to perform operation

trait MyBaseControllerTrait
{
    protected $additionalViewVars = [];


    public static function canReturnBack()
    {
        $returnBack = MyBaseControllerTrait::getReturnBackUrl();

        return (($returnBack != null) && ($returnBack != ''));
    }

    public static function getReturnBackUrl()
    {
        $url = \Request::input('returnBack');

        if ($url == null) {  # if $url == '' then do not look for old value
            $url = \Request::old('returnBack');
        }

        return $url;
    }

    public static function returnBack()
    {
        if (MyBaseControllerTrait::canReturnBack()) {
            $returnBack = MyBaseControllerTrait::getReturnBackUrl();
            return redirect()->to($returnBack);
        }
    }

    protected function addViewVar($varName, $varValue)
    {
        $this->additionalViewVars[$varName] = $varValue;
    }

    protected function addRequestVar($varName, $varValue)
    {
        \Request::merge([$varName=>$varValue]);
    }


}

