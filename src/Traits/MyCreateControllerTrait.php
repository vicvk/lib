<?php

namespace Vicvk\Lib\Traits;

# NYI - should create an authorize() method to check if user is
# illigable to perform operation

trait MyCreateControllerTrait
{
# Seems like there is a bug in trait inheritance, see here:
# http://stackoverflow.com/questions/20382202/php-trait-method-conflicts-trait-inheritance-and-trait-hierarchies
# https://bugs.php.net/bug.php?id=63911
# that is why I'm using a workaround with static function in base trait: MyBaseControllerTrait::returnBack();
#    use MyBaseControllerTrait;



# protected $createFormClassName = '';
# protected $createView = '';
# protected $afterStoreRedirectUrl = '';

    protected function onAdditionalCreateViewVars()
    {
        # Alternatively to add a variable to Request input or 
        # to add a variable to template use any of these methods in the create method:
        # $this->addRequestVar('variable', $variable);
        # $this->addViewVar('variable', $variable);

        return [];
    }

    public function create()
    {
        # to add a variable to Request input:
        # $this->addRequestVar('variable', $variable);

        # to add a variable to view. This is supposed to replace onAdditionalCreateViewVars() method:
        # $this->addViewVar('variable', $variable);

        return $this->traitCreate();
    }

    public function traitCreate()
    {
        return view($this->createView, $this->handleCreate() + $this->onAdditionalCreateViewVars() + $this->additionalViewVars);
    }

    public function handleCreate()
    {
        if (empty($this->createFormClassName)) {
            throw new \BadMethodCallException('$this->createFormClassName property should be supplied.');
        }

        $form = app($this->createFormClassName);

        $form->create();

        return ['form' => $form];
    }

    public function store()
    {
        # to add a variable to Request input:
        # $this->addRequestVar('variable', $variable);

        return $this->traitStore();
    }

    public function traitStore()
    {
        $this->handleStore();

        return $this->onAfterStore();
    }

    public function handleStore()
    {
        if (empty($this->createFormClassName)) {
            throw new \BadMethodCallException('$this->createFormClassName property should be supplied.');
        }

        $form = app($this->createFormClassName);

        $form->store();
    }

    public function onAfterStore()
    {
        $redirectUrl = '';

        if (MyBaseControllerTrait::canReturnBack()) {
            $redirectUrl = MyBaseControllerTrait::getReturnBackUrl();
        }
        elseif (isset($this->afterStoreRedirectUrl) && ($this->afterStoreRedirectUrl != '')) {
            $redirectUrl = $this->afterStoreRedirectUrl;
        }

        if ($redirectUrl != '') {
            if (\Request::ajax() || \Request::wantsJson()) {
                return response()->json(['redirectUrl'=>$redirectUrl]);
            }
            else {
                return redirect()->to($redirectUrl);
            }
        }
    }

}

