<?php

namespace Vicvk\Lib\Traits;

# NYI - should create an authorize() method to check if user is
# illigable to perform operation

trait MyEditControllerTrait
{
# Seems like there is a bug in trait inheritance, see here:
# http://stackoverflow.com/questions/20382202/php-trait-method-conflicts-trait-inheritance-and-trait-hierarchies
# https://bugs.php.net/bug.php?id=63911
# that is why I'm using a workaround with static function in base trait: MyBaseControllerTrait::returnBack();
#    use MyBaseControllerTrait;


# protected $editFormClassName = '';
# protected $editView = '';
# protected $afterUpdateRedirectUrl = '';

    protected $editId = null;
    protected $updateId = null;

    protected function onAdditionalEditViewVars()
    {
        # Alternatively to add a variable to Request input or 
        # to add a variable to template use any of these methods in the edit method:
        # $this->addRequestVar('variable', $variable);
        # $this->addViewVar('variable', $variable);

        return [];
    }

    public function edit($id=null)
    {
        # to add a variable to Request input:
        # $this->addRequestVar('variable', $variable);

        # to add a variable to view. This is supposed to replace onAdditionalEditViewVars() method:
        # $this->addViewVar('variable', $variable);

        return $this->traitEdit($id);
    }

    public function traitEdit($id=null)
    {
        $this->editId = $id;

        return view($this->editView, $this->handleEdit($id)
                                     + $this->onAdditionalEditViewVars()
                                     + $this->additionalViewVars);
    }

    public function handleEdit($id=null)
    {
        if (empty($this->editFormClassName)) {
            throw new \BadMethodCallException('$this->editFormClassName property should be supplied.');
        }

        $form = app($this->editFormClassName);

        $form->edit($id);

        return ['form' => $form];
    }

    public function update($id=null)
    {
        # to add a variable to Request input:
        # $this->addRequestVar('variable', $variable);

        return $this->traitUpdate($id);
    }

    public function traitUpdate($id=null)
    {
        $this->updateId = $id;

        $this->handleUpdate($id);

        return $this->onAfterUpdate();
    }

    public function handleUpdate($id=null)
    {
        if (empty($this->editFormClassName)) {
            throw new \BadMethodCallException('$this->editFormClassName property should be supplied.');
        }

        $form = app($this->editFormClassName);

        $form->update($id);
    }

    public function onAfterUpdate()
    {
        $redirectUrl = '';

        if (MyBaseControllerTrait::canReturnBack()) {
            $redirectUrl = MyBaseControllerTrait::getReturnBackUrl();
        }
        elseif (isset($this->afterUpdateRedirectUrl) && ($this->afterUpdateRedirectUrl != '')) {
            $redirectUrl = $this->afterUpdateRedirectUrl;
        }

        if ($redirectUrl != '') {
            if (\Request::ajax() || \Request::wantsJson()) {
                return response()->json(['redirectUrl'=>$redirectUrl]);
            }
            else {
                return redirect()->to($redirectUrl);
            }
        }
    }



}

