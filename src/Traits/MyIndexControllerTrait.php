<?php

namespace Vicvk\Lib\Traits;


# NYI - should create an authorize() method to check if user is
# illigable to perform operation


//?is it needed?  use Illuminate\Http\JsonResponse;

trait MyIndexControllerTrait
{
# Seems like there is a bug in trait inheritance, see here:
# http://stackoverflow.com/questions/20382202/php-trait-method-conflicts-trait-inheritance-and-trait-hierarchies
# https://bugs.php.net/bug.php?id=63911
# that is why I'm using a workaround with static function in base trait: MyBaseControllerTrait::returnBack();
#    use MyBaseControllerTrait;


# protected $indexModelClassName = '';
# protected $indexSearchByRequestModelMethod = 'searchByRequest';  # set it to 'all' to use $model->all()
# protected $indexListClassName = '';
# protected $indexFormClassName = '';

# protected $indexView = '';
# protected $indexListItemsView = '';


    ##############################################################################################
    ##############################################################################################
    ##############################################################################################

    /*
     * Array returned from this function is added to array of variables that are passed
     * to view for rendering
     *
     *
     * !!!OR!!! the extra variables can be passed to index method liek this:
     * index($pageNum, $extraViewVars=[])   !!!
     * or both methods can be used - passed to index() and a callback
     */

    # maybe rename to Append Output
    # there is also this feature:  view()->share('key', 'value');  maybe there is a way to add
    # variable to views?
    protected function onAdditionalIndexViewVars()
    {
        # Alternatively to add a variable to Request input or 
        # to add a variable to template use any of these methods in the index method:
        # $this->addRequestVar('variable', $variable);
        # $this->addViewVar('variable', $variable);

        return [];
    }

    /**
     * Display a list of items that fit on the given $pageNumber.
     *
     * @param null $pageNumber
     * @return \Illuminate\View\View
     */

    # NYI - shouldn't this method be simpler? Like:
    # $search = app('MySearchList'); or - SearchableList, SearchByFormList or FormList
    #   MySearchList may be a child of MyList. It consists of List and Form that work together to search the list based on criterias entered into the form
    # or! Maybe all Lists should be searchable, hmmm, no, I may want to sometimes
    # use different Forms with the same List.
    # $search->index($pageNumber);
    # return view($this->indexView, ['form' => $search->form, 'list' => $search->list]);
    # this gives the following benefits - the index method of controller may
    # pass any custom values into the view.
    # Also the functionality of search list may be added to any controller,
    # not to just MyIndexController
    # there is even no need to extend the base mySearchList class, we can do it like this:
    # $search = app('MySearchList', ['indexListClassName' => 'ListingsSearchList', 'indexFormClassName' => 'ListingsSearchForm'])
    # $search->index($pageNumber);
    # return view(....)
    # Or even create a helper function:
    # $search = search('ListingsSearchList', 'ListingsSearchForm')
    # $search->index();  // so we move the index method to MySearch class which has a list and optionally a form
    # then all searches can be put into their separate directory: Searches
    # I.e. Searches/Listings/List.php
    #      Searches/Listings/Form.php
    #   ?? Searches/Listings/Search.php - this can be a MySearchList class with constructor
    # overwitten to inject __construct(Searches/Listings/List $list, Searches/Listings/Form $form)

    public function index($pageNumber=null)
    {
        $additionalRequestInput = [];

        # to add a variable to Request input:
        # $this->addRequestVar('variable', $variable);

        # to add a variable to view. This is supposed to replace onAdditionalIndexViewVars() method:
        # $this->addViewVar('variable', $variable);

        # to add a search form:
        # $t = $this->handleCreate();
        # $this->addViewVar('form', $t['form']);

        # $additionalRequestInput will be merged with Request input when passed to model's
        # searchByRequest(Array $requestInput=null) method, but in fact these variables won't
        # be added to Request input. To add a variable to Request input use:
        # $this->addRequestVar('variable', $variable);
        return $this->traitIndex($pageNumber, $additionalRequestInput);
    }

    public function traitIndex($pageNumber=null, $additionalRequestInput=[])
    {
        # $additionalRequestInput will be added to $requestInput when calling model's
        # searchByRequest(Array $requestInput=null) method as it was part of Request input.
        # But in fact these variables won't be added to Request input.
        # Maybe sometimes, perhaps very rare, this will be needed? When we want to pass
        # a search criteria to model, but don't wont to add it to Request input.


//echo '$pageNumber=' . $pageNumber;

//        echo 'index';
//        exit;

        $request = app('request');

///$data = $request->all();
///print_r($data);


        if ($pageNumber != null) {
            $pageNumberParts = explode('=', $pageNumber);
            \Request::merge(['_pg' => $pageNumberParts[1]]);
        }

        # But query not necessarily always needs to be an Eloquent model. It can be a query
        # or even an array.
        # NYI: the method name "searchByRequest" can be configurable if
        # the same model should be able to build more than one different
        # searches by request

        # NYI! check that searchByRequest() method exists on the model, if not, simply
        # return $model->all()

        $model = app($this->indexModelClassName);

        if (!property_exists($this, 'indexSearchByRequestModelMethod')) {
            $this->indexSearchByRequestModelMethod = 'searchByRequest';
        }

        if ($this->indexSearchByRequestModelMethod == '') {
           throw new \BadMethodCallException('indexSearchByRequestModelMethod property shouldn\'t be an empty string');
        }
        else {
            if (method_exists($model, $this->indexSearchByRequestModelMethod)) {
                $requestInput = $request->all();
                $requestInput = array_merge($requestInput, $additionalRequestInput);

                $query = $model->{$this->indexSearchByRequestModelMethod}($requestInput);
            }
            else {
               throw new \BadMethodCallException('Method defined by indexSearchByRequestModelMethod property doesn\'t exist on the Model');
            }
        }


        $list = app($this->indexListClassName);
        $list->fromQuery($query);

        if (!empty($this->indexFormClassName)) {
            $form = app($this->indexFormClassName);
        }
        else {
            # NYI!!! But do I always need a form to perform a search?? Actually its a Model that actually
            # finds items, the form is not always necessary, only when I want to display a search form
            # to user
/// I don't always do a search by form
///            throw new \BadMethodCallException('Either $this->indexFormClassName or $this->formClassName property should be supplied.');

            $form = null;
        }

        if (is_object($form)) {

            $form->receiveRequestInput();

            $validator = \Validator::make($form->v, $form->rulesSearch(), $form->messagesSearch());

            # in case when validation fails we don't want to redirect to previous URL
            # as validate() method does. That is why we need to respond manually to
            # validation errors.
            if ($validator->fails())
            {
                $errors = $validator->errors()->getMessages();

                if ($request->ajax() || $request->wantsJson()) {
                    return new JsonResponse($errors, 422);
                }
                else {
                    # populate Former with errors. This is how it is done when we render a view
                    # on failed validation (no redirection)
                    \Former::withErrors($validator);

                    return view($this->indexView,
                                ['list' => $list, 'form' => $form] + $this->onAdditionalIndexViewVars() + $this->additionalViewVars)
                        ->withErrors($validator);
                }
            }
            else {
                if ($request->ajax() || $request->wantsJson()) {

                    # NYI - we probably also need to return a max page number,
                    # and then in browser we should check if the current page
                    # doesn't exceed the max mage number, and if it does, the browser
                    # should adjust the current page number and re-require list items data
                    $html_list_items = view($this->indexListItemsView, ['list' => $list] + $this->onAdditionalIndexViewVars() + $this->additionalViewVars)->render();

                    $html_list_paginator = app('MyHtmlHelper')->renderPaginator(null, $list);

                    return response()->json(['items_html' => $html_list_items,
                                             'total_items_count' => $list->totalItems,
                                             'paginator_html'=> $html_list_paginator,
                                             'last_page_number'=> $list->getLastPageNumber(),
                    ]);
                }
                else {

                    return view($this->indexView, ['list' => $list, 'form' => $form] + $this->onAdditionalIndexViewVars() + $this->additionalViewVars);
                }
            }
        }
        else {
            if ($request->ajax() || $request->wantsJson()) {

                # NYI - we probably also need to return a max page number,
                # and then in browser we should check if the current page
                # doesn't exceed the max mage number, and if it does, the browser
                # should adjust the current page number and re-require list items data
                $html_list_items = view($this->indexListItemsView, ['list' => $list] + $this->onAdditionalIndexViewVars() + $this->additionalViewVars)->render();

                $html_list_paginator = app('MyHtmlHelper')->renderPaginator(null, $list);

                return response()->json(['items_html' => $html_list_items,
                                         'total_items_count' => $list->totalItems,
                                         'paginator_html'=> $html_list_paginator,
                                         'last_page_number'=> $list->getLastPageNumber(),
                                        ]);
            }
            else {
                return view($this->indexView, ['list' => $list] + $this->onAdditionalIndexViewVars() + $this->additionalViewVars);
            }
        }
    }



}

