<?php

namespace Vicvk\Lib\Traits;

# NYI - should create an authorize() method to check if user is
# illigable to perform operation

// we shouldn't use form class for destroyMultiple() or destroy() because sometimes
// we will need to be able to deleteMultiple but there will be no
// form, because no records are created or updated via a form.

# NYI - to avoid indexing destroy URLs by Search Engines,
# when no ids are given, or when request contains anything else
# except for ids, output 404 error


trait MyDestroyControllerTrait
{
# Seems like there is a bug in trait inheritance, see here:
# http://stackoverflow.com/questions/20382202/php-trait-method-conflicts-trait-inheritance-and-trait-hierarchies
# https://bugs.php.net/bug.php?id=63911
# that is why I'm using a workaround with static function in base trait: MyBaseControllerTrait::returnBack();
#    use MyBaseControllerTrait;


# protected $destroyModelClassName = '';


    public function destroyMultiple()
    {
        return $this->traitDestroyMultiple();
    }

    # destroy and destroyMultiple actually do the same thing.
    # so their implementations will be very similar
    public function traitDestroyMultiple()
    {
///echo 'destroyMultiple';

        $ids = \Request::input('ids');

///        print_r($ids);

        # NYI: !!! this method is not yet implemented, its just a placeholder
        $this->destroyMultipleValidate($ids);
//        $this->validate(app('request'), ['_name' => 'required|max:50'], []);

        $this->onDestroyMultiple($ids);

///exit;


        # NYI - should also validate delete request
        # validate $ids probably
        # probably we should validate for destroy each $id individually
        # and then run multiple destroy only if all individual validations passed



    }

    public function destroy($id)
    {
        # NYI !!!
        //
    }



    protected function destroyMultipleValidate($ids)
    {
        # NYI!
/*
    ???
        foreach($ids as $id) {
            $this->destroyValidate($id);
        }
*/
    }


    # can be overriden in case if some special processing is needed.
    protected function onDestroyMultiple($ids)
    {
        $model = app($this->destroyModelClassName);
        $model->destroy($ids);
    }


}

