<?php

namespace Vicvk\Lib\Traits;

use Vicvk\Lib\Formatter;

trait FieldsFormattingTrait
{
    var $formatterObj;

/*
 *
 *
This is a function, because implementing it as a function (instead of a predefined array) 
allows to dynamically change formatting rules depending on certain conditions (for example 
depending on the values of certain model fields).
And also allows to inherit formatting rules in the descendant classes.

    public function fieldsFormatting()
    {
        return [
            'date_created' => 
                          'extension1|extension2:paramA:paramB'
                              |
                          ['extension1', 'extension2:paramA:paramB', Callable]
                              |
                          # Callable as anonymous function
                          function($fieldName, $fieldValue, $dataObj, Formatter $formatterObj, Array $extraParams) { ... }
                              |
                          # Callable as object method
                          [$this, 'methodName']
                              |
                          # Callable as class static method
                          ['ClassName', 'staticMethodName']  ****this is not supported: 'ClassName::staticMethodName'
        ];
    }

    public function fieldsAliases()
    {
        return [
            'date_created2' => 'date_created',
        ];
    }
*/

    public function f($fieldName)
    {
        if (!is_object($this->formatterObj)) {
            $this->formatterObj = new Formatter($this);
        }

        return $this->formatterObj->format($fieldName);
    }

}

