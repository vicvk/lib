<?php

namespace Vicvk\Lib;

/*

Maybe implement it as a trait??

Also! this class should:

1) NYI: automatically validate model before saving to database

2) implement fieldsMapping so that I could read or write say "id" property and it would
automatically map to say "listingsdb_id"

*/


use Illuminate\Database\Eloquent\Model;
use Vicvk\Lib\Traits\FieldsFormattingTrait;

abstract class MyModel extends Model
{
    # This is how to define default values for attributes when model is created.
    # Or! when we need to calculate the default values, this array can be set inside
    # the model constructor.
    #
    protected $defaultAttributes = [
        //'title' => '1111',
        //'field_name_is_visible' => 'Yes',
    ];

    public function __construct(array $attributes = [])
    {
        # my opinion is that the parent class constructor should be called
        # prior to setting default values.
        # Btw the original Eloquent Model constructor fires the booting
        # and booted events _prior_ to filling the model with $attributes
        parent::__construct($attributes);

        ##########################################################################
        #  Set default values, when the new model instance is created

        $this->setRawAttributes(array_merge($this->attributes, $this->defaultAttributes, $attributes), true);
    }


    //////////////////////////////////////////////////////////////////////////////////////

    use FieldsFormattingTrait;

    public function fieldsFormatting() 
    {
        return [
            'created_at' => 'date',
            'updated_at' => 'date',

            #'created_at_datetime' => 'datetime',
        ];
    }

    public function fieldsAliases()
    {
        return [
            #'created_at_datetime' => 'created_at',
        ];
    }

    // this sets date fields such as created_at, updated_at, deleted_at to be stored
    // as a unix time in bigint(20) fields
    public function getDateFormat()
    {
        return 'U';
    }


/*
In order to implement fields mapping: (prototype)


    var $fieldsMapping = [
        'price' => 'Price',
        'amount' => 'Amount_old',
    ];

    public function __get($key)
    {
        if (array_key_exists($key, $this->attributes) || $this->hasGetMutator($key)) {
            return parent::__get($key);
        }

        if () { # check in $this->fieldsMapping
            // if mapped $key exists in $this->attribute, call the parent __get with mapped $key
        }
        elseif () { # check attribute name snake_case ==> camelCase
            // if camel case $key exists in $this->attribute, fall back to parent __get with mapped $key
        }


        # fall back to parent
        return parent::__get($key);
    }

    // same with setters
    public function __set($key)
    {

    }
*/



}
