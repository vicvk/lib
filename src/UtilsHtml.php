<?php

namespace Vicvk\Lib;

class UtilsHtml
{

# $url ������ ���� ���������� ����� � �������� �������� �������� ��������:  
# http://popmech.ru/123.html
public static function extractImgTags($html, $url='')
{
    $html = UtilsHtml::stripHtmlComments($html);

    $img_array = array();

    $base_path = '';
    $base_domain = '';

    if ($url != '')
    {
        $url_parts = parse_url($url);

        $base_path = isset($url_parts['path']) ? $url_parts['path'] : '/';

        if (substr($base_path, -1) != '/')
        {
            $base_path = dirname($base_path);
        }

        if (substr($base_path, -1) != '/')
        {
            $base_path = $base_path . '/';
        }

        $base_domain = $url_parts['scheme'] . '://' . $url_parts['host'];

//echo '#' . $base_path . '#';
//echo '#' . $base_domain . '#';

//echo '#' . $url_parts['path'] . '#';

//echo '#' . dirname($url_parts['path']) . '#';



    }

    preg_match_all('/(\<img\s[^\<\>]+\>)/i', $html, $matches, PREG_PATTERN_ORDER);

    $matches = $matches[0];

    foreach($matches as $whole_tag)
    {
        if (preg_match('/\ssrc\s*\=\s*(?:(?:\"([^\"]*)\")|(?:\'([^\']*)\'))/i', $whole_tag, $m))
        {
            $tag_src = ($m[1]!='') ? $m[1] : (isset($m[2]) ? $m[2] : '');

            # ignore images with these characters in src
            if (strpbrk($tag_src, '\'"') !== false)
            {
                continue;
            }



///$tag_src = '/images/rek/728_90_1354191675.gif';

            # ������ ����� ��������� ���� ��� � ����������� ����:
            if (substr($tag_src, 0, 2) == '//')
            {
                $tag_src = 'http:' . $tag_src;
            }
            elseif (!preg_match('/^https?\:\/\//i', $tag_src))
            {
                if (substr($tag_src, 0, 1) == '/')
                {
                }
                else
                {
                    $tag_src = $base_path . $tag_src;

//echo '####';
//echo "\$tag_src = $tag_src";
//echo '####';

                    $tag_src = UtilsString::truepath($tag_src);
                }
                $tag_src = $base_domain . $tag_src;
            }
        }
        else
        {
            $tag_src = '';
        }

        if (preg_match('/\sstyle\s*\=\s*(?:(?:\"([^\"]*)\")|(?:\'([^\']*)\'))/i', $whole_tag, $m))
        {
            $tag_style = ($m[1]!='') ? $m[1] : $m[2];
        }
        else
        {
            $tag_style = '';
        }

        if (preg_match('/\swidth\s*\=\s*(?:(?:\"([^\"]*)\")|(?:\'([^\']*)\')|(?:([0-9]+)))/i', $whole_tag, $m))
        {
            $tag_width = ($m[1]!='') ? $m[1] : ( ($m[2]!='') ? $m[2] : $m[3] );
        }
        else
        {
            $tag_width = '';
        }

        if (preg_match('/\sheight\s*\=\s*(?:(?:\"([^\"]*)\")|(?:\'([^\']*)\')|(?:([0-9]+)))/i', $whole_tag, $m))
        {
            $tag_height = ($m[1]!='') ? $m[1] : ( ($m[2]!='') ? $m[2] : $m[3] );
        }
        else
        {
            $tag_height = '';
        }

        # '@'.$tag_style  '@' �������� ����� ��������������� (?:[^\-]) � ������ ������
        if (preg_match('/(?:[^\-\w])width\s*\:\s*([0-9]+)\s*(?:px)?\;?/i', '@'.$tag_style, $m))
        {
            $tag_style_width = $m[1];
        }
        else
        {
            $tag_style_width = '';
        }

        # '@'.$tag_style  '@' �������� ����� ��������������� (?:[^\-]) � ������ ������
        if (preg_match('/(?:[^\-\w])height\s*\:\s*([0-9]+)\s*(?:px)?\;?/i', '@'.$tag_style, $m))
        {
            $tag_style_height = $m[1];
        }
        else
        {
            $tag_style_height = '';
        }

        $width = ($tag_style_width != '') ? $tag_style_width : $tag_width;
        $height = ($tag_style_height != '') ? $tag_style_height : $tag_height;

        $img_array[] = array
        (
            'img_tag' => $whole_tag,
            'src' => $tag_src,
            'style' => $tag_style,
            'width' => $width,
            'height' => $height,
        );
//break;
    }

    return $img_array;
}


public static function extractMetaTitle($html)
{
    $html = UtilsHtml::stripHtmlComments($html);

    if (preg_match("/\<title[^\>]*\>(.*)\<\/title\>/i", $html, $m))
    {
        return $m[1];
    }
    else
    {
        return '';
    }
}

# � PHP ���� ������� get_meta_data() �� ��� ����� ����� ��� ����������� ����������
public static function extractMetaDescription($html)
{
    $html = UtilsHtml::stripHtmlComments($html);

    # <meta name="description" content="mootools shell, easy test you snippets before implementing"/>

    if (preg_match("/\<meta(?:(?:\s+)|(?:\s[^\>]*\s))name\s*\=\s*(?:(?:\"description\")|(?:\'description\'))[^\>]*\scontent\s*\=\s*(?:(?:\"([^\"]*)\")|(?:\'([^\']*)\'))/i", $html, $m))
    {
        $result = ($m[1]!='') ? $m[1] : (isset($m[2]) ? $m[2] : '');
    }
    else
    {
        $result = '';
    }

    return $result;
}


public static function stripHtmlComments($html)
{
    $result = preg_replace("/<!--.*-->/U", '', $html);
    return $result;
}


public static function plainTextFromHtml($html)
{
    ############################################################################################
    # �������� plain text ������ ������

    $result = $html;

    # ������ �������� �����, ������� ���� � html ���� ����������, �.�. �������
    # ��� �������� ����� �� ����������
    # NYI - � ����� �� ������ �� ����������??
    $result = str_replace("\r", '', $result);
    $result = str_replace("\n", '', $result);

    # ������� ���� <br/> �� ������ \r
    $result = preg_replace('/\<br[^\>]*\>/i', "\r", $result);

    # ��� ��� ���� ��������� ���� � ������ ����� �������: </p>   <p>  ������� �� ������� �������
    # ������, �� ���� ������ ������ ����������.
    $result = preg_replace('/\<\/p[^\>]*\>\s*\<p[^\>]*\>/i', "\r\r", $result);
    # ��� ��� �������� ����� ����������� ��� ������ ����� div - ������� �� <br/>
    $result = preg_replace('/\<\/div[^\>]*\>/i', "\r\r", $result);

    # ������ ��� ����

    # ����� ����� ���������� �� ��� ��� ��� ��� ������ ���� iframe ����� �������.
    $result = preg_replace('/\<iframe[^>]*\>.*\<\/iframe\>/i', ' ', $result);

    #$result = strip_tags($result); - ����� ������������ ���������� ��������� - ��������� �������������
    $result = preg_replace('/\<[^>]*?\>/', ' ', $result);
    $result = str_replace("&nbsp;", ' ', $result);

    # ���� ���� ����� ��� 2 ������ ������ ��������� �����, �� ������� �� �� ��� ������ ������ ��������� �����
    $result = preg_replace("/(\r\s*)(\r\s*)(\r\s*)+/i", "\r\r", $result);

    # ������ � ������ �������� ����� ���������
    $result = preg_replace("/^[\r\s]+/i", '', $result);

    # ��������� � ������ �������� ����� ���������
    $result = preg_replace("/[\r\s]+$/i", '', $result);

    # ��������� ��������� �������:
    $result = preg_replace("/\ \ +/i", ' ', $result);
    $result = trim($result);

    return $result;
}



# NYI - ����� �������� ������� extract_tag_attributes
# ����� �� ������ ����� ����� ������� ����:
#        if (preg_match('/\ssrc\s*\=\s*(?:(?:\"([^\"]*)\")|(?:\'([^\']*)\'))/i', $whole_tag, $m))
#        {
#            $tag_src = ($m[1]!='') ? $m[1] : (isset($m[2]) ? $m[2] : '');



}

