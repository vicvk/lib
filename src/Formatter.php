<?php

namespace Vicvk\Lib;

use Vicvk\Lib\UtilsString;

class Formatter
{
    private $dataObj;

    private $extensions = [];

    public function __construct($dataObj)
    {
        $this->dataObj = $dataObj;

        $this->extend('lowercase', ['Vicvk\Lib\Formatter', 'formattingLowerCase']);

        $this->extend('substr', ['Vicvk\Lib\Formatter', 'formattingSubStr']);

        # Example: Dec 9, 2017
        $this->extend('date', ['Vicvk\Lib\Formatter', 'formattingDate']);

        # Example: 7:36 AM, Dec 9, 2017
        $this->extend('datetime', ['Vicvk\Lib\Formatter', 'formattingDateTime']);

        # Example: Sunday, Dec 9, 2017
        $this->extend('dow_date', ['Vicvk\Lib\Formatter', 'formattingDowDate']);
    }


    protected static function formattingLowerCase($fieldName, $fieldValue, $dataObj, Formatter $formatterObj, Array $extraParams=[])
    {
        return strtolower($fieldValue);
    }

    protected static function formattingSubStr($fieldName, $fieldValue, $dataObj, Formatter $formatterObj, Array $extraParams=[])
    {
        $start = 0;
        $length = strlen($fieldValue);

        if (isset($extraParams[0])) {
            $start = $extraParams[0];

            if (isset($extraParams[1])) {
                $length = $extraParams[1];
            }
        }

        return substr($fieldValue, $start, $length);
    }

    protected static function formattingDate($fieldName, $fieldValue, $dataObj, Formatter $formatterObj, Array $extraParams=[])
    {
        return UtilsString::formatDate($fieldValue, '%b %e, %Y');
    }

    protected static function formattingDateTime($fieldName, $fieldValue, $dataObj, Formatter $formatterObj, Array $extraParams=[])
    {
        return UtilsString::formatDateTime($fieldValue, '%l:%M %p, %b %e, %Y');
    }

    protected static function formattingDowDate($fieldName, $fieldValue, $dataObj, Formatter $formatterObj, Array $extraParams=[])
    {
        return UtilsString::formatDate($fieldValue, '%A, %b %e, %Y');
    }

    /*
     *
     * $implementation =
     *                    'extension1|extension2:paramA:paramB'
     *                        |
     *                    ['extension1', 'extension2:paramA:paramB', Callable]
     *                        |
     *                    # Callable as anonymous function
     *                    function($fieldName, $fieldValue, $dataObj, Formatter $formatterObj, Array $extraParams=[]) { ... }
     *                        |
     *                    # Callable as object method
     *                    [$this, 'methodName']
     *                        |
     *                    # Callable as class static method
     *                    ['className', 'staticMethodName']   ****this is not supported: 'ClassName::staticMethodName'
     *
     */

    public function extend($formattingRuleName, $implementation)
    {
        $this->extensions[$formattingRuleName] = $implementation;
    }

    public function format($fieldName)
    {
        # I don't cache fieldsAliases array. See comment below on why I also don't cache fieldsFormatting
        #
        $fieldsAliases = $this->dataObj->fieldsAliases();

        if (isset($fieldsAliases[$fieldName])) {
            $takeDataFromThisField = $fieldsAliases[$fieldName];

            $fieldValue = $this->dataObj->{$takeDataFromThisField};
        }
        else {
            $fieldValue = $this->dataObj->{$fieldName};
        }


        # I don't cache fieldsFormatting array and recalcalate it every time when we need to format
        # a field. This is because these fieldsFormatting rules may depend on the object's
        # attributes at the moment when formatting takes place.
        #
        $fieldsFormatting = $this->dataObj->fieldsFormatting();

        if (isset($fieldsFormatting[$fieldName])) {
            $formattingRule = $fieldsFormatting[$fieldName];
            $extraParams = [];

////echo '#';
////echo $formattingRule;
////echo '#';

            $fieldValue = $this->executeFormattingRule($formattingRule, $fieldName, $fieldValue, $this->dataObj, $this, $extraParams);
        }
        else {
            # if there are no formatting rules defined for this field,
            # then simply return its value
        }


        return $fieldValue;
    }

    protected function executeFormattingRule($formattingRule, $fieldName, $fieldValue, $dataObj, Formatter $formatterObj, Array $extraParams=[])
    {
///echo "begin: executeFormattingRule#{$fieldName}#";
///print_r($formattingRule);
///
///echo "\r\n\r\n";
///
///$isCallable = is_callable($formattingRule, false, $callable_name);
/////$isCallable = is_callable($formattingRule, true, $callable_name);
///
///if ($isCallable) {
///    echo "callable, callable_name={$callable_name}";
///}
///else {
///    echo "NOT callable";
///}
///
///echo "\r\n\r\n";


        if (is_string($formattingRule)) {
            # $formattingRule is a string

            $ruleSet = explode('|', $formattingRule);
        }
        else {

            $isCallable = is_callable($formattingRule, false, $callableName);

            if (is_array($formattingRule) && (!$isCallable)) {

                $ruleSet = $formattingRule;
            }
            elseif ($isCallable) {

                $ruleSet = [];
                $ruleSet[] = $formattingRule;
            }
            else {

                throw new \UnexpectedValueException(
                    "Unknown rule type"
                );
            }

        }

        foreach($ruleSet as $rule) {

            if (is_string($rule)) {

                $x = explode(':', $rule);
                $y = array_shift($x);

                if (isset($formatterObj->extensions[$y])) {
                    $z = $formatterObj->extensions[$y];

////echo '@' . $z . '@';
///print_r($x);


                    $fieldValue = $this->executeFormattingRule($z, $fieldName, $fieldValue, $dataObj, $formatterObj, $x);
                }
                else {

                    throw new \UnexpectedValueException(
                        "Rule {$y} is not defined"
                    );
                }
            }
            else {

                $isCallable = is_callable($rule, false, $callableName);

                if ($isCallable) {

///                    $fieldValue = $rule($fieldName, $fieldValue, $dataObj, $formatterObj, $extraParams);

                    $fieldValue = call_user_func($rule, $fieldName, $fieldValue, $dataObj, $formatterObj, $extraParams);

                }
                else {

                    throw new \UnexpectedValueException(
                        "Unknown rule type"
                    );
                }

            }

        }

        return $fieldValue;
    }
}


