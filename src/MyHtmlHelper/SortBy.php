<?php

namespace Vicvk\Lib\MyHtmlHelper;

use \Vicvk\Lib\MyList;

abstract class SortBy {

    protected $tag;
    protected $label = null;
    protected $tooltip = null;

    protected $list;
    protected $currentUrl = null;
    protected $classes = [];
    protected $linkMode = '';

    public function setLinkMode($linkMode)
    {
        $this->linkMode = $linkMode;
    }

    public function getLinkMode()
    {
        if (empty($this->linkMode)) {
            $result = '';
        }
        else {
            $result = $this->linkMode;
        }

        return $result;
    }

    /**
     * @param mixed $tag
     */
    public function setTag($tag)
    {
        $this->tag = $tag;
    }

    public function getTag()
    {
        return $this->tag;
    }

    /**
     * @param \Vicvk\Lib\MyList $list
     */
    public function setList(MyList $list)
    {
        $this->list = $list;
    }

    /**
     * @param null $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }

    public function setTooltip($tooltip)
    {
        $this->tooltip = $tooltip;
    }

    /**
     * @param null $currentUrl
     */
    public function setCurrentUrl($currentUrl)
    {
        $this->currentUrl = $currentUrl;
    }

    protected function getLabel()
    {
        if ($this->label == null) {

            # NYI Maybe we should use label labels as text, maybe just
            # replace spaces with dashes and that's all?
            # what if we need to use a dot or comma or hash character
            # inside text label?
            $result = $this->tag;

            $result = str_replace('-', ' ', $result);
            $result = str_replace('_', ' ', $result);
            $result = preg_replace('/\s\s+/', ' ', $result);

            $result = ucwords($result);
        }
        else {
            $result = $this->label;
        }

        return $result;
    }

    protected function getTooltip()
    {
        if ($this->tooltip == null) {
            $result = 'Sort by ' . $this->getLabel();
        }
        else {
            $result = $this->tooltip;
        }

        return $result;
    }


    # NYI - should be able to give the current url as parameter, and
    # the list should be able to determine it's currentSortOrder from
    # this given url parameters
    #
    # NYI - most of this stuff can be done via javascript! in browser!
    # only need to supply  #_sb=name&_so=asc
    # and in PHP I only need to detect the currently active sorting class
    # sort links like #_sb=name&_so=asc is a SEO friendly sorting URLs
    # and they will be the same if I want to implement sorting via ajax
    # (without reloading the whole page)
    protected function getSortByUrl()
    {
        if ($this->currentUrl == null) {
            $this->currentUrl = \URL::full();
        }

        $urlParts = parse_url($this->currentUrl);
///        $urlParts['path'] = \Request::getPathInfo(); # getPathInfo() doesn't not trim slashes from the end of path, like other methods do!

        if (isset($urlParts['query'])) {
            parse_str($urlParts['query'], $query_data);
        }
        else {
            $query_data = [];
        }

        if ($this->tag == $this->list->getCurrentSortBy($query_data)) {
            if (strtolower($this->list->getCurrentSortOrder($query_data)) == 'asc') {
                $this->classes[] = 'sorting_asc';
            }
            else {
                $this->classes[] = 'sorting_desc';
            }

            $sortOrder = $this->list->getNextSortOrderForTag($this->tag, $this->list->getCurrentSortOrder($query_data));
        }
        else {
            $sortOrder = $this->list->getFirstSortOrderForTag($this->tag);

            $this->classes[] = 'sorting';
        }

        $sortByParam = $this->list->getSortByParam();
        $sortOrderParam = $this->list->getSortOrderParam();

        $new_query_data = [];
        $new_query_data[$sortByParam] = $this->tag;
        $new_query_data[$sortOrderParam] = $sortOrder;

        ksort($new_query_data);
        $newUrl = '#' . http_build_query($new_query_data);

        return $newUrl;
    }

    protected function getClassAttrAsString()
    {
        return implode(' ', $this->classes);
    }

    #####   Methods for use in templates to set custom behavior   #####

    public function addClass($class)
    {
        $this->classes[] = $class;
        return $this;
    }

    public function label($label)
    {
        $this->setLabel($label);
        return $this;
    }

    public function tooltip($tooltip)
    {
        $this->setTooltip($tooltip);
        return $this;
    }

} 

