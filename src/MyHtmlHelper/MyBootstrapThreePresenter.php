<?php

namespace Vicvk\Lib\MyHtmlHelper;

use Illuminate\Pagination\BootstrapThreePresenter;

class MyBootstrapThreePresenter extends BootstrapThreePresenter {

    protected function getPageLinkWrapper($url, $page, $rel = null)
    {
        if ($page == $this->paginator->currentPage()) {
            return $this->getMyActivePageWrapper($url, $page, $rel);
        }

        return $this->getAvailablePageWrapper($url, $page, $rel);
    }

    protected function getMyActivePageWrapper($url, $page, $rel = null)
    {
        $rel = is_null($rel) ? '' : ' rel="'.$rel.'"';

        return '<li class="active"><a href="'.htmlentities($url).'"'.$rel.'>'.$page.'</a></li>';
    }

} 