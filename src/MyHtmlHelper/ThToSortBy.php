<?php

namespace Vicvk\Lib\MyHtmlHelper;


class ThToSortBy extends SortBy {

    public function __toString()
    {
        $link_tag = $this->getTag();
        $link_label = $this->getLabel();
        $link_tooltip = htmlspecialchars($this->getTooltip());
        $link_url = $this->getSortByUrl();
        $link_class = $this->getClassAttrAsString();
        $link_mode = $this->getLinkMode();

        $tag = " data-tag=\"{$link_tag}\"";

        if ($link_class == '') {
            $class = '';
        }
        else {
            $class = " class=\"{$link_class}\"";
        }

        if ($link_mode == '') {
            $mode = '';
        }
        else {
            $mode = " data-mode=\"{$link_mode}\"";
        }

        if ($link_tooltip == '') {
            $tooltip = '';
        }
        else {
            $tooltip = " title=\"{$link_tooltip}\"";
        }

        $result = "<th{$class}><a href=\"{$link_url}\"{$mode}{$tag}{$tooltip}>{$link_label}</a></th>";

        return $result;
    }

}

