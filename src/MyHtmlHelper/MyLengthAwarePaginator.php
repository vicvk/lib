<?php

namespace Vicvk\Lib\MyHtmlHelper;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

class MyLengthAwarePaginator extends LengthAwarePaginator {

    /*
     * 'path' => page number will become part of url path, example: http://example.com/users/page=2
     * 'query' => page number will become part of url query, example: http://example.com/users?_pg=2
     * 'fragment' => page number will become part of url fragment, example: http://example.com/users#_pg=2
     */
    public $paginatorLinksMode = 'path';

    public function url($page)
    {
        if ($page <= 0) {
            $page = 1;
        }

        $path = $this->path;
        $query = $this->query;
        $fragment = $this->fragment;

        if ($this->paginatorLinksMode == 'path') {
            preg_match('/([\/]*)$/i', $path, $matches);
            $ending = isset($matches[1]) ? $matches[1] : '';

            $path = preg_replace('/(\/+page\=[0-9]+)?([\/]*)$/i', '', $path);

            if ($page != 1) {
                $path = $path . "/page={$page}";
            }

            $path = $path . $ending;
        }
        elseif ($this->paginatorLinksMode == 'query') {
            if ($page != 1) {
                $query[$this->pageName] = $page;
            }
            else {
                unset($query[$this->pageName]);
            }

            ksort($query);
        }
        elseif ($this->paginatorLinksMode == 'fragment') {
            # do not alter path or query in any way because we don't
            # want the page to be reloaded.

            $fragment = "{$this->pageName}={$page}";
            $path = '';
            $query = [];
        }

        $query = http_build_query($query, null, '&');

        if ($query != '') {
            $query = '?'.$query;
        }

        if ($fragment != '') {
            $fragment = '#'.$fragment;
        }

        return $path.$query.$fragment;
    }

} 