<?php

namespace Vicvk\Lib\MyHtmlHelper;

class Dropdown
{
    protected $_params = [];

    protected $_classes = [];
    protected $_classWasExplicitlySet = false;

    protected $_parentClasses = [];
    protected $_parentClassWasExplicitlySet = false;

    protected $_ulClasses = [];
    protected $_ulClassWasExplicitlySet = false;

    protected $_items = [];

    public function addItemRaw($liHtml, $liAttr='') {
        $this->_items[] = ['liHtml'=>$liHtml, 'liAttr'=>$liAttr];
        return $this;
    }

    public function addLink($href, $label, $extraAttributes=[], $disabled=false) {

        $extra = [];

        foreach($extraAttributes as $p_name => $p_value) {
            $extra[] = $p_name . '=' . '"' . $p_value . '"';
        }

        $extra = implode(' ', $extra);

        if ($extra != '') {
            $extra = ' ' . $extra;
        }

//        $label = escape_html($label);

        $liHtml = "<a href=\"{$href}\"{$extra}>{$label}</a>";

        if ($disabled) {
            $liAttr = 'class="disabled"';
        }
        else {
            $liAttr = '';
        }

        $this->addItemRaw($liHtml, $liAttr);

        return $this;
    }

    public function addDivider() {
        $this->addItemRaw('', 'role="separator" class="divider"');
        return $this;
    }

    public function addHeader($headerHtml) {
        $this->addItemRaw($headerHtml, 'class="dropdown-header"');
        return $this;
    }

    public function getItemsHtml() {
        $html = "\r\n";

        foreach($this->_items as $item) {
            $liHtml = $item['liHtml'];
            $liAttr = ($item['liAttr'] != '') ? (' ' . $item['liAttr']) : '';

            $html .= "<li{$liAttr}>$liHtml</li>\r\n";
        }

        return $html;
    }

    public function __call($method, $parameters)
    {
        switch ($method) {

            case 'route':
                if (isset($parameters[1])) {
                    $this->_params['href'] = route($parameters[0], $parameters[1]);
                }
                else {
                    $this->_params['href'] = route($parameters[0]);
                }
                break;

            case 'class':
                if (isset($parameters[0])) {
                    $this->_classWasExplicitlySet = true;
                    $this->_classes = [];

                    $tmp_parts = preg_split('/\s+/', $parameters[0]);

                    foreach($tmp_parts as $tmp_part) {
                        $this->_classes[$tmp_part] = true;
                    }
                }
                break;

            case 'parentClass':
                if (isset($parameters[0])) {
                    $this->_parentClassWasExplicitlySet = true;
                    $this->_parentClasses = [];

                    $tmp_parts = preg_split('/\s+/', $parameters[0]);

                    foreach($tmp_parts as $tmp_part) {
                        $this->_parentClasses[$tmp_part] = true;
                    }
                }
                break;

            case 'ulClass':
                if (isset($parameters[0])) {
                    $this->_ulClassWasExplicitlySet = true;
                    $this->_ulClasses = [];

                    $tmp_parts = preg_split('/\s+/', $parameters[0]);

                    foreach($tmp_parts as $tmp_part) {
                        $this->_ulClasses[$tmp_part] = true;
                    }
                }
                break;

            case 'glyph':
                if (isset($parameters[0])) {
                    $this->_params['glyphIcon'] = $parameters[0];
                }
                break;

            default:
                $this->_params[$method] = isset($parameters[0]) ? $parameters[0] : null;
                break;
        }

        return $this;
    }

    public function addClass($class)
    {
        $tmp_parts = preg_split('/\s+/', $class);

        foreach($tmp_parts as $tmp_part) {
            $this->_classes[$tmp_part] = true;
        }

        return $this;
    }

    public function addParentClass($class)
    {
        $tmp_parts = preg_split('/\s+/', $class);

        foreach($tmp_parts as $tmp_part) {
            $this->_parentClasses[$tmp_part] = true;
        }

        return $this;
    }

    public function addUlClass($class)
    {
        $tmp_parts = preg_split('/\s+/', $class);

        foreach($tmp_parts as $tmp_part) {
            $this->_ulClasses[$tmp_part] = true;
        }

        return $this;
    }

    public function __toString()
    {

        if (!$this->_classWasExplicitlySet) {
            $this->addClass('btn btn-sm btn-default dropdown-toggle');
        }

        if (!$this->_parentClassWasExplicitlySet) {
            $this->addParentClass('btn-group');
        }

        if (!$this->_ulClassWasExplicitlySet) {
            $this->addUlClass('dropdown-menu');
        }

        $href = '#';
        $glyphIcon = '';
        $label = 'Dropdown';
        $id = 'dropdownMenu' . mt_rand(1, 1000000000);

        $extraAttributes = [];


        foreach($this->_params as $p_name => $p_value) {

            switch($p_name) {
                case 'href':
                    $href = $this->_params['href'];
                    break;

                case 'glyphIcon':
                    $glyphIcon = "glyphicon glyphicon-{$this->_params['glyphIcon']}";
                    break;

                case 'label':
                    $label = " {$this->_params['label']}";
                    break;

                case 'id':
                    $id = $this->_params['id'];
                    break;

                default:
                    $p_name = snake_case($p_name);
                    $p_name = str_replace('_', '-', $p_name);
                    $extraAttributes[] = $p_name . '=' . '"' . $p_value . '"';
                    break;
            }
        }

        $extraAttributes = implode(' ', $extraAttributes);

        $class = implode(' ', array_keys($this->_classes));
        $parentClass = implode(' ', array_keys($this->_parentClasses));
        $ulClass = implode(' ', array_keys($this->_ulClasses));


        $itemsHtml = $this->getItemsHtml();

        $html = <<<HTML
<div class="{$parentClass}">
  <button class="{$class}" type="button" id="{$id}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <span class="{$glyphIcon}" aria-hidden="true"></span>
    {$label}
    <span class="caret"></span>
  </button>
  <ul class="{$ulClass}" aria-labelledby="{$id}">
{$itemsHtml}
  </ul>
</div>
HTML;

//        $html = '123';

        return $html;
    }

}