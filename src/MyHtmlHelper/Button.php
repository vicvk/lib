<?php

namespace Vicvk\Lib\MyHtmlHelper;

/*
Button examples:

{!! HH::buttonSubmit()->label('Add') !!}
{!! HH::buttonCancel() !!}

{!! HH::buttonCreate()->route('admin-blog::create') !!}
{!! HH::buttonEdit()->route('admin-blog::edit', [$item->id]) !!}
{!! HH::buttonPreview()->route('blog-post-detail', [$item->id, $item->slug]) !!}

{!! HH::buttonDeleteByCheckboxes()->dataDeleteUrl( route('admin-blog::destroyMultiple') ) !!}
{!! HH::buttonSearch() !!}

{!! HH::buttonSortArrowDown()
    ->dataAjaxCallUrl(route('admin-blog-photo::sort-arrows')) 
    ->dataId($item->id)
    ->dataBlogPostId($blogPostId)
    ->addClass('pull-right')
!!}

{!! HH::buttonSortArrowUp()
    ->dataAjaxCallUrl(route('admin-blog-photo::sort-arrows')) 
    ->dataId($item->id)
    ->dataBlogPostId($blogPostId)
    ->addClass('pull-right')
!!}

##########################################
##########  Custom buttons:  #############

{!! HH::button()->route('admin-blog-photo::index', [$item->id])->addClass('btn-success btn-sm mylib-return-back')->glyphIcon('camera')->label('Photos') !!}

{!! HH::button()
    ->route('admin-blog::index')
    ->addClass('btn-success btn-sm')
    ->label('Return to Blog')
    ->glyphIcon('arrow-left') 
!!}

{!! HH::button()
    ->addClass('btn-success btn-sm mylib-ajax-call')
    ->dataAjaxCallUrl( route('admin-building-listing-photo::addPhotos') )
    ->dataJquerySelectorForDataInputs('.mylib-select-checkboxes')
    ->dataBuildingId($buildingId)
    ->label('Add selected photos to Building')
    ->glyphIcon('plus-sign') 
!!}


{!! HH::button()
    ->route('admin-building::add-address-create', [$item->id])
    ->addClass('btn-danger btn-xs building-add-address-button')
    ->glyphIcon('remove')
    ->dataFancyboxType('iframe')
    ->title('Remove this address from this building') 
!!}




*/


use Vicvk\Lib\MyHtmlHelper;

class Button
{
    protected $_params = [];
    protected $_classes = [];
    protected $_role = '';
    protected $_buttonType = 'a';
    protected $_classWasExplicitlySet = false;

    public function __call($method, $parameters)
    {
        switch ($method) {

            case 'route':
                if (isset($parameters[1])) {
                    $this->_params['href'] = route($parameters[0], $parameters[1]);
                }
                else {
                    $this->_params['href'] = route($parameters[0]);
                }
                break;

            case 'class':
                if (isset($parameters[0])) {
                    $this->_classWasExplicitlySet = true;
                    $this->_classes = [];

                    $tmp_parts = preg_split('/\s+/', $parameters[0]);

                    foreach($tmp_parts as $tmp_part) {
                        $this->_classes[$tmp_part] = true;
                    }
                }
                break;

            case 'glyph':
            case 'icon':
            case 'glyphicon':
                if (isset($parameters[0])) {
                    $this->_params['glyphIcon'] = $parameters[0];
                }
                break;

            default:
                $p_name = str_replace('_', '-', $method);
                $p_value = isset($parameters[0]) ? $parameters[0] : null;

                $this->_params[$p_name] = $p_value;
                break;
        }

        return $this;
    }

    public function setAttribute($name, $value)
    {
        $this->_params[$name] = $value;

        return $this;
    }

    public function setRole($role)
    {
        $this->_role = $role;

        return $this;
    }

    public function setButtonType($buttonType)
    {
        $this->_buttonType = $buttonType;

        return $this;
    }

    public function addClass($class)
    {
        $tmp_parts = preg_split('/\s+/', $class);

        foreach($tmp_parts as $tmp_part) {
            $this->_classes[$tmp_part] = true;
        }

        return $this;
    }

    public function __toString()
    {
        switch ($this->_role) {

            case 'SortArrowDown':
                if (!isset($this->_params['data-ajax-call-url'])) {
                    # for now PHP doesn't allow to throw exceptions from __toString() method
                    # due to PHP engine implementation.
                    #throw new \BadMethodCallException("buttonDeleteByCheckboxes(): dataDeleteUrl parameter is required.");
                    #trigger_error("buttonDeleteByCheckboxes(): dataDeleteUrl parameter is required.", E_USER_ERROR);
                    echo "buttonSortArrowDown(): data_ajax_call_url parameter is required.";
                    exit;
                }

                if (!$this->_classWasExplicitlySet) {
                    $this->addClass('btn btn-sm mylib-ajax-call');
                }

                $this->_params['data-method'] = 'post';
                $this->_params['data-on-success-function-name'] = 'mylib.refreshWindow';
                $this->_params['data-arrow'] = 'down';
                $this->_params['glyphIcon'] = 'arrow-down';

                break;

            case 'SortArrowUp':
                if (!isset($this->_params['data-ajax-call-url'])) {
                    # for now PHP doesn't allow to throw exceptions from __toString() method
                    # due to PHP engine implementation.
                    #throw new \BadMethodCallException("buttonDeleteByCheckboxes(): dataDeleteUrl parameter is required.");
                    #trigger_error("buttonDeleteByCheckboxes(): dataDeleteUrl parameter is required.", E_USER_ERROR);
                    echo "buttonSortArrowUp(): data_ajax_call_url parameter is required.";
                    exit;
                }

                if (!$this->_classWasExplicitlySet) {
                    $this->addClass('btn btn-sm mylib-ajax-call');
                }

                $this->_params['data-method'] = 'post';
                $this->_params['data-on-success-function-name'] = 'mylib.refreshWindow';
                $this->_params['data-arrow'] = 'up';
                $this->_params['glyphIcon'] = 'arrow-up';

                break;

            case 'DeleteByCheckboxes':
                if (!isset($this->_params['data-delete-url'])) {
                    # for now PHP doesn't allow to throw exceptions from __toString() method
                    # due to PHP engine implementation.
                    #throw new \BadMethodCallException("buttonDeleteByCheckboxes(): dataDeleteUrl parameter is required.");
                    #trigger_error("buttonDeleteByCheckboxes(): dataDeleteUrl parameter is required.", E_USER_ERROR);
                    echo "buttonDeleteByCheckboxes(): data_delete_url parameter is required.";
                    exit;
                }

                if (!isset($this->_params['label'])) {
                    $this->_params['label'] = 'Delete';
                }

                if (!$this->_classWasExplicitlySet) {
                    $this->addClass('btn btn-danger btn-sm mylib-delete-by-checkboxes-command');
                }

                $this->_params['glyphIcon'] = 'remove';
                break;

            case 'Create':
                if (!isset($this->_params['href'])) {
                    # for now PHP doesn't allow to throw exceptions from __toString() method
                    # due to PHP engine implementation.
                    #throw new \BadMethodCallException("buttonCreate(): href parameter is required.");
                    echo "buttonCreate(): href parameter is required.";
                    exit;
                }

                if (!isset($this->_params['label'])) {
                    $this->_params['label'] = 'Create';
                }

                if (!$this->_classWasExplicitlySet) {
                    $this->addClass('btn btn-success btn-sm mylib-return-back');
                }

                $this->_params['glyphIcon'] = 'plus';
                break;

            case 'Edit':
                if (!isset($this->_params['href'])) {
                    # for now PHP doesn't allow to throw exceptions from __toString() method
                    # due to PHP engine implementation.
                    #throw new \BadMethodCallException("buttonEdit(): href parameter is required.");
                    echo "buttonEdit(): href parameter is required.";
                    exit;
                }

                if (!isset($this->_params['label'])) {
                    $this->_params['label'] = 'Edit';
                }

                if (!$this->_classWasExplicitlySet) {
                    $this->addClass('btn btn-success btn-sm mylib-return-back');
                }

                $this->_params['glyphIcon'] = 'edit';
                break;

            case 'Preview':
                if (!isset($this->_params['href'])) {
                    # for now PHP doesn't allow to throw exceptions from __toString() method
                    # due to PHP engine implementation.
                    #throw new \BadMethodCallException("buttonPreview(): href parameter is required.");
                    echo "buttonPreview(): href parameter is required.";
                    exit;
                }

                if (!isset($this->_params['target'])) {
                    $this->_params['target'] = '_blank';
                }

                if (!isset($this->_params['label'])) {
                    $this->_params['label'] = 'Preview';
                }

                if (!$this->_classWasExplicitlySet) {
                    $this->addClass('btn btn-info btn-sm');
                }

                $this->_params['glyphIcon'] = 'new-window';
                break;

            case 'Search':
                if (!isset($this->_params['label'])) {
                    $this->_params['label'] = 'Search';
                }

                if (!$this->_classWasExplicitlySet) {
                    $this->addClass('btn btn-info btn-sm mylib-search-command');
                }

                $this->_params['glyphIcon'] = 'search';
                break;

            case 'Submit':
                if (!isset($this->_params['label'])) {
                    $this->_params['label'] = 'Submit';
                }

                if (!$this->_classWasExplicitlySet) {
                    $this->addClass('btn btn-primary btn-large');
                }

                $this->_params['glyphIcon'] = 'ok';
                $this->_params['type'] = 'submit';
                $this->setButtonType('button');
                break;

            case 'Cancel':
                if (!isset($this->_params['href'])) {
                    $this->_params['href'] = MyHtmlHelper::getReturnBackUrl();
                }

                if (!isset($this->_params['label'])) {
                    $this->_params['label'] = 'Cancel';
                }

                if (!$this->_classWasExplicitlySet) {
                    $this->addClass('btn btn-default btn-large');
                }

                $this->_params['glyphIcon'] = 'remove';
                break;

            default:
                if (!$this->_classWasExplicitlySet) {
                    $this->addClass('btn btn-info btn-sm');
                }
                break;
        }


        $href = '#';
        $glyphIcon = '';
        $label = '';

        $extraAttributes = [];

        foreach($this->_params as $p_name => $p_value) {

            switch($p_name) {
                case 'href':
                    $href = $this->_params['href'];
                    break;

                case 'glyphIcon':
                    $glyphIcon = "glyphicon glyphicon-{$this->_params['glyphIcon']}";
                    break;

                case 'label':
                    $label = " {$this->_params['label']}";
                    break;

                default:
                    $extraAttributes[] = $p_name . '=' . '"' . $p_value . '"';
                    break;
            }
        }

        $extraAttributes = implode(' ', $extraAttributes);
        $class = implode(' ', array_keys($this->_classes));

        if ($this->_buttonType == 'a') {
            $html = <<<HTML
<a href="{$href}" class="{$class}" role="button" {$extraAttributes}><span class="{$glyphIcon}" aria-hidden="true"></span>{$label}</a>
HTML;
        }
        else {
            $html = <<<HTML
<button class="{$class}" {$extraAttributes}><span class="{$glyphIcon}" aria-hidden="true"></span>{$label}</button>
HTML;
        }

        return $html;
    }

}

