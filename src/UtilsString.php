<?php

namespace Vicvk\Lib;

/**
 * @package third_party_modules
 * 
 */

class UtilsString
{

public static function makeCleanAsciiText($str)
{
    return UtilsString::normalizeWhitespace(UtilsString::stripNonAsciiChars(UtilsString::weirdCharactersToAscii($str)));
}

/*
import_third_party_module('UtilsString.php');

$urls = array();

$urls[] = array
(
    'input' => '/aaaa/../',
    'expected' => '/',
);

$urls[] = array
(
    'input' => '/aaaa/bbbb/../',
    'expected' => '/aaaa/',
);

$urls[] = array
(
    'input' => '/aaaa/bbbb/../../',
    'expected' => '/',
);

$urls[] = array
(
    'input' => './aaaa/',
    'expected' => 'aaaa/',
);

$urls[] = array
(
    'input' => '/aaaa/./bbbb/',
    'expected' => '/aaaa/bbbb/',
);

foreach($urls as $url)
{
    $result = UtilsString::truepath($url['input']);

    if ($result != $url['expected'])
    {
        echo "\r\n<br/>\r\n";
        echo "{$url['input']} =====> $result (expected: {$url['expected']})";
        echo "\r\n<br/>\r\n";
    }
}

echo 'finished';

*/
public static function truepath($path)
{
   # ������� ������� ����������� ����  ./
   $path = str_replace('/./', '/', $path);        # <-- ���� � ����� ������ �����
   $path = preg_replace('@^\.\/@', '', $path);   # <-- ���� � ������ ������

   # ������ ����������� �� ../  ������� ��� ������� ����������:
   # ������ � ����� ������ ��� ����� ���� ���:  /aaaa/bbbb/cccc/../../../xxxx
   do {
       $path_prev = $path;
       $path = preg_replace('@\/[^\/]+\/\.\.\/@', '/', $path);
   }
   while ($path_prev != $path);

   return $path;
}


////# ����� ������:
////# http://stackoverflow.com/questions/4049856/replace-phps-realpath
////# ������ �� ����������
/////**
//// * This function is to replace PHP's extremely buggy realpath().
//// * @param string The original path, can be relative etc.
//// * @return string The resolved path, it might not exist.
//// */
////function truepath($path){
////    // whether $path is unix or not
////    $unipath=strlen($path)==0 || $path{0}!='/';
////    // attempts to detect if path is relative in which case, add cwd
////    if(strpos($path,':')===false && $unipath)
////        $path=getcwd().DIRECTORY_SEPARATOR.$path;
////    // resolve path parts (single dot, double dot and double delimiters)
////    $path = str_replace(array('/', '\\'), DIRECTORY_SEPARATOR, $path);
////    $parts = array_filter(explode(DIRECTORY_SEPARATOR, $path), 'strlen');
////    $absolutes = array();
////    foreach ($parts as $part) {
////        if ('.'  == $part) continue;
////        if ('..' == $part) {
////            array_pop($absolutes);
////        } else {
////            $absolutes[] = $part;
////        }
////    }
////    $path=implode(DIRECTORY_SEPARATOR, $absolutes);
////    // resolve any symlinks
////    if(file_exists($path) && linkinfo($path)>0)$path=readlink($path);
////    // put initial separator that could have been lost
////    $path=!$unipath ? '/'.$path : $path;
////    return $path;
////}


/*
  ������� ������� ������� � ������ � � ����� ������, � ��������
  ������ ������ ������ ������ �������� ��� ����� �� ���� ������.
*/
public static function normalizeWhitespace($string)
{
    $result = trim(preg_replace('/\s\s+/', ' ', $string));

    return $result;
}


/*
������ ����� �������������� ���� ������: http://php.net/manual/en/function.chr.php
In addition to replacing Microsoft Windows smart quotes, as sgaston demonstrated on 2006-02-13, 
I replace all other Microsoft Windows characters using suggestions[1] published by character 
code specialist[2] Jukka Korpela.

[1] On the use of some MS Windows characters in HTML
http://www.cs.tut.fi/~jkorpela/www/windows-chars.html

[2] Unicode Explained by Jukka Korpela 
http://www.amazon.com/dp/059610121X/
*/
# ����� ��� ����� ������� weird_characters_to_html_entities($str)
# ��� ����� ��������� ����� ��������� ����� ��������, �.�. ascii ����� ������
# ����� ��������, � � html entities ������ ����� ����� ������������
public static function weirdCharactersToAscii($str)
{
    $str = str_replace(chr(130), ',', $str);    // baseline single quote
    $str = str_replace(chr(131), 'NLG', $str);  // florin
    $str = str_replace(chr(132), '"', $str);    // baseline double quote
    $str = str_replace(chr(133), '...', $str);  // ellipsis
    $str = str_replace(chr(134), '**', $str);   // dagger (a second footnote)
    $str = str_replace(chr(135), '***', $str);  // double dagger (a third footnote)
    $str = str_replace(chr(136), '^', $str);    // circumflex accent
    $str = str_replace(chr(137), 'o/oo', $str); // permile
    $str = str_replace(chr(138), 'Sh', $str);   // S Hacek
    $str = str_replace(chr(139), '<', $str);    // left single guillemet
    $str = str_replace(chr(140), 'OE', $str);   // OE ligature
    $str = str_replace(chr(145), "'", $str);    // left single quote
    $str = str_replace(chr(146), "'", $str);    // right single quote
    $str = str_replace(chr(147), '"', $str);    // left double quote
    $str = str_replace(chr(148), '"', $str);    // right double quote
    $str = str_replace(chr(149), '-', $str);    // bullet
    $str = str_replace(chr(150), '-', $str);    // endash
    $str = str_replace(chr(151), '-', $str);   // emdash
    $str = str_replace(chr(152), '~', $str);    // tilde accent
    $str = str_replace(chr(153), '(TM)', $str); // trademark ligature
    $str = str_replace(chr(154), 'sh', $str);   // s Hacek
    $str = str_replace(chr(155), '>', $str);    // right single guillemet
    $str = str_replace(chr(156), 'oe', $str);   // oe ligature
    $str = str_replace(chr(159), 'Y', $str);    // Y Dieresis

    return $str;
}



/*

$subject ����� ���� ��� ������� �������, ��� � �������� - � ��������� ������ ��� ������� 
�������� ������� (����� �����������) ����� ������������ ������ ������-style ���������� �� ��������.
�����: ������ ����� �������������� ������ ��� �������� (values), ��� ������ (keys) ������ �� ����� -
��� ���������� ���������

NYI7: �����-�� ����� ����� ��������� ��� ���� ��� ������ ������ - ����� ��������� �� �����������
�������� ������������, �������, ������ ������� � �.�.
*/

public static function substituteSmartyStyleVars(&$subject, $vars)
{
    if (is_array($subject))
    {
        foreach($subject as $subject_idx => $subject_val)
        {
            UtilsString::substituteSmartyStyleVars($subject[$subject_idx], $vars);
        }
    }
    else
    {
        $smarty_style_var_names  = array();
        $smarty_style_var_values = array();

        foreach($vars as $var_name => $var_value)
        {
            $var_name = "{\${$var_name}}";

            if (is_object($var_value))
            {
                # ������� ����������
            }
            elseif (is_array($var_value))
            {
                # NYI - ������� ����� ���������� ��������� � ���� ����������
            }
            else
            {
                # ������� ��������� ���� �����������
                $smarty_style_var_names[]  = $var_name;
                $smarty_style_var_values[] = $var_value;
            }
        }



        $subject = str_replace($smarty_style_var_names, $smarty_style_var_values, $subject);
    }
}

/*
NYI: ��� ������� ������ ������������ �������� ���� lang!
*/
public static function numberToMonthName($month_number)
{
    $result = '';

    $month_number = (int) $month_number;

    switch ($month_number)
    {
        case 1:  $result = 'January'; break;
        case 2:  $result = 'February'; break;
        case 3:  $result = 'March'; break;

        case 4:  $result = 'April'; break;
        case 5:  $result = 'May'; break;
        case 6:  $result = 'June'; break;

        case 7:  $result = 'July'; break;
        case 8:  $result = 'August'; break;
        case 9:  $result = 'September'; break;

        case 10:  $result = 'October'; break;
        case 11:  $result = 'November'; break;
        case 12:  $result = 'December'; break;
    }

    return $result;
}

public static function numberToShortMonthName($month_number)
{
    $result = '';

    $month_number = (int) $month_number;

    switch ($month_number)
    {
        case 1:  $result = 'Jan'; break;
        case 2:  $result = 'Feb'; break;
        case 3:  $result = 'Mar'; break;

        case 4:  $result = 'Apr'; break;
        case 5:  $result = 'May'; break;
        case 6:  $result = 'Jun'; break;

        case 7:  $result = 'Jul'; break;
        case 8:  $result = 'Aug'; break;
        case 9:  $result = 'Sep'; break;

        case 10:  $result = 'Oct'; break;
        case 11:  $result = 'Nov'; break;
        case 12:  $result = 'Dec'; break;
    }

    return $result;
}


/*
NYI: ��� ������� ������ ������������ �������� ���� lang!
*/
public static function monthNameToNumber($month_name, $pad_with_zero=false)
{
    $result = '';

    switch (strtolower($month_name))
    {
        case 'january':
        case 'jan':
            $result = 1; 
            break;

        case 'february':
        case 'feb':
            $result = 2; 
            break;

        case 'march':
        case 'mar':
            $result = 3; 
            break;


        case 'april':
        case 'apr':
            $result = 4; 
            break;

        case 'may':
        case 'may':
            $result = 5; 
            break;

        case 'june':
        case 'jun':
            $result = 6; 
            break;


        case 'july':
        case 'jul':
            $result = 7; 
            break;

        case 'august':
        case 'aug':
            $result = 8; 
            break;

        case 'september':
        case 'sep':
            $result = 9; 
            break;


        case 'october':
        case 'oct':
            $result = 10; 
            break;

        case 'november':
        case 'nov':
            $result = 11; 
            break;

        case 'december':
        case 'dec':
            $result = 12; 
            break;
    }

    if ($pad_with_zero)
    {
        $result = str_pad($result, 2, '0', STR_PAD_LEFT);
    }

    return $result;
}


/*
������ ������� �� ������ - ���� ��� ����������� �������.
Makes a timestamp from a string.
*/
public static function makeTimestamp($string)
{
    if(empty($string)) 
    {
        $string = "now";
    }

    // is mysql timestamp format of YYYYMMDDHHMMSS?
    if (preg_match('/^\d{14}$/', $string)) 
    {
////echo '11111';

        $time = mktime(substr($string,8,2),substr($string,10,2),substr($string,12,2),
                substr($string,4,2),substr($string,6,2),substr($string,0,4));

        return $time;
    }

    # vicvk change: ������ � �������� ���� ������ ��������� � strtotime() ����������� ��
    # �������� �� mysql timestamp - ��� �������� �����������, �.�. ����� ����������� ��������������
    # ������ ����: '20040318135500'
    $time = strtotime($string);
    if (is_numeric($time) && $time != -1)
    {
////echo '22222';

        return $time;
    }

    // couldn't recognize it, try to return a time
    $time = (int) $string;
    if ($time > 0)
    {
////echo '33333';
        return $time;
    }
    else
    {
////echo '44444';
        return time();
    }
}

/*
���� ������ ��� ������� �� ������ - ���� ��� ����������� �����������
*/
public static function formatDate($string, $format="%b %e, %Y", $default_date=null)
{
    if (($string == '0000-00-00 00:00:00') || (($string == '0000-00-00')))
    {
//        return $string;
        return 'n/a';
    }

    #
    # �������� �������� �������������� ���� �� http://php.net/strftime
    #

    if (substr(PHP_OS,0,3) == 'WIN')
    {
           $_win_from = array ('%e',  '%T',       '%D');
           $_win_to   = array ('%#d', '%H:%M:%S', '%m/%d/%y');
           $format = str_replace($_win_from, $_win_to, $format);
    }

    if($string != '') 
    {
        return strftime($format, UtilsString::makeTimestamp($string));
    } 
    elseif (isset($default_date) && $default_date != '') 
    {
        return strftime($format, UtilsString::makeTimestamp($default_date));
    } 
    else
    {
        return;
    }
}


///public static function formatDateTime($string, $format="%a %b %e, %Y %I:%M %p", $default_date=null)
public static function formatDateTime($string, $format="%b %e, %Y %I:%M %p", $default_date=null)
{
    if (($string == '0000-00-00 00:00:00') || (($string == '0000-00-00')))
    {
//        return $string;
        return 'n/a';
    }

    #
    # �������� �������� �������������� ���� �� http://php.net/strftime
    #

    if (substr(PHP_OS,0,3) == 'WIN')
    {
           $_win_from = array ('%e',  '%T',       '%D');
           $_win_to   = array ('%#d', '%H:%M:%S', '%m/%d/%y');
           $format = str_replace($_win_from, $_win_to, $format);
    }

    if($string != '') 
    {
        return strftime($format, UtilsString::makeTimestamp($string));
    } 
    elseif (isset($default_date) && $default_date != '') 
    {
        return strftime($format, UtilsString::makeTimestamp($default_date));
    } 
    else
    {
        return;
    }
}

/*
NYI!!  ��� ������� ����� ������������� �   prepare_javascript_string()

��������! � ������������ ����:
$objResponse->addScript('$("#fancybox_dialog_content").html(\'' . $html . '\')');

����� .html('........')   <--- ������������ ��������� �������, ��� ������� � ��� ��� �� ����
��������� �������� ������ esacpe ==>  \'
*/
public static function prepareXajaxText($text)
{
    # ������� ��� ��� �� �������� ASCII ���������
    $text = UtilsString::stripNonAsciiChars($text);

    $text = str_replace("\r", '', $text);   # ��� ������ �� ����� �� �������� � \r \n ���������
    $text = str_replace("\n", '', $text);
    $text = str_replace("'", "\'", $text);  # ������ ��� �� ���� ������:  .innerHTML='$x'
    return $text;
}

public static function stripNonAsciiChars($text)
{
    # ������� ��� ��� �� �������� ASCII ���������
    $text = preg_replace("/[^\x9\xA\xD\x20-\x7F]/", '', $text);

    return $text;
}


/*
$allowed_characters �������� ���� ����� ����������������� ������������ ������ ��������,
���� ���� ������� ����� ������ ��� ���������� �������. ����� ������ � ����������� ���������
������ ���� ������ ��� ����� 100 ��������.

����� ��� �������� ����� ������ � ��������� �� �������� 8 �� 10 ��������, �� �� ����� 
��� �� ����� �� � ����� - ���������� ������ ������������� �����. ��� ����� �� ������� ����������
����� ������� �� ����������� � �������� ������, �� ���� ��������� ����� ��� ����� ���� �����.
�������� Roboform->Tools->Generate Passwords  ���� �������� ���������� ����� ������� � ������
�� �����������, �� ��������������� ������ (� �����) �����������.
*/
public static function generatePassword($password_length, $allowed_characters = '4')
{
    # ������ ���� �� ���������� ���������. �� � ����� ������ � ���� �������� ����������
    # ������� - �� ���� �����, ��������� ������� ����� �������� ���� � ������. ��� �� ���
    # ������ ���������� - ����� ������� ����� �������� �������� � html ��� �� �� ����� ����
    # ������� ����� �� ����������.
    #
    # �� ��������� - �����-��������� ������ ��� ���������� ��������, � ����� ������� ��������.
    # ��� ����� ��� �� �� ���� ������������� ������.

    switch((string) $allowed_characters)
    {
        case '1': # === ������ ����� ===
            # $allowed_characters = '0123456789';
            # ��� ��������� ������ ����� �������� ������������     
            # ������� get_random_id()  � ���������� ������������                                                    
            # ������� ������� �������� ������������� - ��� � �����                                                  
            # �������, ����� ��� ������ ����� return                                                                
            return get_random_id($password_length, true);
            break;

        # ����� ��������� ������ �� ����� ����� ������ - ���� ����� �����, �� ��� ��� ����� �����
        # � ����� - ���� � �������-�� ��� ��� ������ ����� ������� - ��� ���������� ��������
        # (�������� �������� ������ � ��������� (1)), � ����� ����������������.

        case '2' : # === ��������� ����� � ����� === 
            # ������ ������������ ���������� �������: l I 1 O 0 Q o u v U V
            $allowed_characters = 'abcdefghijkmnpqrstwxyz23456789';
            break;

        case '3' : # === ������� ����� � ����� ===
            # ������ ������������ ���������� �������: l I 1 O 0 Q o u v U V
            $allowed_characters = 'ABCDEFGHJKLMNPRSTWXYZ23456789';
            break;

        case '4' : # === ��������� � ������� ����� � ����� ===
            # ������ ������������ ���������� �������: l I 1 O 0 Q o u v U V
            $allowed_characters = 'abcdefghijkmnpqrstwxyzABCDEFGHJKLMNPRSTWXYZ23456789';
            break;

        # ������ ����� �������������� �������� �������� ���   ~`!@#$%^&*(){}[]-_+="':;,.<>?/|\
        # ����� �� ��� ����� ������������ � ������? �������� ����.

        # � ����������� ����� ������������� �������� � �� ������ �����:  !@#$%^*+=?

        //case '5' : 
        //    # ����������� ����� �������������� ��������
        //    $allowed_characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPRSTUVWXYZ0123456789!@#$%^&*';
        //    break;
        //
        //case '6' : 
        //    # ������� ����� �������������� �������� (�������� ���� ��, � �������� ����� ���� ��������
        //    # ������� ����)   NYI: ����� �������� � ������ � �������� ����� ������� "�������������"
        //    # � �������� �� ����
        //    $allowed_characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPRSTUVWXYZ0123456789!@#$%^&*+-/=:;?()|';
        //    break;
    }


    # ���� $allowed_characters �� ����� �� � ���� ������� �� switch �� ������ ��� ������
    # �������� ������ ����� ����������� ��������.


    $result = '';

    do
    {
        $char_pos = get_random_id(2, true); # ������ �������� �� 00 �� 99 (string)
        $char_pos = (int) $char_pos; # ������ �������� �� 0 �� 99 (int)

        if ($char_pos >= strlen($allowed_characters))
        {
            # ��������� �������� ��������� �� ������� ��������� ����������� ��������, ������
            # ��������� �������� �����.
            continue;
        }
        else
        {
            $result .= substr($allowed_characters, $char_pos, 1);
        }
    }
    while (strlen($result) < $password_length);

    return substr($result, 0, $password_length);
}


/*
� PHP ��� ���� �������� ������� pathinfo �� ��� �������� ������������: �� ������� ��������
��� Win � Linux, � ������ PHP4 �� ������ [filename] � ��� ���-����� ������������� �����������.
����� �������� ���� ������ ���� �������, �������� �����������.
����� � ������������ � �������� ���������� ������� pathinfo() - ��� ����� �� ������� ��������
�� ������ �������� � � ������������� ����������� �� ������. ����� ������, ����� ���� �������.
*/
public static function pathinfo($path, $options)
{

}

# extractFileBaseName('/home/mywebsite/public_html/index.php')  ==> index.php
public static function extractFileBaseName($path)
{
    # ��� � ����� ��� � �������� ������� ����� ������� �� ����� ���� ������� ����������
    # � ��������������� �����.

    $pos = strpos($path, '?');

    if ($pos === false) 
    {

    } 
    else
    {
        $path = substr($path, 0, $pos);
    }

    # ��������� �� ����� ��� ��������� ����� - ��� �������
    $parts = preg_split('/[\/\\\\]/', $path);
    return array_pop($parts);
}

# extractFileName('/home/mywebsite/public_html/index.php')  ==> index
public static function extractFileName($path)
{
    $file_base_name = UtilsString::extractFileBaseName($path);

    $parts = explode('.', $file_base_name);

    if (count($parts) > 1)
    {
        array_pop($parts);
        return implode('.', $parts);
    }
    else
    {
        return $file_base_name;
    }
}

# extractFileExtension('/home/mywebsite/public_html/index.php')  ==> php
public static function extractFileExtension($path)
{
    $file_base_name = UtilsString::extractFileBaseName($path);

    $parts = explode('.', $file_base_name);

    if (count($parts) > 1)
    {
        return array_pop($parts);
    }
    else
    {
        return '';
    }
}

# extractDirName('/home/mywebsite/public_html/index.php')  ==> /home/mywebsite/public_html/
# ��������, �����������: � ����� ����� ���������� ����� ������ ������, ��� �� ������� � ����
# �� ����������. � PHP �������� ���������� ���������� �� ������� ����� �����, ������� ����� ����.
public static function extractDirName($path)
{
    # ���� � $path ����������� �������� ����� \  �� �� ����� � �����
    # ���� � ��������, �� ��������������� � ������� ������ /  ��� ����� ������� ������ ����� ��
    # $path ��������� extractFileBaseName($path) ���������� ��������.

    $file_base_name = UtilsString::extractFileBaseName($path);

    $leave_characters = strlen($path) - strlen($file_base_name);

    return substr($path, 0, $leave_characters);
}



public static function fileExtensionToMimeType($extension) 
{
    $mimes = array
    (
        'hqx'  =>  'application/mac-binhex40',
        'cpt'   =>  'application/mac-compactpro',
        'doc'   =>  'application/msword',
        'bin'   =>  'application/macbinary',
        'dms'   =>  'application/octet-stream',
        'lha'   =>  'application/octet-stream',
        'lzh'   =>  'application/octet-stream',
        'exe'   =>  'application/octet-stream',
        'class' =>  'application/octet-stream',
        'psd'   =>  'application/octet-stream',
        'so'    =>  'application/octet-stream',
        'sea'   =>  'application/octet-stream',
        'dll'   =>  'application/octet-stream',
        'oda'   =>  'application/oda',
        'pdf'   =>  'application/pdf',
        'ai'    =>  'application/postscript',
        'eps'   =>  'application/postscript',
        'ps'    =>  'application/postscript',
        'smi'   =>  'application/smil',
        'smil'  =>  'application/smil',
        'mif'   =>  'application/vnd.mif',
        'xls'   =>  'application/vnd.ms-excel',
        'ppt'   =>  'application/vnd.ms-powerpoint',
        'wbxml' =>  'application/vnd.wap.wbxml',
        'wmlc'  =>  'application/vnd.wap.wmlc',
        'dcr'   =>  'application/x-director',
        'dir'   =>  'application/x-director',
        'dxr'   =>  'application/x-director',
        'dvi'   =>  'application/x-dvi',
        'gtar'  =>  'application/x-gtar',
        'php'   =>  'application/x-httpd-php',
        'php4'  =>  'application/x-httpd-php',
        'php3'  =>  'application/x-httpd-php',
        'phtml' =>  'application/x-httpd-php',
        'phps'  =>  'application/x-httpd-php-source',
        'js'    =>  'application/x-javascript',
        'swf'   =>  'application/x-shockwave-flash',
        'sit'   =>  'application/x-stuffit',
        'tar'   =>  'application/x-tar',
        'tgz'   =>  'application/x-tar',
        'xhtml' =>  'application/xhtml+xml',
        'xht'   =>  'application/xhtml+xml',
        'zip'   =>  'application/zip',
        'mid'   =>  'audio/midi',
        'midi'  =>  'audio/midi',
        'mpga'  =>  'audio/mpeg',
        'mp2'   =>  'audio/mpeg',
        'mp3'   =>  'audio/mpeg',
        'aif'   =>  'audio/x-aiff',
        'aiff'  =>  'audio/x-aiff',
        'aifc'  =>  'audio/x-aiff',
        'ram'   =>  'audio/x-pn-realaudio',
        'rm'    =>  'audio/x-pn-realaudio',
        'rpm'   =>  'audio/x-pn-realaudio-plugin',
        'ra'    =>  'audio/x-realaudio',
        'rv'    =>  'video/vnd.rn-realvideo',
        'wav'   =>  'audio/x-wav',
        'bmp'   =>  'image/bmp',
        'gif'   =>  'image/gif',
        'jpeg'  =>  'image/jpeg',
        'jpg'   =>  'image/jpeg',
        'jpe'   =>  'image/jpeg',
        'png'   =>  'image/png',
        'tiff'  =>  'image/tiff',
        'tif'   =>  'image/tiff',
        'css'   =>  'text/css',
        'html'  =>  'text/html',
        'htm'   =>  'text/html',
        'shtml' =>  'text/html',
        'txt'   =>  'text/plain',
        'text'  =>  'text/plain',
        'log'   =>  'text/plain',
        'rtx'   =>  'text/richtext',
        'rtf'   =>  'text/rtf',
        'xml'   =>  'text/xml',
        'xsl'   =>  'text/xml',
        'mpeg'  =>  'video/mpeg',
        'mpg'   =>  'video/mpeg',
        'mpe'   =>  'video/mpeg',
        'qt'    =>  'video/quicktime',
        'mov'   =>  'video/quicktime',
        'avi'   =>  'video/x-msvideo',
        'movie' =>  'video/x-sgi-movie',
        'doc'   =>  'application/msword',
        'word'  =>  'application/msword',
        'xl'    =>  'application/excel',
        'eml'   =>  'message/rfc822'
    );
    return isset($mimes[strtolower($extension)]) ? $mimes[strtolower($extension)] : 'application/x-unknown-content-type';
}



public static function urlEncodeArray(
    $var,                # the array value
    $varName,            # variable name to be used in the query string
    $separator = '&'     # what separating character to use in the query string
)
{
    $toImplode = array();
    foreach ($var as $key => $value) {
        if (is_array($value)) {
            $toImplode[] = urlEncodeArray($value, "{$varName}[{$key}]", $separator);
        } else {
            $toImplode[] = "{$varName}[{$key}]=".urlencode($value);
        }
    }
    return implode($separator, $toImplode);
}



/*
  ��� �� ����� ������� ��� ������� ����� ����������� �� ��������� ��������� 
  (����� ��� ������� �� locale)  � ������� ����������� ���� ��������������, ��������
  USA:  $1,234.57   �� �������� ��� ������� ������� ��������� ��������. �.�. ������
  ������� ��� � ��������� �������� ������������ � �������� ������ ���������� $format
*/
public static function formatMoney($number, $format='$ %n')
{
    if ($number < 0)
    {
        $sign = '- ';
        $number = abs($number);
    }
    else
    {
        $sign = '';
    }

    if(function_exists('money_format'))
    {
        return($sign.money_format($format, $number));
    }
    else
    {
        return($sign."$".number_format($number, 2));
    }
}

/*
��� �������������� ������� �������� �������, ����� ���������� ������ �� �����, � �����
����������� �����
*/
public static function formatBigMoney($number, $dec_point='.', $thousands_sep=',')
{
    return('$'.number_format($number, 0, $dec_point, $thousands_sep));
}

public static function formatBigNumber($number, $dec_point='.', $thousands_sep=',')
{
    return(number_format($number, 0, $dec_point, $thousands_sep));
}


public static function formatTimeInterval($seconds, $granularity = 2)
{
    # NYI: ����� ���� �� ������� ���������� � ��������� ����� ������������ $units,
    # �������� ������ - ������ �����, ������ ���

    $units = array(
        '1 year|:count years' => 31536000,
//        '1 week|:count weeks' => 604800,
        '1 day|:count days' => 86400,
        '1 hour|:count hours' => 3600,
        '1 min|:count min' => 60,
        '1 sec|:count sec' => 1);
    $output = '';
    foreach ($units as $key => $value) {
        $key = explode('|', $key);
        if ($seconds >= $value) {
            $count = floor($seconds / $value);
            $output .= ($output ? ' ' : '');
            if ($count == 1) {
                $output .= $key[0];
            } else {
                $output .= str_replace(':count', $count, $key[1]);
            }
            $seconds %= $value;
            $granularity--;
        }
        if ($granularity == 0) {
            break;
        }
    }

    return $output ? $output : '0 sec';
}


/*
This is a copy of standard Smarty function:
smarty_modifier_truncate($string, $length = 80, $etc = '...', $break_words = false)


Another good implementation is here:
http://stackoverflow.com/questions/79960/how-to-truncate-a-string-in-php-to-the-word-closest-to-a-certain-number-of-chara

*/
public static function truncate($string, $length = 80, $etc = '...', $break_words = false)
{
    if ($length == 0)
        return '';

    if (strlen($string) > $length) {
        $length -= strlen($etc);
        if (!$break_words)
            $string = preg_replace('/\s+?(\S+)?$/', '', substr($string, 0, $length+1));

        return substr($string, 0, $length).$etc;
    } else
        return $string;
}

/*
# was: smarty_modifier_mb_truncate
# from here: http://phpclub.ru/talk/threads/%D0%9F%D1%80%D0%B0%D0%B2%D0%B8%D0%BB%D1%8C%D0%BD%D0%B0%D1%8F-%D0%BE%D0%B1%D1%80%D0%B5%D0%B7%D0%BA%D0%B0-%D1%81%D0%BB%D0%BE%D0%B2.64501/
public static function truncate($string, $length = 80, $etc = '...', $charset='UTF-8',
                                $break_words = false, $middle = false)
{
    if ($length == 0)
        return '';

    if (strlen($string) > $length) {
        $length -= min($length, strlen($etc));
        if (!$break_words && !$middle) {
            $string = preg_replace('/\s+?(\S+)?$/u', '', mb_substr($string, 0, $length+1, $charset));
        }
        if(!$middle) {
            return mb_substr($string, 0, $length, $charset) . $etc;
        } else {
            return mb_substr($string, 0, $length/2, $charset) . $etc . mb_substr($string, -$length/2, $charset);
        }
    } else {
        return $string;
    }
}
*/

#
# http://www.seomoz.org/learn-seo/title-tag
#
/*

$extra_params = array();
$extra_params['required_prefix'] = '123';
$extra_params['required_suffix'] = '456';

$extra_params['optional_strings'] = array();
$extra_params['optional_strings'][] = array('prefix'=>'Buy Handmade');
$extra_params['optional_strings'][] = array('suffix'=>'on iCraft.ca');
$extra_params['optional_strings'][] = array('suffix'=>'marketplace');

*/
public static function formatMetaTitle($str, $recommended_length=70, $max_length=75, $extra_params=array())
{

# NYI ����� ��� ������� ��� ������� ����� �� �� �����, ����� �������� remove_string1
# �������� ����� ������ ���� ����������� �� ������ ������� beautifulTitle
# � �� �������� YogiBead � ������ �������� ������ �������� �������� ���� ScreenName
# �������

    if ($recommended_length==null)
    {
        $recommended_length = 70;
    }

    if ($max_length==null)
    {
        $max_length = 75;
    }

    if (isset($extra_params['required_prefix']))
    {
        $extra_params['required_prefix'] = UtilsString::beautifulTitle($extra_params['required_prefix'], 
        array('begin_with_letter_or_digit'=>false));
    }
    else
    {
        $extra_params['required_prefix'] = '';
    }

    if (isset($extra_params['required_suffix']))
    {
        # � �������� �� ������ ������ ����� UPPER_CASE
        $extra_params['required_suffix'] = UtilsString::beautifulTitle($extra_params['required_suffix'], 
        array('begin_with_letter_or_digit'=>false, 'first_char_to_uppercase'=>false));
    }
    else
    {
        $extra_params['required_suffix'] = '';
    }

    $str = UtilsString::beautifulTitle($str);

    $truncate_length = $max_length - strlen($extra_params['required_prefix']) - strlen($extra_params['required_suffix']);

    $untruncated_str_length = strlen($str);
    $str = UtilsString::truncate($str, $truncate_length, '', false);
    if ($untruncated_str_length == strlen($str))
    {
        $add_dots = false;
    }
    else
    {
        $add_dots = true;
    }

    # � ����� ������� ��� ����� ����������
    $str = preg_replace('/[^a-z0-9]+$/i', '', $str);

    if ($add_dots)
    {
        $str = $str . '...';
    }

    if ($extra_params['required_prefix'] != '')
    {
        $str = $extra_params['required_prefix'] . ' ' . $str;
    }

    if ($extra_params['required_suffix'] != '')
    {
        $str = $str . ' ' . $extra_params['required_suffix'];
    }

    if (isset($extra_params['optional_strings']) && is_array($extra_params['optional_strings']))
    {
        foreach($extra_params['optional_strings'] as $optional_string)
        {

///print_r($optional_string);

            if (isset($optional_string['prefix']))
            {
                $optional_string['prefix'] = UtilsString::beautifulTitle($optional_string['prefix'], 
                array('begin_with_letter_or_digit'=>false));

                if ((strlen($str)+strlen($optional_string['prefix'])+1) < $recommended_length)
                {
                    $str = $optional_string['prefix'] . ' ' . $str;
                }
                else
                {
                    break;
                }
            }
            else
            {
                # � �������� �� ������ ������ ����� UPPER_CASE
                $optional_string['suffix'] = UtilsString::beautifulTitle($optional_string['suffix'], 
                array('begin_with_letter_or_digit'=>false, 'first_char_to_uppercase'=>false));

                if ((strlen($str)+strlen($optional_string['suffix'])+1) < $recommended_length)
                {
                    $str = $str . ' ' . $optional_string['suffix'];
                }
                else
                {
                    break;
                }
            }
        }
    }

    # ���� ����� ���� ���������� ����������� � �������� ����������� ����� ���������
    # (�������� �������)
    # ����� ����� ������� ���������� ������� ���� �� ������
    $str = preg_replace('/\s*([\.\,\!\:\?\%])/i', '$1', $str);

    # ��� �� ���� ����������� �� ����� - �������� ��� ����� � ��� �������
    ## � ����� ���� ������ ���������� ������ ���� ������ (����� ����� �� ���� ������-��� - �������� �����)
    #$str = preg_replace('/([\,\!\:\?\%])/i', '$1 ', $str);


    # ����� ��� ��� �������� �� �������������� �� UPPER CASE - ������ ��� �� ��������� �
    # ������� beautifulTitle  �� ������ �� �������� ������ �� ������ ����� � �����������
    # ���� � ������ �������� ���������� - ��� ����� ��� ������ � ������ ������ ����������
    # upper case
    # ���� �������������� UPPER CASE �� ������������� ������ ��� ����� lower case
    # 1 ������ �� 5�� ����� ���� UPPER CASE ���� ������ �� ��������� �������
    if (UtilsString::countUpperCase($str) > floor(strlen($str)/5))
    {
        $str = strtolower($str);
    }

    # ������ ������ ������ upper case, ��������� ������� ��������� ��� ���������
    $str = ucfirst($str);

    return $str;
}

#
# http://www.seomoz.org/learn-seo/meta-description
#
public static function formatMetaDescription($str, $recommended_length=150, $max_length=160, $extra_params=array())
{
//    return 'qq';

    if ($recommended_length==null)
    {
        $recommended_length = 150;
    }

    if ($max_length==null)
    {
        $max_length = 160;
    }

    $str = UtilsString::formatMetaTitle($str, $recommended_length, $max_length, $extra_params);

    return $str;
}


public static function beautifulTitle($str, $options=array())
{
    # Default options:
    $options['begin_with_letter_or_digit'] = isset($options['begin_with_letter_or_digit']) ? $options['begin_with_letter_or_digit'] : true;
    $options['first_char_to_uppercase'] = isset($options['first_char_to_uppercase']) ? $options['first_char_to_uppercase'] : true;

    $str = preg_replace('/\<[^\>]*\>/', ' ', $str); # ������� html ����
    $str = preg_replace('/\[[^\]]*\]/', ' ', $str); # ������� BB ����

    # �� �������� html entities ��� ����� ��������� �������� � ���������, �.�. ��� �������
    # ����������� ������� ���������:
    # Left single (opening) quotation mark ('): ' or &lsquo; 
    # Right single (closing) quotation mark or apostrophe ('): ' or &rsquo; 
    $str = str_replace('&#8217;', "'", $str);
    $str = str_replace('&#39;', "'", $str);
    $str = str_ireplace('&apos;', "'", $str);
    $str = str_ireplace('&lsquo;', "'", $str);
    $str = str_ireplace('&rsquo;', "'", $str);

    $str = str_replace('&#38;', '&', $str);
    $str = str_ireplace('&amp;', '&', $str);
    # ������ ������� ���� ����� ���� �������� ������� ��������� ����� html entities, ����
    # ����� ������������ ������� ��� ����� ��� �������. �� ��� �� ��� ������� �����������,
    # � ��� ������ ��������� ��������� ������� � ���������� ����������� ������ � �����.

    $str = preg_replace('/\&\#?[a-z0-9]{2,8}\;/i', ' ', $str); # ������� html entities

    # ����� ���� � ����, ��� ��������� ������� ����� ������������ �� ��������, ����
    # �����-����� �����������
    # ������ ��� ����� ����, ���� � ��������  .,!:?'&-$%|
    # ������������ ����� ����� ������ ��� ��� � meta title ������������ ��� �����������

    # ������� ��� ������������� �������
    $str = preg_replace('/[^a-z0-9\.\,\!\:\?\'\&\-\$\%\|\/\(\)]/i', ' ', $str);

    # ���������� ������ � ����� ��� �����
    if ($options['begin_with_letter_or_digit'])
    {
        $str = preg_replace('/^[^a-z0-9]+/i', '', $str);
    }


    # ��� ����� ���������� ��� ����������� ����� ������ ����, ��������� �� ������ �����
    # ����������, ������������ ������
    $str = preg_replace('/([\.\,\!\:\?\'\&\-\$\%\|\/\(\)])[\.\,\!\:\?\'\&\-\$\%\|\/\(\)]+/i', '$1', $str);

    # ����� ����� ������� ���������� ������� ���� �� ������
    $str = preg_replace('/\s*([\.\,\!\:\?\%\)])/i', '$1', $str);
    # � ����� ���� ������ ���������� ������ ���� ������ (����� ����� �� ���� ������-��� - �������� �����)
    $str = preg_replace('/([\,\!\:\?\%])/i', '$1 ', $str);

    # ��������� ������ spaces
    $str = preg_replace('/\s\s*/', ' ', $str);
    $str = trim($str);



    # ���� �������������� UPPER CASE �� ������������� ������ ��� ����� lower case
    # 1 ������ �� 5�� ����� ���� UPPER CASE ���� ������ �� ��������� �������
    if (UtilsString::countUpperCase($str) > floor(strlen($str)/5))
    {
        $str = strtolower($str);
    }

    # ������ ������ ������ upper case, ��������� ������� ��������� ��� ���������
    if ($options['first_char_to_uppercase'])
    {
        $str = ucfirst($str);
    }

    ## !!! ��� �������� �� � ������� beautifulTitle � ��������������� ����� ������������ � html ���
    ## � ����� ����� ������� (����� ������� ����� ��������) ������� ����� ������:
    ## ������ & �������� �� &amp; (� ������� ����� �������)
    ## ��������� ������� �������� �� &apos; (� ������� ����� �������)

    return $str;
}



public static function countUpperCase($str)
{
    return strlen(preg_replace('/[^A-Z]/', '', $str));
}




# ��������: http://kuikie.com/snippets/snippet.php/90-17/php-function-to-convert-bbcode-to-html
public static function bbCodeToHtml($bbtext)
{
  $bbtags = array(
    '[heading1]' => '<h1>','[/heading1]' => '</h1>',
    '[heading2]' => '<h2>','[/heading2]' => '</h2>',
    '[heading3]' => '<h3>','[/heading3]' => '</h3>',
    '[h1]' => '<h1>','[/h1]' => '</h1>',
    '[h2]' => '<h2>','[/h2]' => '</h2>',
    '[h3]' => '<h3>','[/h3]' => '</h3>',

    '[paragraph]' => '<p>','[/paragraph]' => '</p>',
    '[para]' => '<p>','[/para]' => '</p>',
    '[p]' => '<p>','[/p]' => '</p>',
    '[left]' => '<p style="text-align:left;">','[/left]' => '</p>',
    '[right]' => '<p style="text-align:right;">','[/right]' => '</p>',
    '[center]' => '<p style="text-align:center;">','[/center]' => '</p>',
    '[justify]' => '<p style="text-align:justify;">','[/justify]' => '</p>',

///    '[quote]' => '<span class="bbcode_quote">','[/quote]' => '</span>',

    '[bold]' => '<span style="font-weight:bold;">','[/bold]' => '</span>',
    '[italic]' => '<span style="font-style:italic;">','[/italic]' => '</span>',
    '[underline]' => '<span style="text-decoration:underline;">','[/underline]' => '</span>',
    '[b]' => '<span style="font-weight:bold;">','[/b]' => '</span>',
    '[i]' => '<span style="font-style:italic;">','[/i]' => '</span>',
    '[u]' => '<span style="text-decoration:underline;">','[/u]' => '</span>',
    '[break]' => '<br/>',
    '[br]' => '<br/>',
    '[newline]' => '<br/>',
    '[nl]' => '<br/>',
    
    '[unordered_list]' => '<ul>','[/unordered_list]' => '</ul>',
    '[list]' => '<ul>','[/list]' => '</ul>',
    '[ul]' => '<ul>','[/ul]' => '</ul>',
    
    '[ordered_list]' => '<ol>','[/ordered_list]' => '</ol>',
    '[ol]' => '<ol>','[/ol]' => '</ol>',
    '[list_item]' => '<li>','[/list_item]' => '</li>',
    '[li]' => '<li>','[/li]' => '</li>',
        
    '[*]' => '<li>','[/*]' => '</li>',
    '[code]' => '<code>','[/code]' => '</code>',
    '[preformatted]' => '<pre>','[/preformatted]' => '</pre>',
    '[pre]' => '<pre>','[/pre]' => '</pre>',

    '[table]' => '<table>','[/table]' => '</table>',
    '[tr]' => '<tr>','[/tr]' => '</tr>',
    '[td]' => '<td>','[/td]' => '</td>',
  );
    
  $bbtext = str_ireplace(array_keys($bbtags), array_values($bbtags), $bbtext);


  $bbextended = array(
    "/\[quote(\s+[^\]]*)?\]\s*(.*?)\s*\[\/quote\]/i" => "<span class=\"bbcode_quote\">$2</span>",

    "/\[url](.*?)\[\/url\]/i" => "<a href=\"http://$1\" title=\"$1\">$1</a>", # �� ������� � ����� ����� � title ��������� ���?
    "/\[url=(.*?)\](.*?)\[\/url\]/i" => "<a href=\"$1\" title=\"$1\">$2</a>", # �� ������� � ����� ����� � title ��������� ���?
    "/\[email=(.*?)\](.*?)\[\/email\]/i" => "<a href=\"mailto:$1\">$2</a>",
    "/\[mail=(.*?)\](.*?)\[\/mail\]/i" => "<a href=\"mailto:$1\">$2</a>",
    "/\[img\]([^[]*)\[\/img\]/i" => "<img src=\"$1\" alt=\" \" />",
    "/\[image\]([^[]*)\[\/image\]/i" => "<img src=\"$1\" alt=\" \" />",
    "/\[image_left\]([^[]*)\[\/image_left\]/i" => "<img src=\"$1\" alt=\" \" class=\"img_left\" />",
    "/\[image_right\]([^[]*)\[\/image_right\]/i" => "<img src=\"$1\" alt=\" \" class=\"img_right\" />",
  );


  foreach($bbextended as $match=>$replacement){
    $bbtext = preg_replace($match, $replacement, $bbtext);
  }
  return $bbtext;
}


# http://icraft.ca/forum/Smileys/default/kiss.gif
public static function bbCodeSmileys($string, $path_to_smileys_dir='/forum/Smileys/default/')
{
    $smileysfrom = array('>:D', ':D', '::)', '>:(', ':)', ';)', ';D', ':(', ':o', '8)', ':P', '???', ':-[', ':-X', ':-*', ':\'(', ':-\\', '^-^', 'O0', 'C:-)', '0:)');

////    $smileysto = array('evil.gif', 'cheesy.gif', 'rolleyes.gif', 'angry.gif', 'smiley.gif', 'wink.gif', 'grin.gif', 'sad.gif', 'shocked.gif', 'cool.gif', 'tongue.gif', 'huh.gif', 'embarrassed.gif', 'lipsrsealed.gif', 'kiss.gif', 'cry.gif', 'undecided.gif', 'azn.gif', 'afro.gif', 'police.gif', 'angel.gif');

    $smileysto = array( "<img border=\"0\" src=\"{$path_to_smileys_dir}evil.gif\"/>", "<img border=\"0\" src=\"{$path_to_smileys_dir}cheesy.gif\"/>", "<img border=\"0\" src=\"{$path_to_smileys_dir}rolleyes.gif\"/>", "<img border=\"0\" src=\"{$path_to_smileys_dir}angry.gif\"/>", "<img border=\"0\" src=\"{$path_to_smileys_dir}smiley.gif\"/>", "<img border=\"0\" src=\"{$path_to_smileys_dir}wink.gif\"/>", "<img border=\"0\" src=\"{$path_to_smileys_dir}grin.gif\"/>", "<img border=\"0\" src=\"{$path_to_smileys_dir}sad.gif\"/>", "<img border=\"0\" src=\"{$path_to_smileys_dir}shocked.gif\"/>", "<img border=\"0\" src=\"{$path_to_smileys_dir}cool.gif\"/>", "<img border=\"0\" src=\"{$path_to_smileys_dir}tongue.gif\"/>", "<img border=\"0\" src=\"{$path_to_smileys_dir}huh.gif\"/>", "<img border=\"0\" src=\"{$path_to_smileys_dir}embarrassed.gif\"/>", "<img border=\"0\" src=\"{$path_to_smileys_dir}lipsrsealed.gif\"/>", "<img border=\"0\" src=\"{$path_to_smileys_dir}kiss.gif\"/>", "<img border=\"0\" src=\"{$path_to_smileys_dir}cry.gif\"/>", "<img border=\"0\" src=\"{$path_to_smileys_dir}undecided.gif\"/>", "<img border=\"0\" src=\"{$path_to_smileys_dir}azn.gif\"/>", "<img border=\"0\" src=\"{$path_to_smileys_dir}afro.gif\"/>", "<img border=\"0\" src=\"{$path_to_smileys_dir}police.gif\"/>", "<img border=\"0\" src=\"{$path_to_smileys_dir}angel.gif\"/>" );


    $result = str_replace($smileysfrom, $smileysto, $string);

    return $result;
}




}

