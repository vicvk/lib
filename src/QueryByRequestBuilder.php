<?php

namespace Vicvk\Lib;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class QueryByRequestBuilder
{
    protected $model;
    protected $requestInput;

    protected $queryParts = [];

    /**
     * QueryByRequestBuilder constructor.
     *
     * @param $model
     * @param $requestInput
     */
    public function __construct(Model $model, Array $requestInput)
    {
        $this->model = $model;
        $this->requestInput = $requestInput;
    }

    /*
     * NYI - maybe there will be needed a addByWhereParam (alternatively - addWhereParam)
     * or - addWhereLike()??, addWhereStartsWith(), addWhereEndsOn(), addWhereContains(),
     * addWhereEqual()
     * $result = $result->where('name', 'like', "%{$requestInput['name']}%");
     *
     */

    public function addScope($scope)
    {
        $requiredScopeMethod = Str::camel('scope_' . $scope);

        if (!method_exists($this->model, $requiredScopeMethod)) {
            throw new \BadMethodCallException("Model does not have a {$requiredScopeMethod} method defined");
        }

        $this->queryParts[] = ['type' => 'scope', 'scope' => $scope];

        return $this;
    }

    // addByScope() is deprecated, use addParamScope() instead (the later name is more self explanatory)
    public function addByScope($paramName, $scope = null)
    {
        if ($scope == null) {
            $scope = Str::camel('by_' . $paramName);
        }

        return $this->addParamScope($paramName, $scope);
    }

    public function addParamScope($paramName, $scope = null)
    {
        if ($scope == null) {
            $scope = Str::camel('param_' . $paramName);
        }

        $requiredScopeMethod = Str::camel('scope_param_' . $paramName);

        if (!method_exists($this->model, $requiredScopeMethod)) {
            throw new \BadMethodCallException("Model does not have a {$requiredScopeMethod} method defined");
        }

        $this->queryParts[] = ['type' => 'paramScope', 'paramName' => $paramName, 'scope' => $scope];

        return $this;
    }

    public function addWhereEqual($paramName, $dbField = null)
    {
        if ($dbField == null) {
            $dbField = $paramName;
        }

        $this->queryParts[] = ['type' => 'whereEqual', 'paramName' => $paramName, 'dbField' => $dbField];

        return $this;
    }

    public function addWhereBeginsWith($paramName, $dbField = null)
    {

    }

    # This is an Alias for function addWhereBeginsWith
    public function addWhereBeginsOn($paramName, $dbField = null)
    {
        return $this->addWhereBeginsWith($paramName, $dbField);
    }

    # This is an Alias for function addWhereBeginsWith
    public function addWhereStartsWith($paramName, $dbField = null)
    {
        return $this->addWhereBeginsWith($paramName, $dbField);
    }

    # This is an Alias for function addWhereBeginsWith
    public function addWhereStartsOn($paramName, $dbField = null)
    {
        return $this->addWhereBeginsWith($paramName, $dbField);
    }

    public function addWhereEndsWith($paramName, $dbField = null)
    {

    }

    # This is an Alias for function addWhereEndsWith
    public function addWhereEndsOn($paramName, $dbField = null)
    {
        return $this->addWhereEndsWith($paramName, $dbField);
    }


    public function addWhereContains($paramName, $dbField = null)
    {
        if ($dbField == null) {
            $dbField = $paramName;
        }

        $this->queryParts[] = ['type' => 'whereContains', 'paramName' => $paramName, 'dbField' => $dbField];
    }

    public function getQuery()
    {
        $result = $this->model;

        foreach ($this->queryParts as $idx => $queryPart) {

            if ($queryPart['type'] == 'scope') {

                $result = $result->{$queryPart['scope']}();
                continue;
            }

            if (isset($this->requestInput[$queryPart['paramName']]) && ($this->requestInput[$queryPart['paramName']] != '')) {

                $paramValue = $this->requestInput[$queryPart['paramName']];

                if ($queryPart['type'] == 'paramScope') {
                    $result = $result->{$queryPart['scope']}($paramValue);
                }
                elseif ($queryPart['type'] == 'whereEqual') {
                    $dbField = $queryPart['dbField'];
                    $result = $result->where($dbField, '=', $paramValue);
                }
                elseif ($queryPart['type'] == 'whereContains') {
                    $dbField = $queryPart['dbField'];
                    $result = $result->where($dbField, 'LIKE', "%{$paramValue}%");
                }

            }
        }

        return $result;
    }
}

