<?php

namespace Vicvk\Lib;

use Validator;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\File;

class MyForm
{
    use ValidatesRequests;

    protected $modelClassName = '';
    protected $model;


    # An array of inputs of checkbox type. If a checkbox from this list was not checked when the form
    # was submitted, the request will contain $checkboxes_unchecked_value as a value for such
    # unchecked checkbox.
    protected $checkboxes = [];  # Example $checkboxes = ['is_active', 'is_visible'];
    protected $checkboxesUncheckedValue = 'No';

    #
    # $fileUploads = [
    #     'input_name' => 'upload_type_for_this_input',  // The upload types are defined in config/upload.php
    # ];
    #
    protected $fileUploads = [];
    protected $skipValidationForTheseInputs = [];

    public $v = [];
    public $id;
    public $mode = '';

    protected $validator; # of type: \Illuminate\Validation\Validator

    ///////////////////////////////////////////////////////////////////////////////////////////////


   function __construct()
   {
       $this->v = [];  # form values shouldn't be null, it's always an array even if empty
   }

    /*
     * Display form (empty or with initial values) when creating a new resource
     */
    public function create()
    {
        $this->mode = 'create';

        $old = \Request::old();

        if ($old == array())
        {
            $this->onInitValuesForCreate();

            # then see if any values were passed to the form, these values should
            # overwrite the initial values set in onInitValuesForEdit()
            $this->receiveRequestInput(false);


            # assign values to upload inputs that already have a value
            foreach ($this->fileUploads as $fileUploadName => $fileUploadType) {
                if (isset($this->v[$fileUploadName]) && ($this->v[$fileUploadName]!='')) {
                    $this->v[$fileUploadName . '_tmp_upload_value'] = $this->v[$fileUploadName];
                }
            }
        }
        else
        {
            # The form already has been submitted, but didn't pass validation, that is why
            # we were redirected back here withErrors and withInput. The form should catch
            # its old values from \Request::old()

            $this->v = $old;
        }
    }

    /*
     * When the form was submitted in a "create new resource" mode, validate form data and
     * execute onHandleStore() method that is supposed to perform actions to actually handle
     * creating a new resource.
     */
    public function store()
    {
        $this->mode = 'store';

        $request = app('request');

        $this->handleTemporaryUploads();

        # NYI: $this->postprocessors();

        $this->receiveRequestInput(true);

        # need to create a model instance here, because the rulesFor() method may be dependant on
        # specific model attribute values
        # NYI!! What is that? And why this construct is not present in update() method?
        if (!empty($this->modelClassName)) {
            $this->model = app($this->modelClassName);
        }

        # NYI - think about it, should we move validation to controller?
        # because in case of errors, validator has to deal with http protocol
        # and this is controller's responsibility.

# !!!! NYI! we should validate $this->v here, not Request!!!
# maybe result of onProcessRequestInput should substitute results
# of its work to Request  <- Done!

# Actually - instead of using a validate() trait here, we can manually
# create a validator and validate $this->v, handling json requests as well
# see how I already implemented it in the index() method.


        $rules = $this->rulesFor('Store');
        $messages = $this->messagesFor('Store');

        foreach ($this->fileUploads as $fileUploadName => $fileUploadType) {

            # if this upload was restored from temp uploads, that means it already passed validation
            # before it was even saved to temp uploads.

            if ($this->skipValidationForTheseInputs[$fileUploadName]) {
                unset($rules[$fileUploadName]);
                unset($messages[$fileUploadName]);
            }
        }

///        $this->validate(app('request'), $rules, $messages);

        $this->validator = Validator::make($request->all(), $rules, $messages);

        $this->validator->after(function($validator) {
            $this->onValidate();
        });

        $this->validator->validate();

///        if ($this->validator->fails()) {
///            $this->throwValidationException($request, $this->validator);
///        }

        $this->onHandleStore();


        # NYI - throw an event indicating that a new resource was stored.
        # The event class name should be configurable in object properties.
    }

    public function edit($id=null)  # if $id==null then we can't retrieve and update model, then these actions should be done manually in overwritten methods onInitValuesForEdit(), onHandleUpdate()
    {
        $this->mode = 'edit';
        $this->id = $id;

        $old = \Request::old();

        if ($old == array())
        {
            # if the form hasn't yet been submited
            $this->onInitValuesForEdit($id);

            # then see if any values were passed to the form, these values should
            # overwrite the initial values set in onInitValuesForEdit()
            $this->receiveRequestInput(false);


            # assign values to upload inputs that already have a value
            foreach ($this->fileUploads as $fileUploadName => $fileUploadType) {
                if (isset($this->v[$fileUploadName]) && ($this->v[$fileUploadName]!='')) {
                    $this->v[$fileUploadName . '_tmp_upload_value'] = $this->v[$fileUploadName];
                }
            }
        }
        else
        {
            # The form already has been submitted, but didn't pass validation, that is why
            # we were redirected back here withErrors and withInput. The form should catch
            # its old values from \Request::old()

            $this->v = $old;
        }
    }

    public function update($id=null)
    {
        $this->mode = 'update';

        $request = app('request');

        $this->id = $id;


        $this->handleTemporaryUploads();


////exit;

        # NYI: $this->postprocessors();

        $this->receiveRequestInput(true);

        # need to create a model instance here, because the rules() method may be dependant on
        # specific model attribute values
        if (!empty($this->modelClassName)) {
            $this->model = app($this->modelClassName)->findOrFail($id);
        }

        # NYI - think about it, should we move validation to controller?
        # because in case of errors, validator has to deal with http protocol
        # and this is controller's responsibility.

# !!!! NYI! we should validate $this->v here, not Request!!!
# maybe result of onProcessRequestInput should substitute results
# of its work to Request   <- Done!

# Actually - instead of using a validate() trait here, we can manually
# create a validator and validate $this->v, handling json requests as well
# see how I already implemented it in the index() method.

///echo 'before-validate';
//exit;


        $rules = $this->rulesFor('Update');
        $messages = $this->messagesFor('Update');

        foreach ($this->fileUploads as $fileUploadName => $fileUploadType) {

            # if this upload was restored from temp uploads, that means it already passed validation
            # before it was even saved to temp uploads.

            if ($this->skipValidationForTheseInputs[$fileUploadName]) {
                unset($rules[$fileUploadName]);
                unset($messages[$fileUploadName]);
            }
        }

///        $this->validate(app('request'), $rules, $messages);

        $this->validator = Validator::make($request->all(), $rules, $messages);

        $this->validator->after(function($validator) {
            $this->onValidate();
        });


        $this->validator->validate();

///        if ($this->validator->fails()) {
///            $this->throwValidationException($request, $this->validator);
///        }


        $this->onHandleUpdate($id);

        # NYI - throw an event indicating that a resource was updated.
        # The event class name should be configurable in object properties.
    }

    protected function handleTemporaryUploads()
    {
////echo 'handleTemporaryUploads()';

        # If there are no upload inputs, then there is nothing to do in this method
        if (!is_array($this->fileUploads) || ($this->fileUploads == [])) {
            return;
        }

        # this is needed because if there is already a value for upload input in database,
        # we should skip validation for this input even if no new value was uploaded - because
        # the old value is considered to exist (so it passes the "required" rule) and valid
        # (otherwise it wouldn't be uploaded)

        if ($this->mode == 'create') {
            $this->onInitValuesForCreate();
        }
        elseif ($this->mode == 'update') {
            $this->onInitValuesForEdit($this->id);
        }

        $request = app('request');

        $this->skipValidationForTheseInputs = [];

        foreach ($this->fileUploads as $fileUploadName => $fileUploadType) {

////echo "!!!!{$fileUploadName}!!!!";
////exit;

            $this->skipValidationForTheseInputs[$fileUploadName] = false;

            $uploadedFile = $request->file($fileUploadName);

            if ($uploadedFile && $uploadedFile->isValid()) {

////echo '!!!!upload-is-valid!!!!';

                # in case if there was a previously uploaded file that passed validation
                # and was saveTempFileUpload(), then we should remove it from temp uploads.
                $this->removeTempFileUpload($fileUploadName);

                # Now validate through validation rules for this input - only if validation
                # passes we can saveTempUpload().

                if ($this->mode == 'create') {
                    $rules = $this->rulesFor('Create');
                }
                elseif ($this->mode == 'update') {
                    $rules = $this->rulesFor('Update');
                }

                if (isset($rules[$fileUploadName])) {
                    $rules = [$fileUploadName => $rules[$fileUploadName]];
                }
                else {
                    $rules = [];
                }

                if ($this->mode == 'create') {
                    $messages = $this->messagesFor('Create');
                }
                elseif ($this->mode == 'update') {
                    $messages = $this->messagesFor('Update');
                }

                if (isset($messages[$fileUploadName])) {
                    $messages = [$fileUploadName => $messages[$fileUploadName]];
                }
                else {
                    $messages = [];
                }

///                $validator = Validator::make($request->all(), $rules, $messages);

                $this->validator = Validator::make($request->all(), $rules, $messages);

                $this->validator->after(function($validator) {
                    $this->onValidate();
                });


                if ($this->validator->fails()) {
////echo '!!!!validation-fails!!!!';
                    # if there previously was a value, it should be reset by the new uploaded
                    # file, even if this file didn't pass the validation
                    $requestInput = $request->all();
                    $requestInput[$fileUploadName . '_tmp_upload_value'] = '?reset';
                    $request->replace($requestInput);

                }
                else {
////echo '!!!!validation-success!!!!';
                    # Only save to temp uploads if the file passes validation rules. Because
                    # on subsequent submits, when file is restored from temp uploads, it wouldn't
                    # be validated.
////echo 'begin';
////$requestInput = \Request::all();
////print_r($requestInput);
////echo 'end';

                    $this->saveTempFileUpload($fileUploadName);

////echo 'aaaa';
////$requestInput = \Request::all();
////print_r($requestInput);
////echo 'bbbb';
////exit;


                    $this->skipValidationForTheseInputs[$fileUploadName] = true;
                }
            }
            else {

                if ($this->restoreTempFileUpload($fileUploadName)) {
                    $this->skipValidationForTheseInputs[$fileUploadName] = true;
                }
                elseif (isset($this->v[$fileUploadName]) && ($this->v[$fileUploadName] != '')) {

                    # but what if this value was reset by uploading a file that didn't pass validation?
                    $requestInput = $request->all();

                    if (isset($requestInput[$fileUploadName . '_tmp_upload_value']) && $requestInput[$fileUploadName . '_tmp_upload_value']=='?reset') {
                        # if the value from saved model was reset by unsuccessfull upload, then
                        # do not bypass the validation
                    }
                    else {
                        $this->skipValidationForTheseInputs[$fileUploadName] = true;
                    }
                }
            }
        }
    }


    protected function saveTempFileUpload($fileUploadName)
    {
        $tempUploadId = null;

        $request = app('request');

        $uploadedFile = $request->file($fileUploadName);

        if ($uploadedFile && $uploadedFile->isValid()) {

            # move uploaded file to my directory for such temporary uploads
            $tempUploadsDir = rtrim(config('vicvk-lib.form_temporary_uploads_path'), "\\/");
            $tempFileName = basename(tempnam($tempUploadsDir, ''));
            $newPath = $tempUploadsDir . '/' . $tempFileName;

            # Do not move the file! Because the validation rules in validator will be calling isValid()
            # method on validated uploads and that method requires that is_uploaded_file() function
            # returned true on files being validated. If we move the file, is_uploaded_file() won't
            # return true for it, and all validation rules will fail due to that.
            ## $uploadedFile->move($tempUploadsDir, $tempFileName);

            # instead copy the uploaded file to temp uploads location

///echo 'src_path=' . $uploadedFile->getPathname();
///echo '#';
///echo 'dst_path=' . $newPath;
///exit;

            @copy($uploadedFile->getPathname(), $newPath);

            // maybe we should add file extension?? - probably not


            $tempUpload = [
                'path' => $tempFileName,
                'originalName' => $uploadedFile->getClientOriginalName(),
                'mimeType' => $uploadedFile->getClientMimeType(),
                'size' => $uploadedFile->getClientSize(),
                'error' => $uploadedFile->getError(),
                'test' => false,
            ];

///echo '+++++++';
///print_r($tempUpload);
///echo '+++++++';
///exit;


            $tempUploadId = $this->writeTempUploadRecord($tempUpload);

            $tempUploadId = '?tmp_upload_value=' . $tempUploadId;

            # it's not of UploadedFile class anymore because the file was copied to another location and
            # it hasn't necessarily been just uploaded on current request. So now it is just a File class.
            $newFile = new UploadedFile($newPath, $tempUpload['originalName'], $tempUpload['mimeType'], $tempUpload['size'], $tempUpload['error'], $tempUpload['test']);
            //$newFile = new File($tempUpload['path']);

            $requestInput = $request->all();

            $requestInput[$fileUploadName] = $newFile;

            # should add this input  ($fileUploadName . '_tmp_upload_value') to Request
            $requestInput[$fileUploadName . '_tmp_upload_value'] = $tempUploadId;

            $request->replace($requestInput);
            $request->files->set($fileUploadName, $newFile);
        }

        return $tempUploadId;
    }

    protected function restoreTempFileUpload($fileUploadName)
    {
        $result = false;

        $request = app('request');

        $requestInput = $request->all();

        if (isset($requestInput[$fileUploadName . '_tmp_upload_value'])) {

            $tempUploadId = $requestInput[$fileUploadName . '_tmp_upload_value'];

            if (substr($tempUploadId, 0, 18) == '?tmp_upload_value=') {

                $tempUploadId = substr($tempUploadId, 18);

                $tempUpload = $this->readTempUploadRecord($tempUploadId);

                if (is_array($tempUpload)) {

                    $tempUploadsDir = rtrim(config('vicvk-lib.form_temporary_uploads_path'), "\\/");

                    # it's not of UploadedFile class anymore because the file was copied to another location and
                    # it hasn't necessarily been just uploaded on current request. So now it is just a File class.
                    $file = new UploadedFile($tempUploadsDir . '/' . $tempUpload['path'], $tempUpload['originalName'], $tempUpload['mimeType'], $tempUpload['size'], $tempUpload['error'], $tempUpload['test']);
                    //$file = new File($tempUploadsDir . '/' . $tempUpload['path']);

                    $requestInput[$fileUploadName] = $file;

                    $request->replace($requestInput);
                    $request->files->set($fileUploadName, $file);

                    $result = true;
                }
            }
        }

        return $result;
    }

    protected function removeTempFileUpload($fileUploadName)
    {
        $request = app('request');

        $requestInput = $request->all();

        if (isset($requestInput[$fileUploadName . '_tmp_upload_value'])) {

            $tempUploadId = $requestInput[$fileUploadName . '_tmp_upload_value'];

            if (substr($tempUploadId, 0, 18) == '?tmp_upload_value=') {

                $tempUploadId = substr($tempUploadId, 18);

                $tempUpload = $this->readTempUploadRecord($tempUploadId);

                if (is_array($tempUpload)) {

                    if (isset($tempUpload['path'])) {
                        $tempUploadsDir = rtrim(config('vicvk-lib.form_temporary_uploads_path'), "\\/");
                        @unlink($tempUploadsDir . '/' . $tempUpload['path']);
                    }

                    $this->deleteTempUploadRecord($tempUploadId);
                }

                unset($requestInput[$fileUploadName . '_tmp_upload_value']);
                $request->replace($requestInput);
            }
        }
    }


    protected function writeTempUploadRecord(Array $tempUpload)
    {
        $table = config('vicvk-lib.form_temporary_uploads_db_table');
        $connection = config('vicvk-lib.form_temporary_uploads_db_connection');

        $tempUploadId = str_random(80) . str_replace(' ', '', microtime());

        $sql = <<<SQL
INSERT INTO {$table}
(id, created_at, path, original_name, mime_type, size, error, test)
values (?, ?, ?, ?, ?, ?, ?, ?)
SQL;

        \DB::connection($connection)->insert($sql, [
            $tempUploadId,
            time(),
            $tempUpload['path'],
            $tempUpload['originalName'],
            $tempUpload['mimeType'],
            $tempUpload['size'],
            $tempUpload['error'],
            ($tempUpload['test'] ? 1 : 0),
        ]);

        return $tempUploadId;
    }

    protected function readTempUploadRecord($tempUploadId)
    {
        $table = config('vicvk-lib.form_temporary_uploads_db_table');
        $connection = config('vicvk-lib.form_temporary_uploads_db_connection');

        $results = \DB::connection($connection)->select("SELECT * FROM {$table} WHERE id = ? LIMIT 1", [$tempUploadId]);

        if (isset($results[0])) {
            $tempUpload = [
                'path' => $results[0]->path,
                'originalName' => $results[0]->original_name,
                'mimeType' => $results[0]->mime_type,
                'size' => $results[0]->size,
                'error' => $results[0]->error,
                'test' => (bool) $results[0]->test,
            ];
        }
        else {
            $tempUpload = null;
        }

        return $tempUpload;
    }

    protected function deleteTempUploadRecord($tempUploadId)
    {
        $table = config('vicvk-lib.form_temporary_uploads_db_table');
        $connection = config('vicvk-lib.form_temporary_uploads_db_connection');

        \DB::connection($connection)->delete("DELETE FROM {$table} WHERE id = ? LIMIT 1", [$tempUploadId]);
    }

#//    public function search()
#//    {
#//        # search request is validated in controller!!
#//        # Can't use validate() method here because it redirects to a previous url in case of errors,
#//        # and if this is not a POST but GET request, such redirection leads to infinite redirection loop.
#//
#//        # NYI here we should catch form inputs' values from search params
#//        $this->v = app('request')->all();
#//
#////        $this->v['first_name'] = 'abc';
#//    }

    ///////////////////////////////////////////////////////////////////////////////////////////////

    /*
     * The order of precedence for rules and messages
     *   1) $this->rulesStore() / $this->rulesUpdate()
     *   2) $this->rulesSave
     *   3) $this->model->rulesStore() / $this->model->rulesUpdate()  (if $this->model is an object)
     *   4) $this->model->rulesSave();  (if $this->model is an object)
     *   5) app($this->modelClassName)->rulesStore()/Update; (if $this->model is not an object)
     *   6) app($this->modelClassName)->rulesSave(); (if $this->model is not an object)
     *
     */

    protected function rulesFor($operation)
    {
        $rules = $this->_formRulesFor($operation);

        if ($rules !== null) { # important! should be: !== null
            return $rules;
        }

        $rules = $this->_modelRulesFor($operation);

        if ($rules !== null) { # important! should be: !== null
            return $rules;
        }

        return [];
    }

    protected function _formRulesFor($operation)
    {
        $method = "rules{$operation}";

        if (method_exists($this, $method)) {
            return call_user_func(array($this, $method));
        }

        if (($operation == 'Store') || ($operation == 'Update')) {
            if ($operation != 'Save') { # because we already checked for rulesSave before.
                $method = 'rulesSave';

                if (method_exists($this, $method)) {
                    return call_user_func(array($this, $method));
                }
            }
        }

        return null;
    }

    protected function _modelRulesFor($operation)
    {
        if (!empty($this->modelClassName)) {

            # if there is already a found model, then use it, as it may
            # be an existing model with real data and it's data may affect
            # validation rules
            if (is_object($this->model)) {
                $obj = $this->model;
            }
            else {
                $obj = app($this->modelClassName);
            }

            $method = "rules{$operation}";

            if (method_exists($obj, $method)) {
                return call_user_func(array($obj, $method));
            }

            if (($operation == 'Store') || ($operation == 'Update')) {
                if ($operation != 'Save') { # because we already checked for rulesSave before.
                    $method = 'rulesSave';

                    if (method_exists($obj, $method)) {
                        return call_user_func([$obj, $method]);
                    }
                }
            }
        }

        return null;
    }

    # NYI!!!! Actually we can use the same rules() method to fetch rules
    # both for validating a new model before inserting it to database,
    # and also for validating an existing model, before updating it in database.
    # We can distinguish between these two states insert/update by examining
    # $this->model->id property - it will be non-empty only when in "update"
    # state. Think about it. But again, what when we don't have a model
    # attached to the form at all? And still would like to have different
    # sets of rules when "creating" a resource and "editing" it. Think about it.

    # Place this in descending class if we need a custom rules set for Store
    # public function rulesStore()
    # {
    #     return [];
    # }

    # Place this in descending class if we need a custom rules set for Update
    # public function rulesUpdate()
    # {
    #     return [];
    # }

    # Place this in descending class if we need a custom rules set for Save
    # public function rulesSave()
    # {
    #     return [];
    # }


    # Place this in descending class if we need a custom rules set for search
    # public function rulesSearch()
    # {
    #     return [];
    # }

    ///////////////////////////////////////////////////////////////////////////////////////////////

    public function messagesFor($operation)
    {
        $messages = $this->_formMessagesFor($operation);

        if ($messages !== null) { # important! should be: !== null
            return $messages;
        }

        $messages = $this->_modelMessagesFor($operation);

        if ($messages !== null) { # important! should be: !== null
            return $messages;
        }

        return [];
    }

    protected function _formMessagesFor($operation)
    {
        $method = "messages{$operation}";

        if (method_exists($this, $method)) {
            return call_user_func(array($this, $method));
        }

        if (($operation == 'Store') || ($operation == 'Update')) {
            if ($operation != 'Save') { # because we already checked for messagesSave before.
                $method = 'messagesSave';

                if (method_exists($this, $method)) {
                    return call_user_func([$this, $method]);
                }
            }
        }

        return null;
    }

    protected function _modelMessagesFor($operation)
    {
        if (!empty($this->modelClassName)) {

            # if there is already a found model, then use it, as it may
            # be an existing model with real data and it's data may affect
            # validation messages
            if (is_object($this->model)) {
                $obj = $this->model;
            }
            else {
                $obj = app($this->modelClassName);
            }

            $method = "messages{$operation}";

            if (method_exists($obj, $method)) {
                return call_user_func(array($obj, $method));
            }

            if (($operation == 'Store') || ($operation == 'Update')) {
                if ($operation != 'Save') { # because we already checked for messagesSave before.
                    $method = 'messagesSave';

                    if (method_exists($obj, $method)) {
                        return call_user_func([$obj, $method]);
                    }
                }
            }
        }

        return null;
    }

    # Place this in descending class if we need a custom messages set for Store
    # public function messagesStore()
    # {
    #     return [];
    # }

    # Place this in descending class if we need a custom messages set for Update
    # public function messagesUpdate()
    # {
    #     return [];
    # }

    # Place this in descending class if we need a custom messages set for Save
    # public function messagesSave()
    # {
    #     return [];
    # }

    # Place this in descending class if we need a custom messages set for search
    # public function messagesSearch()
    # {
    #     return [];
    # }

    ///////////////////////////////////////////////////////////////////////////////////////////////

    protected function onInitValuesForCreate()
    {
        # If there is a model specified, then this model can
        # supply default values for the new instance of a model.
        #
        # This is done by overriding the $attributes variable inside the Eloquent Model:
        #
        #    protected $attributes = [
        #        'title' => '1111',
        #        'field_name_is_visible' => 'Yes',
        #    ];

        if (!empty($this->modelClassName)) {
            $this->model = app($this->modelClassName);
            $this->v = $this->model->toArray();
        }

        # initial values are set by assigning $this->v input elements
        # $this->v['first_name'] = 'Tom';
    }

    /*
     * Default actions that are executed to handle creating a new resource.
     *
     * Can be overwritten in descending class
     */
    protected function onHandleStore()
    {
        $this->mode = 'store';

        // Maybe place it to store() method?
        $this->handlePermanentUploads();

        if (!empty($this->modelClassName) && is_object($this->model)) {

            $this->model->fill($this->v);
            $this->model->save();
        }
    }

    protected function onInitValuesForEdit($id=null)
    {
        #
        # $this->id is available here
        #

        if (!empty($this->modelClassName)) {
            $this->model = app($this->modelClassName)->findOrFail($id);

            // maybe use attributesToArray() instead of toArray()
            $this->v = $this->model->toArray();
        }
    }

    /*
     * Default actions that are executed to handle updating a resource.
     *
     * Can be overwritten in descending class
     */

    protected function onHandleUpdate($id=null)
    {
        $this->mode = 'update';
        $this->id = $id;

        // Maybe place it to update() method?
        $this->handlePermanentUploads();

////echo '1111';
////print_r($this->v);

	    if (!empty($this->modelClassName) && is_object($this->model)) {

            $this->model->fill($this->v);
            $this->model->save();
        }
////exit;
    }

    protected function handlePermanentUploads()
    {
//// /*
////  * move file from temp uploads to permanent location (helper function getUploadPermanentPath())
////  * set $this->v equal to new file path
////  * removeTempUploads - from directory and from DB
////  * move this piece of code to separate rewritable method - descending classes should be able to
////  * override this method and provide their own way to handle uploads
////  */



        # we need this $initial_v values for this reason:
        # If we received a request to delete an existing upload (by passing a '?reset' value),
        # we need to know if there already existed a value for upload, and what that value was,
        # so that we could delete ot.
        if ($this->mode == 'store') {
            # If we are creating a resource, then obviously there couldn't have been an existing value
            # for upload.
            $initial_v = [];
        }
        elseif ($this->mode == 'update') {
            $saved_v = $this->v;
            $saved_model = $this->model;

            $this->onInitValuesForEdit($this->id);

            $initial_v = $this->v;

            $this->v = $saved_v;
            $this->model = $saved_model;
        }

        foreach ($this->fileUploads as $fileUploadName => $fileUploadType) {

//print_r($this->v);
//exit;

            $tmpUploadValue = (isset($this->v[$fileUploadName.'_tmp_upload_value']) && ($this->v[$fileUploadName.'_tmp_upload_value']!='')) ? $this->v[$fileUploadName.'_tmp_upload_value'] : null;

            if (substr($tmpUploadValue, 0, 6) == '?reset') {
//echo '1111';
//exit;

                # any value for this upload should be removed - be it a permanent value
                # stored in database or a temporary upload

                if (isset($initial_v[$fileUploadName]) && ($initial_v[$fileUploadName]!='')) {

                    # we should delete not only the originals, but other file versions as well !!!

                    $fileVersions = config("upload.types.{$fileUploadType}.file_versions");

                    if (is_array($fileVersions)) {
                        foreach ($fileVersions as $fileVersionName => $fileVersionAttr) {
                            $path = isset($fileVersionAttr['path']) ? $fileVersionAttr['path'] : null;

                            if ($path) {

                                # just for safety, ensure that there is no trailing slash
                                $path = rtrim($path, '/');

                                @unlink($path . '/' . $initial_v[$fileUploadName]);
                            }
                        }
                    }
                }

                $this->v[$fileUploadName] = '';
//exit;

            }
            elseif (substr($tmpUploadValue, 0, 18) == '?tmp_upload_value=') {
///echo '2222';
                # store temporary upload as a permanent value in database

                $tempUploadId = substr($tmpUploadValue, 18);

                $tempUpload = $this->readTempUploadRecord($tempUploadId);

                if (is_array($tempUpload)) {

                    $tempUploadsDir = rtrim(config('vicvk-lib.form_temporary_uploads_path'), "\\/");

                    # it's not of UploadedFile class because the file was copied to another location and
                    # it even hasn't necessarily been just uploaded on the current request.
                    # So now it is just a File class.
                    //$file = new UploadedFile($tempUploadsDir . '/' . $tempUpload['path'], $tempUpload['originalName'], $tempUpload['mimeType'], $tempUpload['size'], $tempUpload['error'], $tempUpload['test']);
                    $file = new File($tempUploadsDir . '/' . $tempUpload['path']);

                    $original_path = config("upload.types.{$fileUploadType}.file_versions.original.path");

                    if ($original_path) {

                        # just for safety, ensure that there is no trailing slash
                        $original_path = rtrim($original_path, '/');

                        $permanentFileName = basename(tempnam($original_path, ''));

                        $extension = $file->guessExtension();

                        if ($extension) {
                            # Because tempnam() creates a zero length file in the directory,
                            # which serves as a placeholder.
                            @unlink($original_path . '/' . $permanentFileName);

                            $permanentFileName = "{$permanentFileName}.{$extension}";
                        }

                        $file->move($original_path, $permanentFileName);

                        $this->v[$fileUploadName] = $permanentFileName;

                        $this->removeTempFileUpload($fileUploadName);
                    }
                }
            }
            else {
////echo '3333';
                # do nothing - no new file was uploaded, and no old file was removed
            }

////exit;

        }
    }


    ///////////////////////////////////////////////////////////////////////////////////////////////

    public function receiveRequestInput($formWasSubmitted=true)
    {
        $requestInput = \Request::all();

        foreach($requestInput as $inputName => $inputValue) {
            $this->v[$inputName] = $inputValue;
        }

        # Handle checkboxes only if form was submitted by browser and non-checked checkboxes
        # didn't pass any value. So we need to set a default unchecked value for them.
        if ($formWasSubmitted) {

            foreach($this->checkboxes as $checkboxName) {
                if (isset($this->v[$checkboxName])) {
                    # no treatment is needed
                }
                else {
                    $this->v[$checkboxName] = $this->checkboxesUncheckedValue;
                }
            }
        }


        $this->onProcessRequestInput($formWasSubmitted);
        \Request::replace($this->v);

//$requestInput = \Request::all();
//print_r($requestInput);
//exit;




//print_r($this->v);
//exit;

    }

    # Should be ovewritten in child classes in case if some processing should be applied to
    # input received from \Request::input() before this input will be substituted to $this->v
    protected function onProcessRequestInput($formWasSubmitted)
    {
        # This processing should take place on $this->v
    }

    protected function onValidate()
    {
        # $this->mode = 'store' | 'update'


//        if ($this->v['field'] != 'valid value') {
//            $this->validator->errors()->add('field', 'Something is wrong with this field!');
//        }
    }


    protected function _getModelOptionsFor($inputName)
    {
        $method = "get{$inputName}Options";

        if (!empty($this->modelClassName)) {

            # if there is already a found model, then use it, as it may
            # be an existing model with real data and it's data may affect
            # validation messages
            if (is_object($this->model)) {
                $obj = $this->model;
            }
            else {
                $obj = app($this->modelClassName);
            }

            if (method_exists($obj, $method)) {
                return call_user_func(array($obj, $method));
            }
            else {
                throw new \BadMethodCallException(
                    "Bad call to method {$method}: this method is not defined neither within the Form nor within the attached Model."
                );
            }
        }
        else {
            throw new \BadMethodCallException(
                "Bad call to method {$method}: this method is not defined within the Form and we can't try to get the options from Model, because there is no Model attached to this Form."
            );
        }

        return null;
    }

    public function __call($method, $arguments)
    {
        if (preg_match('/^get([a-z0-9\_]+)Options$/i', $method, $matches)) {
            $inputName = $matches[1];

            return $this->_getModelOptionsFor($inputName);
        }
        else {
            switch ($method) {
                case 'rulesStore':
                    return $this->rulesFor('Store');
                    break;

                case 'rulesUpdate':
                    return $this->rulesFor('Update');
                    break;

                case 'rulesSave':
                    return $this->rulesFor('Save');
                    break;

                case 'rulesSearch':
                    return $this->rulesFor('Search');
                    break;



                case 'messagesStore':
                    return $this->messagesFor('Store');
                    break;

                case 'messagesUpdate':
                    return $this->messagesFor('Update');
                    break;

                case 'messagesSave':
                    return $this->messagesFor('Save');
                    break;

                case 'messagesSearch':
                    return $this->messagesFor('Search');
                    break;
            }

        }

        throw new \BadMethodCallException(
            sprintf(
                'Call to undefined method %s->%s()',
                get_called_class(),
                $method
            )
        );

    } // public function __call($method, $arguments)

    public function v($inputName, $defaultValue='')
    {
        return isset($this->v[$inputName]) ? $this->v[$inputName] : $defaultValue;
    }

    public function v_checked($inputName, $checkedValue='Yes')
    {
        return (isset($this->v[$inputName]) && ($this->v[$inputName] == $checkedValue)) ? ' checked ' : '';
    }

}

