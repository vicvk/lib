<?php

namespace Vicvk\Lib;

class UtilsFileSystem
{


/**
 * @package third_party_modules
 * 
 */
/*
������� ����������� ������ � ������ � ��������� ������.
�������� ����������:

$digits - ������������ ���������� ���� � �������������� ������. �������� �����
          ����������� �� ������� ���-�� ����
$mode   - ���� 'iec' �� ������� �� 1024, ���� 'si' �� ������� �� 1000
$unit   - ���� 'bit' �� ��������� ��� � ����, ���� 'byte' �� ������� ��� � ������

�������� ��� ������� � utils_string ��������� ����
*/
public static function formatBytes($val, $digits = 3, $mode = "iec", $unit = "byte")
{ 
    $mode = strtolower($mode);
    $unit = strtolower($unit);

    $si = array("", "k", "M", "G", "T", "P", "E", "Z", "Y");
    $iec = array("", "K", "M", "G", "T", "P", "E", "Z", "Y");

    /// ��� ���� � ���������, �� ��� ��� �� ����:
    ///$iec = array("", "Ki", "Mi", "Gi", "Ti", "Pi", "Ei", "Zi", "Yi");

    switch(strtoupper($mode)) 
    {
        case "SI" : $factor = 1000; $symbols = $si; break;
        case "IEC" : $factor = 1024; $symbols = $iec; break;
        default : $factor = 1000; $symbols = $si; break;
    }
    switch($unit) 
    {
        case "bit" : 
            $val *= 8;
            $bB = 'b';
            break;

        default : 
            $bB = "B"; 
            break;
    }

    for($i=0;$i<count($symbols)-1 && $val>=$factor;$i++) $val /= $factor;

    $p = strpos($val, ".");

    if($p !== false && $p > $digits) $val = round($val);
    elseif($p !== false) $val = round($val, $digits-$p);

    return round($val, $digits) . " " . $symbols[$i] . $bB;
}



/*
������� ������� ����� � ���������� (� ����������� ������������ ��������� ����������)
�� �������� ���������� $dir. ������ � $dir ��� ���������� ������������� ������ ������������� ��
������. � ����� � ������������� - ��� ��� �����. ��� ��� ��� ��� ���� ���������� ����, �� ����������
������ �� �������, ��� ����.

$exclude_items - ������ � ��������� ������� �� ������ ���� �������. ����� ��������� ������ � �����,
��������: array('directory/subdirectory/file.zip')
���� ������ �������� �����������, �� ��������� �� ����� ���� / ������: � � ������ ���� �����
�� ������ ����!
array('directory/subdirectory', 'my-directory')

$exclude_files_regexp, $exclude_dirs_regexp - ���������� ��������� (��� ��������� ��� ������ preg_match)
��� ���������� � ����� �� ���������� ��������� ����� �� ����� ������. (������������ � ����� � ����������)
���� � ������� � ��������� �� ��������� - ������ ��� ����� ��� ����������
$exclude_files_regexps ������ ��������� ����������� � �������� ���������� �������, �
$exclude_dirs_regexps ������ ����������� � �����������

� ��� ��� ���������� ��������� ����������� ����� ��������� ������� �� ������������ ������ ������
$include_files_regexps
$include_dirs_regexps
���� ��� ���������� ��������� �� ��������� �� ������, �� ��������� ��� ������������ �����
��� ������� (�� ����� ����������).

$recursive_curr_path - ��������������� ���������� ������������ ��� �������� ��� ����������� ������
�������� ���� ������������ �������� ���������� $dir. ������� ������ ���� ���������� �� �����,
�� ��������� ����� ������ ������ - ��� � ����.

������� ���������� ���������� �������� (������ ��� ����������) ������� �� ���� �������. �� �������
���� ��� ������� unlink ��� rmdir �� ������ ������� ������ - ���� �� ������� ����, ����
��������� ��� �����-�� ��������� ������.
�� ������� ��� � �� ������ ���� ���� ������� (�� ������ ���������� ��� ����������� ��� ����������
��������� ����������) �� ����������� ��� �������� ����������� ��������.
���� ������� ������� 0 ������ ���� ������� ��� ������� ��������� (����� ����������� ����������) -
��� ���������� ���������� �������, ��� � ������ ����. ���� �� ��������� ������� ������� ������
���� ���� �������, �� ���������� �������, �� ������� ������ �������� �������� �� ���� � ������
���������� ����� ����������� ��������.
*/

public static function cleanupDirectory($dir, $exclude_items=array(), $exclude_files_regexps=array(), $exclude_dirs_regexps=array(), $include_files_regexps=array(), $include_dirs_regexps=array(), $recursive_curr_path='')
{
    # ����� ����� ���� �� ������� �� $dir ���������� ������, �� � �������� ��� �� �����
    # ������������� ������ �� ��������

    clearstatcache();

    $count_failed_items = 0;

    $curr_dir = ($recursive_curr_path == '') ? $dir : ($dir . '/' . $recursive_curr_path);

    if ($handle = opendir($curr_dir))
    {
        while (false !== ($file = readdir($handle))) 
        {
            if (($file != '.') && ($file != '..'))
            {
                $x = ($recursive_curr_path == '') ? $file : ($recursive_curr_path . '/' . $file);

                $x_full = $dir . '/' . $x;

                # ������ ����� $include_dirs_regexps ��������, ����� ���� �������� � �������
                # � �� ����� ������� �������
                if (($include_dirs_regexps != array()) && is_dir($x_full))
                {
                    $tmp_matched = false;

                    foreach($include_dirs_regexps as $include_dirs_regexp)
                    {
                        if (preg_match($include_dirs_regexp, $file))
                        {
                            # ��� ���������, ����� ���������� ������ ������, ���� ������ ������� �� �� �����
                            # � ����������
                            $tmp_matched = true;
                            break;
                        }
                    }

                    if (!$tmp_matched)
                    {
                        continue;
                    }
                }

                # ������ ����� $include_files_regexps ��������, ����� ���� �������� � �������
                # � �� ����� ������� �������
                if (($include_files_regexps != array()) && is_file($x_full))
                {
                    $tmp_matched = false;

                    foreach($include_files_regexps as $include_files_regexp)
                    {
                        if (preg_match($include_files_regexp, $file))
                        {
                            # ��� ���������, ����� ���������� ������ ������, ���� ������ ������� �� �� �����
                            # � ����������
                            $tmp_matched = true;
                            break;
                        }
                    }

                    if (!$tmp_matched)
                    {
                        continue;
                    }
                }

                if (in_array($x, $exclude_items))
                {
                    continue;
                }
                elseif (($exclude_files_regexps != array()) && is_file($x_full))
                {
                    $tmp_matched = false;

                    foreach($exclude_files_regexps as $exclude_files_regexp)
                    {
                        if (preg_match($exclude_files_regexp, $file))
                        {
                            $tmp_matched = true;
                            break;
                        }
                    }

                    if ($tmp_matched)
                    {
                        continue;
                    }
                }
                elseif (($exclude_dirs_regexps != array()) && is_dir($x_full))
                {
                    $tmp_matched = false;

                    foreach($exclude_dirs_regexps as $exclude_dirs_regexp)
                    {
                        if (preg_match($exclude_dirs_regexp, $file))
                        {
                            $tmp_matched = true;
                            break;
                        }
                    }

                    if ($tmp_matched)
                    {
                        continue;
                    }
                }

                if (is_dir($x_full))
                {
                    $count_failed_items += static::cleanupDirectory($dir, $exclude_items, $exclude_files_regexps, $exclude_dirs_regexps, $include_files_regexps, $include_dirs_regexps, $x);

                    # ������� ���������� ������ � ��� ������ ���� ��� ������
                    if (static::isEmptyDirectory($x_full))
                    {
                        if (@rmdir($x_full)) { }
                        else
                        {
                            # ������ ��� ����� �� ������� ���� ������� ����������
                            $count_failed_items++;
                        }
                    }
                }
                else
                {
                    if (@unlink($x_full)) { }
                    else
                    {
                        # ������ ��� ����� �� ������� ���� ������� ����
                        $count_failed_items++;
                    }
                }
            }
        }
        closedir($handle);
    }

    return $count_failed_items;
}

/*

Note that mode  is not automatically assumed to be an octal value, so strings 
(such as "g+w") will not work properly. To ensure the expected operation, 
you need to prefix mode  with a zero (0):

chmod("/somedir/somefile", 755);           // decimal; probably incorrect
chmod("/somedir/somefile", "u+rwx,go+rx"); // string; incorrect
chmod("/somedir/somefile", 0755);          // octal; correct value of mode

*/
public static function chmodSubdirectories($dir, $mode, $exclude_items=array(), $exclude_files_regexps=array(), $exclude_dirs_regexps=array(), $include_files_regexps=array(), $include_dirs_regexps=array(), $recursive_curr_path='')
{
    # ����� ����� ���� �� ������� �� $dir ���������� ������, �� � �������� ��� �� �����
    # ������������� ������ �� ��������

    clearstatcache();

    $count_failed_items = 0;

    $curr_dir = ($recursive_curr_path == '') ? $dir : ($dir . '/' . $recursive_curr_path);

    if ($handle = opendir($curr_dir))
    {
        while (false !== ($file = readdir($handle))) 
        {
            if (($file != '.') && ($file != '..'))
            {
                $x = ($recursive_curr_path == '') ? $file : ($recursive_curr_path . '/' . $file);

                $x_full = $dir . '/' . $x;

                # ������ ����� $include_dirs_regexps ��������, ����� ���� �������� � �������
                # � �� ����� ������� �������
                if (($include_dirs_regexps != array()) && is_dir($x_full))
                {
                    $tmp_matched = false;

                    foreach($include_dirs_regexps as $include_dirs_regexp)
                    {
                        if (preg_match($include_dirs_regexp, $file))
                        {
                            # ��� ���������, ����� ���������� ������ ������, ���� ������ ������� �� �� �����
                            # � ����������
                            $tmp_matched = true;
                            break;
                        }
                    }

                    if (!$tmp_matched)
                    {
                        continue;
                    }
                }

                # ������ ����� $include_files_regexps ��������, ����� ���� �������� � �������
                # � �� ����� ������� �������
                if (($include_files_regexps != array()) && is_file($x_full))
                {
                    $tmp_matched = false;

                    foreach($include_files_regexps as $include_files_regexp)
                    {
                        if (preg_match($include_files_regexp, $file))
                        {
                            # ��� ���������, ����� ���������� ������ ������, ���� ������ ������� �� �� �����
                            # � ����������
                            $tmp_matched = true;
                            break;
                        }
                    }

                    if (!$tmp_matched)
                    {
                        continue;
                    }
                }

                if (in_array($x, $exclude_items))
                {
                    continue;
                }
                elseif (($exclude_files_regexps != array()) && is_file($x_full))
                {
                    $tmp_matched = false;

                    foreach($exclude_files_regexps as $exclude_files_regexp)
                    {
                        if (preg_match($exclude_files_regexp, $file))
                        {
                            $tmp_matched = true;
                            break;
                        }
                    }

                    if ($tmp_matched)
                    {
                        continue;
                    }
                }
                elseif (($exclude_dirs_regexps != array()) && is_dir($x_full))
                {
                    $tmp_matched = false;

                    foreach($exclude_dirs_regexps as $exclude_dirs_regexp)
                    {
                        if (preg_match($exclude_dirs_regexp, $file))
                        {
                            $tmp_matched = true;
                            break;
                        }
                    }

                    if ($tmp_matched)
                    {
                        continue;
                    }
                }

                if (is_dir($x_full))
                {
                    $count_failed_items += static::chmodSubdirectories($dir, $mode, $exclude_items, $exclude_files_regexps, $exclude_dirs_regexps, $include_files_regexps, $include_dirs_regexps, $x);

                    if (@chmod($x_full, $mode)) { }
                    else
                    {
                        # ������ ��� ����� �� ������� ���� �������� �����
                        $count_failed_items++;
                    }
                }
                else
                {
                    if (@chmod($x_full, $mode)) { }
                    else
                    {
                        # ������ ��� ����� �� ������� ���� ������� ����
                        $count_failed_items++;
                    }
                }
            }
        }
        closedir($handle);
    }

    return $count_failed_items;
}

/*
����� ��������� � ������� ������ ����������� ��������� ������� cleanupDirectory()
*/
public static function copySubdirectories($dir_source, $dir_dest, $exclude_items=array(), $exclude_files_regexps=array(), $exclude_dirs_regexps=array(), $include_files_regexps=array(), $include_dirs_regexps=array(), $recursive_curr_path='')
{
    # ����� ����� ���� �� ������� �� $dir ���������� ������, �� � �������� ��� �� �����
    # ������������� ������ �� ��������

    clearstatcache();

    $count_failed_items = 0;

    $curr_dir_source = ($recursive_curr_path == '') ? $dir_source : ($dir_source . '/' . $recursive_curr_path);

    if ($handle = opendir($curr_dir_source))
    {
        while (false !== ($file = readdir($handle)))
        {
            if (($file != '.') && ($file != '..'))
            {
                $x = ($recursive_curr_path == '') ? $file : ($recursive_curr_path . '/' . $file);

                $x_full = $dir_source . '/' . $x;
                $y_full = $dir_dest . '/' . $x;


                # ������ ����� $include_dirs_regexps ��������, ����� ���� �������� � �������
                # � �� ����� ������� �������
                if (($include_dirs_regexps != array()) && is_dir($x_full))
                {
                    $tmp_matched = false;

                    foreach($include_dirs_regexps as $include_dirs_regexp)
                    {
                        if (preg_match($include_dirs_regexp, $file))
                        {
                            # ��� ���������, ����� ���������� ������ ������, ���� ������ ������� �� �� �����
                            # � ����������
                            $tmp_matched = true;
                            break;
                        }
                    }

                    if (!$tmp_matched)
                    {
                        continue;
                    }
                }

                # ������ ����� $include_files_regexps ��������, ����� ���� �������� � �������
                # � �� ����� ������� �������
                if (($include_files_regexps != array()) && is_file($x_full))
                {
                    $tmp_matched = false;

                    foreach($include_files_regexps as $include_files_regexp)
                    {
                        if (preg_match($include_files_regexp, $file))
                        {
                            # ��� ���������, ����� ���������� ������ ������, ���� ������ ������� �� �� �����
                            # � ����������
                            $tmp_matched = true;
                            break;
                        }
                    }

                    if (!$tmp_matched)
                    {
                        continue;
                    }
                }


                if (in_array($x, $exclude_items))
                {
                    continue;
                }
                elseif (($exclude_files_regexps != array()) && is_file($x_full))
                {
                    $tmp_matched = false;

                    foreach($exclude_files_regexps as $exclude_files_regexp)
                    {
                        if (preg_match($exclude_files_regexp, $file))
                        {
                            $tmp_matched = true;
                            break;
                        }
                    }

                    if ($tmp_matched)
                    {
                        continue;
                    }
                }
                elseif (($exclude_dirs_regexps != array()) && is_dir($x_full))
                {
                    $tmp_matched = false;

                    foreach($exclude_dirs_regexps as $exclude_dirs_regexp)
                    {
                        if (preg_match($exclude_dirs_regexp, $file))
                        {
                            $tmp_matched = true;
                            break;
                        }
                    }

                    if ($tmp_matched)
                    {
                        continue;
                    }
                }

                if (is_dir($x_full))
                {
                    # C������ ���������� ���������� ������ � ��� ������ ���� ��� ��� �� ����������.
                    # ����� ��������� ��� ��� � ���������� ���������� ��� ���������� ���� � ��� ��
                    # ������ ��� � ���������� ����������. � ���� ������ �� �������� �� ��������
                    # $count_failed_items (���� ���� ���������� ����������� �� �������) � ��� �����
                    # ������� ���������� copySubdirectories() ������� �����������, � � ������ ��������
                    # � ��� ����� ���������� ������� $count_failed_items - � � �������� ����� �� �����
                    # ���������� ������� ���������� � ������ �� ������� �����������.
                    if (file_exists($y_full))
                    {
                        if (is_dir($y_full))
                        {
                            # ��� ���������, ���������� ����� �� ��������� � ������ ������� ����
                        }
                        else
                        {
                            # �� ������� ����������� ����������, �.�. ��� ���������� ������
                            # � ��� �� ������ ��� � ���������� ����������, �� �� ����������
                            # �����������
                            $count_failed_items++;
                        }
                    }
                    else
                    {
                        if (@mkdir($y_full)) { }
                        else
                        {
                            # ������ ��� ����� �� ������� ���� ������� ����������
                            $count_failed_items++;
                        }
                    }

                    $count_failed_items += static::copySubdirectories($dir_source, $dir_dest, $exclude_items, $exclude_files_regexps, $exclude_dirs_regexps, $include_files_regexps, $include_dirs_regexps, $x);
                }
                else
                {
                    if (@copy($x_full, $y_full)) { }
                    else
                    {
                        # ������ ��� ����� �� ������� ���� ����������� ����
                        $count_failed_items++;
                    }
                }
            }
        }
        closedir($handle);
    }

    return $count_failed_items;
}





/*
  Removes directory even it is not empty 
*/
public static function removeDirectory($dir)
{
    static::cleanupDirectory($dir);
    return rmdir($dir);
}


public static function isEmptyDirectory($dir)
{
    clearstatcache();

    $result = true;

    if ($handle = opendir($dir)) 
    {
        while (false !== ($file = readdir($handle))) 
        {
            if ($file != '.' && $file != '..') 
            {
                $result = false;
                break;
            }
        }
        closedir($handle);
    }

    return $result;
}


public static function readDirectoryFileNames($dir)
{
    clearstatcache();

    $result = array();

    if ($handle = opendir($dir)) 
    {
        while (false !== ($file = readdir($handle))) 
        {
            if ($file != "." && $file != "..") 
            {
                if (is_file($dir . $file))
                {
                    $result[] = $file;
                }
            }
        }
        closedir($handle);
    }

    return $result;
}

public static function readDirectorySubdirectories($dir)
{
    clearstatcache();

    $result = array();

    if ($handle = opendir($dir)) 
    {
        while (false !== ($file = readdir($handle))) 
        {
            if ($file != "." && $file != "..") 
            {
                if (is_dir($dir . $file))
                {
                    $result[] = $file;
                }
            }
        }
        closedir($handle);
    }

    return $result;
}


/*
������ ��� ����� ����������� � ������ ���������� � �� ���� ��������� ��������������.
*/
#function readDirectoryFileNamesRecursive($dir, $level=0)
#{
#    if (substr($dir, -1) != '/')
#    {
#        $dir = $dir . '/';
#    }
#
#    clearstatcache();
#
#    $result = array();
#
#    if ($handle = opendir($dir)) 
#    {
#        while (false !== ($file = readdir($handle))) 
#        {
#//echo "*$file*";
#
#            if ($file != '.' && $file != '..') 
#            {
#                if (is_file($dir . $file))
#                {
#                    $result[] = $dir . $file;
#                }
#                elseif(is_dir($dir . $file))
#                {
#                    $files_in_nested_directory = static::readDirectoryFileNamesRecursive($dir . $file, $level+1);
#
#                    foreach($files_in_nested_directory as $x)
#                    {
#                        $result[] = $x;
#                    }
#                }
#            }
#        }
#        closedir($handle);
#    }
#
#
#    # �������� ���� � �������� ���������� ����� ��� ���� ���������� ������������ �������� ����������
#    if ($level == 0)
#    {
#        $c = strlen($dir);
#
#        foreach($result as $key => $value)
#        {
#            $result[$key] = substr($value, $c);
#        }
#    }
#
#    return $result;
#}




public static function readDirectoryFileNamesRecursive($dir, $exclude_items=array(), $exclude_files_regexps=array(), $exclude_dirs_regexps=array(), $include_files_regexps=array(), $include_dirs_regexps=array(), $recursive_curr_path='')
{
    # ����� ����� ���� �� ������� �� $dir ���������� ������, �� � �������� ��� �� �����
    # ������������� ������ �� ��������

    clearstatcache();

    $result = array();

    $curr_dir = ($recursive_curr_path == '') ? $dir : ($dir . '/' . $recursive_curr_path);

    if ($handle = opendir($curr_dir))
    {
        while (false !== ($file = readdir($handle))) 
        {
            if (($file != '.') && ($file != '..'))
            {
                $x = ($recursive_curr_path == '') ? $file : ($recursive_curr_path . '/' . $file);

                $x_full = $dir . '/' . $x;

                # ������ ����� $include_dirs_regexps ��������, ����� ���� �������� � �������
                # � �� ����� ������� �������
                if (($include_dirs_regexps != array()) && is_dir($x_full))
                {
                    $tmp_matched = false;

                    foreach($include_dirs_regexps as $include_dirs_regexp)
                    {
                        if (preg_match($include_dirs_regexp, $file))
                        {
                            # ��� ���������, ����� ���������� ������ ������, ���� ������ ������� �� �� �����
                            # � ����������
                            $tmp_matched = true;
                            break;
                        }
                    }

                    if (!$tmp_matched)
                    {
                        continue;
                    }
                }

                # ������ ����� $include_files_regexps ��������, ����� ���� �������� � �������
                # � �� ����� ������� �������
                if (($include_files_regexps != array()) && is_file($x_full))
                {
                    $tmp_matched = false;

                    foreach($include_files_regexps as $include_files_regexp)
                    {
                        if (preg_match($include_files_regexp, $file))
                        {
                            # ��� ���������, ����� ���������� ������ ������, ���� ������ ������� �� �� �����
                            # � ����������
                            $tmp_matched = true;
                            break;
                        }
                    }

                    if (!$tmp_matched)
                    {
                        continue;
                    }
                }

                if (in_array($x, $exclude_items))
                {
                    continue;
                }
                elseif (($exclude_files_regexps != array()) && is_file($x_full))
                {
                    $tmp_matched = false;

                    foreach($exclude_files_regexps as $exclude_files_regexp)
                    {
                        if (preg_match($exclude_files_regexp, $file))
                        {
                            $tmp_matched = true;
                            break;
                        }
                    }

                    if ($tmp_matched)
                    {
                        continue;
                    }
                }
                elseif (($exclude_dirs_regexps != array()) && is_dir($x_full))
                {
                    $tmp_matched = false;

                    foreach($exclude_dirs_regexps as $exclude_dirs_regexp)
                    {
                        if (preg_match($exclude_dirs_regexp, $file))
                        {
                            $tmp_matched = true;
                            break;
                        }
                    }

                    if ($tmp_matched)
                    {
                        continue;
                    }
                }

                if (is_dir($x_full))
                {
                    $files_in_nested_directory = static::readDirectoryFileNamesRecursive($dir, $exclude_items, $exclude_files_regexps, $exclude_dirs_regexps, $include_files_regexps, $include_dirs_regexps, $x);

                    foreach($files_in_nested_directory as $tmp)
                    {
                        $result[] = $tmp;
                    }
                }
                else
                {
                    $result[] = $x;
                }
            }
        }
        closedir($handle);
    }

    return $result;
}


/*
�������� ���� ��� ���� ���� � $destination �� ������� ���������� �� �������� ���������� ����������,
�� ��� ��������� ���������� ���������.
�������� ������ ����, �� ����������.
*/
public static function copy($source, $destination)
{
    # ��� ����� ��������� �������� � windows-������:
    $source = str_replace('\\', '/', $source);
    $destination = str_replace('\\', '/', $destination);


////echo "begin: $source, $destination\r\n<br/>\r\n";
////echo "###########################\r\n<br/>\r\n";

    $destination_parts = explode('/', $destination);



    # ��� ����� ������� ���� ������� � �����, �� ��� ������ ���� �������� �������
    # ��� ���� �� � ������ ���� ���� ��������� ������ ��������.
    do
    {
        $last_element = array_pop($destination_parts);
    }
    while ($last_element == '');

    $destination_parent_directory = implode('/', $destination_parts) . '/';

////exit;

    # ������������ ���������� ������ ������������, ���� ��� �� ���, �� ���������� 
    # �������� ��� ��������� ����������, ������� �� ������������ � ������ �����.
    if (file_exists($destination_parent_directory))
    {
        $parent_directory_exists = true;
    }
    else
    {
        $parent_directory_exists = static::copy($source, $destination_parent_directory);
    }

    # ������ ����� ������������ ���������� �������� ���������� 

    if ($parent_directory_exists)
    {
        if (substr($destination, -1) == '/')
        {
            $result = mkdir($destination);
        }
        else
        {
            $result = copy($source, $destination);
        }
    }
    else
    {
        $result = false;
    }

    return $result;



////echo $x;

////exit;
}



/*
���������� ���� ��� ���� ���� � $destination �� ������� ���������� �� �������� ���������� ����������,
�� ��� ��������� ���������� ���������.
���������� ������ ����, �� ����������.
*/
public static function rename($source, $destination)
{
    # ��� ����� ��������� �������� � windows-������:
    $source = str_replace('\\', '/', $source);
    $destination = str_replace('\\', '/', $destination);


////echo "begin: $source, $destination\r\n<br/>\r\n";
////echo "###########################\r\n<br/>\r\n";

    $destination_parts = explode('/', $destination);



    # ��� ����� ������� ���� ������� � �����, �� ��� ������ ���� �������� �������
    # ��� ���� �� � ������ ���� ���� ��������� ������ ��������.
    do
    {
        $last_element = array_pop($destination_parts);
    }
    while ($last_element == '');

    $destination_parent_directory = implode('/', $destination_parts) . '/';

////exit;

    # ������������ ���������� ������ ������������, ���� ��� �� ���, �� ���������� 
    # �������� ��� ��������� ����������, ������� �� ������������ � ������ �����.
    if (file_exists($destination_parent_directory))
    {
        $parent_directory_exists = true;
    }
    else
    {
        $parent_directory_exists = static::copy($source, $destination_parent_directory);
    }

    # ������ ����� ������������ ���������� �������� ���������� 

    if ($parent_directory_exists)
    {
        if (substr($destination, -1) == '/')
        {
            $result = mkdir($destination);
        }
        else
        {
            $result = rename($source, $destination);
        }
    }
    else
    {
        $result = false;
    }

    return $result;



////echo $x;

////exit;
}


}

